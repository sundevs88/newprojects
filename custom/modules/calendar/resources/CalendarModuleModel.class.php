<?php

  // Include application specific model base
  require_once APPLICATION_PATH . '/resources/ActiveCollabModuleModel.class.php';

  /**
   * Tasks module model
   * 
   * @package activeCollab.modules.tasks
   * @subpackage models
   */
  class CalendarModuleModel extends ActiveCollabModuleModel {
  
    /**
     * Load initial framework data
     *
     * @param string $environment
     */
    function loadInitialData($environment = null) {
      $project_tabs = $this->getConfigOptionValue('project_tabs');

      if(!in_array('calendar', $project_tabs)) {
        $project_tabs[] = 'calendar';
        $this->setConfigOptionValue('project_tabs', $project_tabs);
      } // if
      
      parent::loadInitialData($environment);
    } // loadInitialData
    
  }