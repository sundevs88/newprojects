<?php

  // Build on top of backend controller
  AngieApplication::useController('backend', ENVIRONMENT_FRAMEWORK_INJECT_INTO);

  /**
   * System level calendar
   *
   * @package activeCollab.modules.calendar
   * @subpackage controllers
   */
  class CalendarController extends BackendController  {

    /**
     * Active module
     *
     * @var string
     */
    protected $active_module = CALENDAR_MODULE;

    /**
     * Prepare controller
     */
    function __before() {
      parent::__before();
      
      $this->wireframe->tabs->clear();
      $this->wireframe->tabs->add('calendar', lang('Calendar'), Router::assemble('dashboard_calendar'), null, true);
      
      EventsManager::trigger('on_calendar_tabs', array(&$this->wireframe->tabs, &$this->logged_user));

      $this->wireframe->breadcrumbs->add('calendar', lang('Calendar'), Router::assemble('dashboard_calendar'));
      $this->wireframe->setCurrentMenuItem('calendar');
    } // __construct

    /**
     * Calendar
     */
    function index() {
      require_once CALENDAR_MODULE_PATH . '/models/generators/DashboardCalendarGenerator.class.php';

      if ($this->logged_user->isFeedUser()) {
        $this->wireframe->actions->add('ical_feed', lang('iCalendar Feed'), Router::assemble('ical_subscribe'), array(
          'onclick' => new FlyoutCallback(array(
            'width'		=> 'narrow'
          )),
          'primary' => true,
        ));
      } // if
			
      if($this->request->get('month') && $this->request->get('year')) {
      	$date = DateTimeValue::make(0, 0, 0, $this->request->get('month'), 1, $this->request->get('year'));
      } else {
        $date = new DateTimeValue(time() + get_user_gmt_offset());
      } // if
      
      $today = new DateTimeValue(time() + get_user_gmt_offset());

      $first_weekday = ConfigOptions::getValueFor('time_first_week_day', $this->logged_user);
      $work_days = ConfigOptions::getValueFor('time_workdays', $this->logged_user);

      $generator = new DashboardCalendarGenerator($date->getMonth(), $date->getYear(), $first_weekday, $work_days);

      $generator->setData(Calendar::getActiveProjectsData($this->logged_user, $date->getMonth(), $date->getYear(), true));

      $this->smarty->assign(array(
      	'date'		 => $date,
        'calendar' => $generator,
      	'today'		 => $today
      ));
    } // calendar

    /**
     * Show calendar day
     */
    function day() {
      if($this->request->get('year') && $this->request->get('month') && $this->request->get('day')) {
        $day = new DateValue($this->request->get('year') . '-' . $this->request->get('month') . '-' . $this->request->get('day'));
      } else {
        $day = DateValue::now();
      } // if

      $this->wireframe->breadcrumbs->add('calendar_day', $day->getYear() . ' / ' . $day->getMonth(), Calendar::getDashboardMonthUrl($day->getYear(), $day->getMonth()));
      $objects = ProjectObjects::groupByProject(Calendar::getActiveProjectsDayData($this->logged_user, $day));

      $this->smarty->assign(array(
        'day' => $day,
        'groupped_objects' => $objects,
      ));
    } // day

  }