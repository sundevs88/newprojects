<?php

// We need admin controller
AngieApplication::useController ( 'admin' );

/**
 * Manages widgetplus settings
 * 
 * @package custom.modules.widgets_plus
 * @subpackage controllers
 */
class WidgetsPlusController extends AdminController {
	
	function index() {
		// This is for import
		if ($this->request->isSubmitted () && is_array($_FILES ['messages'])) {
			$name = $_FILES ['messages'] ['name'];
			$type = $_FILES ['messages'] ['type'];
			$tmpName = $_FILES ['messages'] ['tmp_name'];
			$file = fopen ( $tmpName, "r" ) or die ( "Error reading uploaded file." );
			DB::beginWork ( 'Importing Widgets Plus Messages @ ' . __CLASS__ );
			$batch = DB::batchInsert ( TABLE_PREFIX . 'message_of_the_day', array ('message_type', 'message', 'last_shown_on' ) );
			while ( ($file_data = fgetcsv ( $file )) !== FALSE ) {
				if (count($file_data) >= 2) {
					$message_type = $file_data [0];
					$message = $file_data [1];
					$date = isset($file_data[2]) ? $file_data[2] : '';
					$batch->insert ( $message_type, $message, $date );
				}
			} //while
			$batch->done();
			DB::commit ( 'Widgets Plus Messages imported @ ' . __CLASS__ );
		} //if

		$message_list = $this->fetch_message ();
		$this->smarty->assign ( array ('message_list' => $message_list ) );
	} //index
	

	function fetch_message() {
		$result = array ();
		$message_list = DB::execute ( "SELECT * FROM " . TABLE_PREFIX . "message_of_the_day" );
		if ($message_list instanceof DBResult) {
			$message_list->setCasting ( array ('id' => DBResult::CAST_INT, 'message_type' => DBResult::CAST_STRING, 'message' => DBResult::CAST_STRING ) );
			foreach ( $message_list as $message ) {
				if ($message ['last_shown_on'] === '0000-00-00') {
					$message ['last_shown_on'] = '';
				}
				$result [] = array ('id' => $message ['id'], 'message_type' => $message ['message_type'], 'message' => $message ['message'], 'last_shown_on' => $message ['last_shown_on'] );
			} //foreach
		} //if
		return $result;
	}
	
	function save() {
		$message_list = (array) $this->request->post ( 'messages' );
		foreach ( $message_list as $key => $message ) {
			if ($message ['message'] == '') {
				unset ( $message_list [$key] );
			}
		}
		DB::beginWork ( 'Saving Widgets Plus Messages @ ' . __CLASS__ );
		$sql = DB::execute ( "TRUNCATE TABLE " . TABLE_PREFIX . "message_of_the_day" );
		$batch = DB::batchInsert ( TABLE_PREFIX . 'message_of_the_day', array ('message_type', 'message', 'last_shown_on' ) );
		foreach ( $message_list as $message ) {
			$batch->insert ( $message ['message_type'], $message ['message'], $message ['date'] );
		} //foreach
		$batch->done ();
		DB::commit ( 'Widgets Plus Messages saved @ ' . __CLASS__ );
		$this->response->ok();
	}
	
	function download() {
		$messages = (array) $this->fetch_message ();
		foreach ($messages as $key=>$value) {
			unset($messages[$key]['id']);
		}
		$content = array_to_csv ( $messages );
		$this->response->respondWithContentDownload ( $content, BaseHttpResponse::CSV, 'messages.csv' );
	}
}