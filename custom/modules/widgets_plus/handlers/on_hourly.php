<?php

/**
 * widgets_plus module on_hourly event handler
 *
 * @package custom.modules.widgetplus
 * @subpackage handlers
 */

function widgets_plus_handle_on_hourly() {
	RssFeedHomescreenWidget::write_fetch_rss_feed ();
	
} // widgets_plus_handle_on_hourly