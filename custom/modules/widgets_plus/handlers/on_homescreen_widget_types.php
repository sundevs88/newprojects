<?php

/**
 *on_homescreen_widgets_types event handler
 * 
 * @package custom.modules.widgets_plus
 * @subpackage handlers
 *
 **
 * Handle on_homescreen_widget_types event
 * 
 * @param array $types
 * @param IUser $user
 */
function widgets_plus_handle_on_homescreen_widget_types(&$types, IUser &$user) {
	$types [] = new RssFeedHomescreenWidget ();
	$types [] = new MessagesHomescreenWidget ();
	$types [] = new RemotecontentHomescreenWidget ();
	$types [] = new TextHomescreenWidget ();
	$types [] = new NotesHomescreenWidget ();
} // widgets_plus_handle_on_homescreen_widget_types