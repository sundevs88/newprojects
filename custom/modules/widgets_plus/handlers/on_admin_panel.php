<?php

/**
 * on_admin_panel event handler
 * 
 * @package custom.modules.widgets_plus
 * @subpackage handlers
 */

/**
 * Handle on_admin_panel event
 * 
 * @param AdminPanel $admin_panel
 */
function widgets_plus_handle_on_admin_panel(AdminPanel &$admin_panel) {
	$admin_panel->addToTools ( 'widgets_plus', lang ( 'Widgets Plus Messages' ), Router::assemble ( 'widgets_plus' ), AngieApplication::getImageUrl ( 'module.png', WIDGETS_PLUS_MODULE ) );
} // widgets_plus_on_admin_panel