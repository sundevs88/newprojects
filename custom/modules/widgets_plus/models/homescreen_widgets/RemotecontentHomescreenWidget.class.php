<?php

/**
 * Widget_plus home screen widget implementation
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class RemotecontentHomescreenWidget extends HomescreenWidget {
	
	/**
	 * Return widget name
	 * 
	 * @return string
	 */
	function getName() {
		return lang ( 'Remote Content' );
	} // getName
	

	/**
	 * Return widget description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Show any webpage in an iframe' );
	} // getDescription
	

	/**
	 * Return group name for widgets of this type
	 * 
	 * @return string
	 */
	function getGroupName() {
		return lang ( 'Widgets Plus' );
	} // getGroupName
	

	/**
	 * Return widget title
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = null) {
		
		return "";
	
	} // renderTitle
	

	/**
	 * Return widget body
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
		$url = $this->getUrl ();
		$styles = array(); $style = 'style=\'';
		$styles["z-index"] = "999";
		if ($column_wrapper_class == SINGLE_COLUMN) {
			$styles["margin"] = "-58px -45px -50px -45px";
		}
		foreach ($styles as $key=>$value) {
			$style .= $key .": ".$value."; ";
		}
		$style .= '\'';
		$iframeDimensions = 'width="100%"';
		$iframeDimensions .= ($column_wrapper_class == SINGLE_COLUMN) ? ' height="700"' : ' height="200"';
		
		$result = '<div class="remote_content" '.$style.'><iframe src="' . $url . '" '.$iframeDimensions.' id="if' . $widget_id . '" ></iframe></div>';
		return $result;
	} // renderBody
	/**
	 * Returns true if this widget has additional options
	 * 
	 * @return boolean
	 */
	function hasOptions() {
		return true;
	} // hasOptions
	

	/**
	 * Render widget options form section
	 * 
	 * @param IUser $user
	 * @return string
	 */
	function renderOptions(IUser $user) {
		$view = SmartyForAngie::getInstance ()->createTemplate ( AngieApplication::getViewPath ( 'add_remote_content_url', 'homescreen_widgets', WIDGETS_PLUS_MODULE, AngieApplication::INTERFACE_DEFAULT ) );
		
		$view->assign ( array ('widget' => $this, 'user' => $user, 'widget_data' => array ('remote_url' => $this->getUrl () ) ) );
		
		return $view->fetch ();
	} // renderOptions
	

	/**
	 * Bulk set widget attributes
	 * 
	 * @param array $attributes
	 */
	function setAttributes($attributes) {
		
		if (array_key_exists ( 'remote_url', $attributes )) {
			$this->setUrl ( array_var ( $attributes, 'remote_url' ) );
		} // if
		parent::setAttributes ( $attributes );
	} // setAttributes
	

	/**
	 * Return value of remote url
	 * 
	 * @return string
	 */
	function getUrl() {
		return $this->getAdditionalProperty ( 'remote_url' );
	} // getUrl
	

	/**
	 * Set remote url propery value
	 * 
	 * @param  $value
	 * @return string
	 */
	function setUrl($value) {
		return $this->setAdditionalProperty ( 'remote_url', $value );
	} // setUrl
}
  