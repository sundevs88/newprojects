<?php

/**
 * Widget_plus home screen widget implementation
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class MessagesHomescreenWidget extends HomescreenWidget {
	
	/**
	 * Return widget name
	 * 
	 * @return string
	 */
	function getName() {
		return lang ( 'Message Of The Day' );
	} // getName
	

	/**
	 * Return widget description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Show a random message to users - e.g. tips, inspirational quotes' );
	} // getDescription
	

	/**
	 * Return group name for widgets of this type
	 * 
	 * @return string
	 */
	function getGroupName() {
		return lang ( 'Widgets Plus' );
	} // getGroupName
	

	function renderTitle(IUser $user, $widget_id, $column_wrapper_class = null) {
		return "";
	} // renderTitle
	

	/**
	 * Return widget body
	 * 
	 * @param IUser $user
	 * @param string $widget_id
	 * @param string $column_wrapper_class
	 * @return string
	 */
	function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
		$type = $this->getMessageType ();
        $cache_id = $this->getId(). '_message';
        $today_message = AngieApplication::cache()->get($cache_id);
        if (empty ( $today_message )) {
			$today_message = $this->fetch_message ();
		}
		return '<div class="messages ' . $type . '"><h3 class="head"><span class="head_inner">' . lang ( ucfirst ( $type ) ) . '</span></h3><p>' . $today_message . '</p></div>';
	
	} // renderBody
	/**
	 * Returns true if this widget has additional options
	 * 
	 * @return boolean
	 */
	function hasOptions() {
		return true;
	} // hasOptions
	

	/**
	 * Render widget options form section
	 * 
	 * @param IUser $user
	 * @return string
	 */
	function renderOptions(IUser $user) {
		$view = SmartyForAngie::getInstance ()->createTemplate ( AngieApplication::getViewPath ( 'add_inspiration_and_tricks', 'homescreen_widgets', WIDGETS_PLUS_MODULE, AngieApplication::INTERFACE_DEFAULT ) );
		
		$view->assign ( array ('widget' => $this, 'user' => $user, 'widget_data' => array ('extended' => $this->getMessageType () ) ) );
		
		return $view->fetch ();
	} // renderOptions
	

	/**
	 * Bulk set widget attributes
	 * 
	 * @param array $attributes
	 */
	function setAttributes($attributes) {
		
		if (array_key_exists ( 'extended', $attributes )) {
			$this->setMessageType ( array_var ( $attributes, 'extended' ) );
		} // if
		
		parent::setAttributes ( $attributes );
	} // setAttributes
	

	/**
	 * Return value of type property
	 * 
	 * @return string
	 */
	function getMessageType() {
		return $this->getAdditionalProperty ( 'message_type' );
	} // getExtended
	

	/**
	 * Set type propery value
	 * 
	 * @param  $value
	 * @return string
	 */
	function setMessageType($value) {
		return $this->setAdditionalProperty ( 'message_type', $value );
	} // setExtended
	

	function fetch_message() {
		
		$type = $this->getMessageType ();
		$date = DateValue::now ();
		$limit = 5;
		$today_messages = '';
		$inspiration_tricks_table = TABLE_PREFIX . 'message_of_the_day';
		
		$results = DB::execute ( "SELECT id, message, 
                                   CASE WHEN last_shown_on = CURDATE()
                                    THEN -1
								    ELSE IFNULL( unix_timestamp( last_shown_on ) , 0 )
									END AS last_shown_on
									FROM $inspiration_tricks_table
									WHERE message_type = ?
									ORDER BY last_shown_on ASC , rand()
									LIMIT 5 ", $type );
		
		$today_messages = array ();
		if ($results && $results->count () > 0) {
			$message_of_the_day = $results->toArray ();
			if (is_foreachable ( $message_of_the_day )) {
				foreach ( $message_of_the_day as $message ) {
					if ($message ['last_shown_on'] == - 1) {
						$today_messages ['message'] = $message ['message'];
					}
				}
			}
		}
		
		if (count ( $today_messages ) == 0) {
			$key = array_rand ( $message_of_the_day );
			$id = $message_of_the_day [$key] [id];
			$today_messages ['message'] = $message_of_the_day [$key] ['message'];
			$results = DB::execute ( "UPDATE  $inspiration_tricks_table SET `last_shown_on`= NOW() WHERE `id` = ? ", $id );
		}
		$today_message = implode ( '<br />', array_unique ( $today_messages ) );
		$cache_id = $this->getId() . '_message';
        AngieApplication::cache()->set($cache_id, $today_message);
        return $today_message;
	}

}
  