<?php

/**
 * Homescreen tab with single column
 * 
 * @package custom.modules.widgets_plus
 * @subpackage models
 */
class SingleHomescreenTab extends WidgetsHomescreenTab {
	
	/**
	 * This home screen tab does accepts widgets
	 *
	 * @var boolean
	 */
	protected $accept_widgets = true;
	
	/**
	 * Column definitions (none)
	 *
	 * @var array
	 */
	
	protected $columns = array (1 => SINGLE_COLUMN )

	;
	
	/**
	 * Return homescreen tab description
	 * 
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Single Column, Full Width' );
	} // getDescription


}