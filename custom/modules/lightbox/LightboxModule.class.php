<?php

/**
 * Lightbox module definition
 *
 * @package custom.modules.lightbox
 * @subpackage models
 */
class LightboxModule extends AngieModule {
	
	/**
	 * Plain module name
	 *
	 * @var string
	 */
	var $name = 'lightbox';
	
	/**
	 * Is system module flag
	 *
	 * @var boolean
	 */
	var $is_system = false;
	
	/**
	 * Module version
	 *
	 * @var string
	 */
	var $version = '4.1.1';
	
	/**
	 * Get module display name
	 *
	 * @return string
	 */
	function getDisplayName() {
		return lang ( 'Lightbox' );
	} // getDisplayName
	

	/**
	 * Return module description
	 *
	 * @param void
	 * @return string
	 */
	function getDescription() {
		return lang ( 'Adds lightbox for images, Flash, YouTube & Vimeo' );
	} // getDescription
	

	/**
	 * Return module uninstallation message
	 *
	 * @param void
	 * @return string
	 */
	function getUninstallMessage() {
		return lang ( 'Module will be deactivated.' );
	} // getUninstallMessage
}

?>