<?php

/**
 * Init lightbox module
 *
 * @package custom.modules.lightbox
 */

define ( 'LIGHTBOX_MODULE', 'lightbox' );
define ( 'LIGHTBOX_MODULE_PATH', CUSTOM_PATH . '/modules/lightbox' );

// acHack: Little hack to hijack the smarty helpers and route the same helpers through our modules
$smarty = SmartyForAngie::getInstance ();
$search_plus_smarty_plugin_dir = LIGHTBOX_MODULE_PATH . '/helpers/';
$plugin_dir = $smarty->getPluginsDir ();
array_unshift ( $plugin_dir, $search_plus_smarty_plugin_dir );
$smarty->setPluginsDir ( $plugin_dir );
// acHack: Over


?>