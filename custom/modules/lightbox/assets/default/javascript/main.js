App.Wireframe.Events.bind('single_content_updated single_content_loaded single_loaded comment_created comment_updated', function (event) {
    $("a[href*='http']").each(function() {
        if( $(this).attr('href').match(/lightbox=1|youtube\.com\/watch|vimeo\.com|\b\.mov\b|\b\.mp4\b|\b\.m4v\b|\b\.swf\b|\b\.pjpeg\b|\b\.jpeg\b|\b\.jpg\b|\b\.gif\b|\b\.png\b|\biframe=true\b/i)) {		
            $(this).attr("rel","prettyPhoto[my]");
        }
    });
        
    $("a[rel*='prettyPhoto[my]']").prettyPhoto({
        deeplinking : false, 
        default_width: 700, 
        default_height: 550, 
        allow_resize: true, 
        allow_expand: true
    });
});

App.Wireframe.Events.bind('single_content_updated single_content_loaded single_loaded new_file_version_created', function (event) {
    
    if ($("#page_title").text().match(/\b\.mov\b|\b\.mp4\b|\b\.m4v\b|\b\.swf\b|\b\.pjpeg\b|\b\.jpeg\b|\b\.jpg\b|\b\.gif\b|\b\.png\b/i) || $("img[src*='forward_thumbnail']").length > 0) {		
        $("a[href*='http']").each(function() {
            if( ($(this).attr('href').match(/disposition=attachment|versions/i) )  && !$(this).attr('href').match(/delete/i) ) {
                
                if ($(this).attr("title") != App.lang("Download")) {
                  
                    var filename = ($("#page_title").text().split('File')[1])?$("#page_title").text().split('File')[1] : $("#page_title").text().split('Document')[1];
                    if(this.href.match(/disposition=attachment/i)){
                        this.href = this.href.replace("disposition=attachment", "disposition=1&lightbox=1&name="+filename);
                    }else{
                        this.href  = this.href+"&disposition=1&lightbox=1&name="+filename;  
                    }
                    $(this).attr("rel","prettyPhoto[my]");
                }
                if ($(this).attr("title") == App.lang("Click to Download")) {
                    $(this).attr("title", "");
                }
            }
        });

         //removing a latest version file as they are duplicated (view image + latest version)
        if($('.file_versions').get(0)){
            // $('.file_versions span.latest_file_version').parent().parent().find('td.version a').attr("rel", "");

            $('div.file_preview a').on('click', function(){
                $('tr.latest td.version a').attr("rel", "");
                if($(this).attr("rel") == "")  $(this).attr("rel","prettyPhoto[my]");
            });

           $('tr.latest td.version a').on('click', function(){
                $('div.file_preview a').attr("rel", "");
                if($(this).attr("rel") == "")  $('tr.latest td.version a').attr("rel","prettyPhoto[my]");
            });
        }
      
        $("a[rel*='prettyPhoto[my]']").prettyPhoto({
            deeplinking : false, 
            default_width: 700, 
            default_height: 550, 
            allow_resize: true, 
            allow_expand: true
        });
    }
});