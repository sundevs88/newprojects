/**
 * Object attachments widgets behavior
 */
jQuery.fn.objectAttachments = function(s) {
  var settings = jQuery.extend({
    'attachments' : null 
  }, s);

  
  return this.each(function() {
    var wrapper = $(this).addClass('attachments');
   
    /**
     * Render the attachments
     * 
     * @param Array attachments
     */
    var render_attachments = function (attachments) {
      wrapper.find('ul.attachments_table').remove();
      var list = $('<ul class="attachments_table"></ul>').appendTo(wrapper);
      
      if (attachments && attachments.length) {
        $.each(attachments, function (index, attachment) {
          var row = $('<li class="attachment"></li>').attr('id', 'attachment_' + attachment['id']).appendTo(list);
         
          //acHack: For lightbox
          var relAttr = "";
          var img_url = App.extendUrl(attachment['urls']['view'] , {force : 1, name : attachment['name']});
          var dwonload_img_url = App.extendUrl(attachment['urls']['view'], {force : 1});
          var attachment_type = getFileType(img_url);
          var short_filename = App.clean(App.Wireframe.Utils.shortFilename(attachment['name']));
          var full_filename = App.clean(attachment['name']);
        
          if(attachment_type != 'none') {

        	  //We want to group attachment by it's parent
        	 //relAttr = ' rel="prettyPhoto[my]" ';
        	 //img_url = App.extendUrl(img_url, {disposition : 1, lightbox : 1});
             relAttr = "prettyPhoto[my]";
          } else {
              relAttr = "";
          }
          
          aTag = '<a href="' + img_url + '" target="_blank" title="'+full_filename+'"';
          if (relAttr != '') {
              aTag += ' rel="'+relAttr+'">';
          } else {
              aTag += '>';
          }
          
          
          //$('<a href="' + App.extendUrl(attachment['urls']['view'], {force : 1}) + '" target="_blank"><img src="' + attachment['preview']['icons']['large'] + '"> <span class="filename">' + App.clean(App.Wireframe.Utils.shortFilename(attachment['name'])) + '</span></a>').appendTo(row);
          $( aTag + '<img src="' + attachment['preview']['icons']['large'] + '"><span class="filename">' + short_filename + '</span></a>').appendTo(row);
        
          daTag = '<a href="' + dwonload_img_url + '" target="_blank" title="'+full_filename+'">';  //todo
          $( daTag + '<span class="filename" style="margin-top: -10px !important">' + App.lang('Download') + '</span></a>').appendTo(row);
          //acHack: Over
         
          if(typeof(attachment['options']) == 'object' && attachment['options']) {
            $('<span class="options"></span>').appendTo(row);
          } // if
        });
      } // if
      
      if(list.find('li.attachment').length < 1) {
        list.hide();
      } else {
        list.show();
      } // if
      
    
      
    }; // render_attachments
    
    if(typeof(settings['attachments']) == 'object' && settings['attachments']) {
      render_attachments(settings['attachments']);
    } // if
    
    // listen to parent updates
    if (settings.object.listen) {
      App.Wireframe.Events.bind(settings.object.listen + '.single', function (event, updated_object) {
        if (updated_object['id'] == settings.object['id'] && updated_object['class'] == settings.object['class']) {
          render_attachments(updated_object.attachments);
          // acHack: Apply lightbox
          $("a[href*='lightbox=1']").attr("rel","prettyPhoto[my]").prettyPhoto({deeplinking : false, default_width: 700, default_height: 550, allow_resize: true, allow_expand: true});
          // acHack: Over
        } // if
      });
    } // if
	
  });

  //acHack: find the attachment type
  function getFileType(itemSrc) {
  	 
		if (itemSrc.match(/youtube\.com\/watch/i) || itemSrc.match(/youtu\.be/i)) {
			return 'youtube';
		}else if (itemSrc.match(/vimeo\.com/i)) {
			return 'vimeo';
		// Changes made to support Non Latin file names. for image, quicktime, flash filetype.
		}else if(itemSrc.match(/\.(?:pjpeg|jpeg|gif|png|jpg)$/i)){   
			return 'image';
		}else if(itemSrc.match(/\.(?:mov|mp4|m4v)$/i)){   
			return 'quicktime';
                }else if(itemSrc.match(/\.(?:swf)$/i)){
			return 'flash';
		}else if(itemSrc.match(/\biframe=true\b/i)){
			return 'iframe';
		}else if(itemSrc.match(/\bajax=true\b/i)){
			return 'ajax';
		}else if(itemSrc.match(/\bcustom=true\b/i)){
			return 'custom';
		}else if(itemSrc.substr(0,1) == '#'){
			return 'inline';
		
		}else{
			return 'none';
		};
	};
//acHack: Over
  
};