<?php

  /**
   * SearchPlus module definition
   *
   * @package custom.modules.search_plus
   * @subpackage models
   */
  class SearchPlusModule extends AngieModule  {
    
    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'search_plus';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '4.1';
    
    /**
     * Define module routes
     *
     * @return null
     */
    function defineRoutes() {

      // Search
      	//Router::map('quick_backend_search', 'search/quick', array('controller' => 'backend_search', 'action' => 'quick_search')); Original Route
        Router::map('quick_backend_search', 'search/quick', array('controller' => 'search_plus', 'action' => 'quick_search'));
    }


    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Search Plus');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
      return lang('Enhances Search - better results, easier to use');
    } // getDescription
    
  }