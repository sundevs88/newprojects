<?php

 // We need backend controller
  AngieApplication::useController('fw_backend_search', SEARCH_FRAMEWORK);

/**
 * 
 * @package custom.modules.search_plus
 * @subpackage controllers
 * 
 */
  class SearchPlusController extends FwBackendSearchController {
  
    /**
     * activeCollab specific quick search implementation
     */
    function quick_search() {

    	// Asynchronous request
      if($this->request->isAsyncCall()) {
        $search_for = trim($this->request->get('q'));
        
        if($search_for) {
          $project_id = $this->request->get('project_id', null);
          
          $active_project =  "<script type='text/javascript'>App.Config.get('');</script>";
          $search_criterions = array( new SearchCriterion('short_name', SearchCriterion::IS, $search_for, SearchCriterion::EXTEND_RESULT) );

          if ($project_id) {
            $search_criterions[] = new SearchCriterion('item_context', SearchCriterion::LIKE, 'projects:projects/' . $project_id . '/%', SearchCriterion::FILTER_RESULT);
          } // if
          $result = Search::queryPaginated($this->logged_user, 'names', $search_for, $search_criterions, 1, 30);
          
          /*acHack: start */
          if($this->request->get('current_main_menu', null) !== null){ // We don't want to search comments when we add the link through editor.

            // TODO : Remove in future version
            // Not including the comments from the modules which are not installed. 
            $condition = '';
            $modules_with_comments = array('Task' => 'tasks',
                                           'NotebookPage' => 'notebooks', 
                                           'Quote' => 'invoicing', 
                                           'File' => 'files',
                                           'TextDocument' => 'files',
            							   'Bookmark' => 'files',
            							   'YouTubeVideo' => 'files', 
                                           'Discussion' => 'discussions',
            							   );
            foreach( $modules_with_comments as $type => $module){
              if(AngieApplication::isModuleLoaded($module)){
               unset($modules_with_comments[$type]);
              }
            }
            
            $condition = (count($modules_with_comments) > 0) ? DB::prepare(' parent_type NOT IN ( ? ) AND ', array_keys($modules_with_comments)) : '';

            $cp_visibility_condition = '';
            if(AngieApplication::isModuleLoaded('comments_plus') && !$this->logged_user->isAdministrator()){
              $cp_visibility = ($this->logged_user->canSeePrivate()) ? 0 : 1;

              $rows = DB::executeFirstColumn("SELECT CONCAT(`parent_type`, '_', `parent_id`) as parent_type_id  FROM `". TABLE_PREFIX . "confidential_comments_subscriptions` WHERE user_id = ?", $this->logged_user->getId());
              
              $confidential_condition = 0;
              if(isset($rows) && is_foreachable($rows)) {
              	$confidential_condition = DB::prepare("CONCAT(`parent_type`, '_', `parent_id`) IN ( ? )", $rows);
              }
              
              $cp_visibility_condition = " ((cp_visibility = -1 AND {$confidential_condition} ) OR (cp_visibility >=  {$cp_visibility} )) AND ";
            }

            //Getting comments based on the search string
            $query = DB::prepare("SELECT * FROM ". TABLE_PREFIX . "comments WHERE {$condition} {$cp_visibility_condition} state > ? AND body LIKE ? ", STATE_TRASHED, "%". substr($search_for, 0, -1) . "%" );
            
            $comments = Comments::findBySQL($query);
            // Creating array structure of the comments, and append them in the results.
            if ($comments instanceof DBResult && $comments->count() > 0) {
                foreach ($comments as $comment) {
                  $parent = $comment->getParent();
                  $result_comments = array();
                  if($comment instanceof Comment && ($parent instanceof IComments && $parent->canView($this->logged_user)) ){
                      $name = strip_tags($comment->getBody());
                      $result_comments = array('id' => $comment->getId(),
                                               'type' => $comment->getVerboseType(true),
                                               'verbose_type' => $comment->getVerboseType(),
                                               'name' => strlen($name) > 50 ? substr($name, 0, 47) . '...' : $name, 
                                               'permalink' => $parent->getViewUrl(),
                                               'data_object' => $comment,
                                               'comment_body' => $name  // passing comment body 
                                               );

                      //Adding a comments and count to the total Hits.
                      $result[0][] = $result_comments;
                      $result[1] ++;
                  }
                } //foreach

            } // if : comments
          }
          /*acHack: end */
        } else {
          $result = array();
        } // if

        $this->response->respondWithData($result, array('as' => 'search_results'));
        
      // Request made by phone device
      } elseif($this->request->isPhone()) {
      	$search_for = trim($this->request->post('q'));

        if($search_for) {
          $project_id = $this->request->get('project_id', null);
          $search_criterions = array( new SearchCriterion('short_name', SearchCriterion::IS, $search_for, SearchCriterion::EXTEND_RESULT) );

          if ($project_id) {
            $search_criterions[] = new SearchCriterion('item_context', SearchCriterion::LIKE, 'projects:projects/' . $project_id . '/%', SearchCriterion::FILTER_RESULT);
          } // if

          $result = Search::query($this->logged_user, 'names', $search_for, $search_criterions);
        } else {
          $result = array();
        } // if
        
        $this->response->assign(array(
        	'search_for' => $search_for,
        	'search_results' => $result
        ));
      	
      } else {
        $this->response->badRequest();
      } // if
    } // quick_search
    
  }