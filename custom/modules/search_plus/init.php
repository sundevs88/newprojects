<?php

  /**
   * Init search_plus module
   *
   * @package custom.modules.search_plus
   */
  
  define('SEARCH_PLUS_MODULE', 'search_plus');
  define('SEARCH_PLUS_MODULE_PATH', CUSTOM_PATH . '/modules/search_plus');

 AngieApplication::setForAutoload(array('ISearchItemImplementation' => SEARCH_PLUS_MODULE_PATH . '/models/ISearchItemImplementation.class.php'));
  
 