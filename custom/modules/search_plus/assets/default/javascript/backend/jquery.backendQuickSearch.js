App.search_plus = {
    controllers : {},
    models      : {}
};

/**
 * Quick backend search
 */
jQuery.fn.backendQuickSearch = function (s) {
    
    
  // Settings

    var settings = jQuery.extend({
        search_url: "",
        quick_search_url: "",
        quick_search_delay: 500
    }, s);
    return this.each(function () {
        var wrapper = $(this);
        var form = $('<form action="' + App.clean(settings.quick_search_url) + '"><input id="search_for" type="text" name="q" autocomplete="off" placeholder="' + App.lang("Search") + '"></form>').appendTo(wrapper);
        var input = form.find("input[name=q]");
        var popup = $('<div id="global_search_autocomplete" class="empty" style="display: none"><div id="global_search_autocomplete_results"><div class="message">' + App.lang("Type the search term in the field above to search") + '</div></div><div id="global_search_autocomplete_advanced">' + App.lang('Not finding what you are looking for? Try <a href=":url">advanced search</a>.', {
            url: settings.search_url
        }) + "</div></div>").appendTo(wrapper);
        var advanced_search = $("#global_search_autocomplete_advanced a");
        advanced_search.click(function () {
            hide_popup()
        }).flyout({
            width: 800,
            title: App.lang("Advanced Search"),
            href: function () {
                var search_for = jQuery.trim(input.val());
                if (search_for) {
                    return App.extendUrl(settings.search_url, {
                        search_for: search_for
                    })
                } else {
                    return settings.search_url
                }
            }
        });
        var results = popup.find("#global_search_autocomplete_results");
        // Variables and flags
        var search_delayed_timer = false;
        /**
        * Execute search
        */
        var search = function () {
            if (search_delayed_timer) {
                clearTimeout(search_delayed_timer)
            }
            //achack: Start
            var search_for = jQuery.trim(input.val());
            if (search_for.length < 2) {
                return;
            }
            if(search_for) {
                $('#search_for').attr('search_for', search_for);
                search_for += '*';
            }
            //acHack: Over

            // Execute search
            if (search_for) {
                popup.addClass("searching");
                results.empty().append('<div class="message"><img src="' + App.Wireframe.Utils.indicatorUrl() + '"></div>');
                var current_menu = App.Wireframe.MainMenu.getCurrent();
                $.ajax({
                    url: App.extendUrl(form.attr("action"), {
                        async: 1,
                        q: search_for,
                        current_main_menu : current_menu
                    }),
                    type: "get",
                    success: function (response) {

                        if (jQuery.isArray(response) && response.length == 2) {
                            var search_results = response[0];
                            var total_hits = response[1];
                            results.empty();
                            if (total_hits == 0) {
                                results.append('<div class="message">' + App.lang("Search returned no results. Please try a different term or use advanced search.") + "</div>")
                            } else {
                                var table = $('<table class="search_results common" cellspacing="0"><tbody></tbody></table>').appendTo(results);
                                var tbody = table.find("tbody");
                                var counter = 1;

                                //acHack: Start
                                var typearr = [];
                                for(var i=0; i < search_results.length; i++) {
                                  var result = search_results[i];
                                  var type = search_results[i]['verbose_type'];
                                  var url = search_results[i]['permalink'];
                                  var name = search_results[i]['name'];
                                  switch(type) {
                                   case 'User':
                                      //var short_var = search_results[i]['email'];
                                      var short_var = '';
                                      break;
                                    case 'Task':
                                      var short_var = '#' + search_results[i]['id'];
                                      break;
                                    default:
                                      var short_var = '';
                                  } // switch
                                  
                                  if((jQuery.inArray(type, typearr)) === -1){
                                      typearr.push(type);
                                      var row = $('<tr popups_position="left" class="headingRow search_result search_result_first'+type+'" search_result_url="' + App.clean(url) + '"><td class="heading type">'+type+'</td><td class="name">' + App.clean(name) + '</td><td class="short_name">' + App.clean(short_var) + '</td></tr>').appendTo(tbody);
                                  } else {
                                      var parentRow = $('tr.search_result_first'+ type, table);
                                      var row = $('<tr popups_position="left"  class="search_result search_result_'+type+'" search_result_url="' + App.clean(url) + '"><td class="type"></td><td class="name">' + App.clean(name) + '</td><td class="short_name">' + App.clean(short_var) + '</td></tr>').insertAfter(parentRow);
                                  }
                                  
                                  if(typeof(type) != 'undefined' && jQuery.inArray(type, ['Project', 'Milestone', 'Task', 'TodoList', 'File', 'Discussion', 'Comment']) >= 0 && (App.compareVersions(App.Config.get('application_version'), "3.3.0") == 1)) {

                                      // Commnets title needs to be render differently
                                      var title = (type != 'Comment') ? (type + ': '+ search_results[i]['name']) : (search_results[i]['data_object']['name'] + '  [' + search_results[i]['data_object']['parent_class'] + ']');
                                      row.contextPopup({
                                          // here we are setting a value as an id but internally it sets as a class
                                          'id': 'quick_search_popup',
                                          'triggerEvent' : 'mouseover',
                                          'title' : title,
                                          'autoClose' : true,
                                           
                                          'data': function(){
                                            return render_dialog(search_results);
                                          },
                                          callback : function(){
                                             var w =  $('#context_popup_container').width();
                                             var p = parseInt($('#context_popup_container').css('padding-left'));
                                             $('#context_popup_tip').css('margin-left' , w+(2*p));
                                             // $('#context_popup_container').css('padding-left');
                                          }
                                        });
                                      
                                      row.bind('mouseout', function(){
                                          $('#context_popup').remove();
                                          if (typeof(this.contextpopup) != 'undefined') {
                                            this.contextpopup.instance = null;
                                          }
                                      });
                                      
                                      row.bind('mouseover', function(){
                                          var selected_result = results.find('table.search_results tbody tr.selected');
                                          selected_result.removeClass('selected');
                                          $(this).addClass('selected');
                                      });
                                  } else {
                                      $('#context_popup').hide();
                                  }
                                
                                  //acHack: Over 
                                
                                  if(counter == 1) {
                                      row.addClass('selected');
                                  } // if
                                  row.click(function (e) {
                                      if (e.shiftKey) {
                                          var anchor = $('<a class="quick_view_item" href="' + url + '">' + App.clean(name) + "</a>");
                                          clear_results();
                                          hide_popup();
                                          App.widgets.QuickView.preview(anchor, true)
                                      } else {
                                          go_to_result($(this).attr("search_result_url"))
                                      }
                                  });
                                  counter++
                                }
                            }
                        } else {}
                    },
                    error: function () {
                        clear_results();
                        App.Wireframe.Flash.error("Failed to execute your search query. Please try again later")
                    }
                })
            } else {
                clear_results()
            }
        }; //search

        /*
     * acHack: Start
     * Render Dialog box for the different type of object
     * 
     */
    var render_dialog = function(search_results){
        var counter = 1;
        var hoverItem = results.find('table.search_results tbody tr:hover');
        var url = hoverItem.attr('search_result_url');
        var view = '';
        for(var i=0; i < search_results.length; i++) {
          if(search_results[i]['permalink'] == url){
              var view = '<table>';
              switch(search_results[i]['verbose_type']) {
              case 'User':
                     break;
              case 'Project':
                  if(jQuery.trim(search_results[i]['data_object']['overview']) != '' ) {
                    view +='<tr><td>' + search_results[i]['data_object']['overview'] + '</td></tr>';
                  } else {
                    view +='<tr><td>' + App.lang('No overview available for this project') + '</td></tr>';
                  }
                 break;
              case 'Comment':
                  if(jQuery.trim(search_results[i]['comment_body']) != '' ) {
                    view +='<tr><td>' + search_results[i]['comment_body'] + '</td></tr>';
                  } else {
                    view +='<tr><td>' + App.lang('No description available') + '</td></tr>';
                  }
                 break;
              case 'Milestone': 
              case 'Task':
              case 'TodoList':
              case 'Discussion':
              case 'File':  
                  if(jQuery.trim(search_results[i]['data_object']['body']) != '' ) {
                    view +='<tr><td>' + search_results[i]['data_object']['body'] + '</td></tr>';
                  } else {
                    view +='<tr><td>' + App.lang('No description available') + '</td></tr>';
                  }
                 break;
              default :
                  view +='<tr><td>' + App.lang('No description available') + '</td></tr>';
                 break;
              }
          }
           counter++;
        }
        return view + '</table>';
     }
  //acHack: Over
    
    /**
     * Set search timer
     */


        var search_delayed = function () {
            if (search_delayed_timer) {
                clearTimeout(search_delayed_timer)
            }
            search_delayed_timer = setTimeout(search, settings.quick_search_delay)
        };
        var clear_results = function () {
            popup.addClass("empty");
            results.empty().append('<div class="message">' + App.lang("Type the search term in the field above to search") + "</div>")
        };
        var next_result = function () {
            var selected_result = results.find("table.search_results tbody tr.selected");
            if (selected_result.length == 1) {
                var next_result = selected_result.next("tr.search_result");
                if (next_result.length == 1) {
                    selected_result.removeClass("selected");
                    next_result.addClass("selected")
                }
            }
        };
        var prev_result = function () {
            var selected_result = results.find("table.search_results tbody tr.selected");
            if (selected_result.length == 1) {
                var prev_result = selected_result.prev("tr.search_result");
                if (prev_result.length == 1) {
                    selected_result.removeClass("selected");
                    prev_result.addClass("selected")
                }
            }
        };
        var open_current_link = function (preview) {
            var selected_result = results.find("table.search_results tbody tr.selected");
            if (selected_result.length > 0) {
                if (preview) {
                    var anchor = $('<a class="quick_view_item" href="' + selected_result.attr("search_result_url") + '">' + selected_result.find("td.name").text() + "</a>");
                    clear_results();
                    hide_popup();
                    App.widgets.QuickView.preview(anchor, true)
                } else {
                    go_to_result(selected_result.attr("search_result_url"))
                }
            }
        };
        var go_to_result = function (url) {
            App.Wireframe.Content.setFromUrl(url);
            //achack:Start
            // clear_results();
            //acHack: Over
            hide_popup()
        };
        var show_popup = function () {
         
            setTimeout(function () {
                $(window).bind("click.global_search", function (event) {
                    if (!event.target) {
                        return true
                    }
                    var target = $(event.target);
                    if (!target.parents("#global_search_autocomplete").length && !target.is(input)) {
                        hide_popup()
                    }
                })
            }, 100);
            popup.show();
            //acHack:start - to retain result 
            //clear_results();
            //acHack: Over
            input.addClass("active")
        };
        /**
         * Hide autocompletion popup
         */
        var hide_popup = function () {
            $(window).unbind("click.global_search");
            popup.hide();
            $('#context_popup').hide();
            input.removeClass("active").val("").blur()
        };// hide_popup

        // show popup on focusing the input field
        input.focus(function () {
            //acHack: Start
            var search_for = $('#search_for').attr('search_for');
            input.val(search_for);
            //acHack: Over
            show_popup();
        });
        var search_value = "";
        input.keydown(function (event) {
            switch (event.keyCode) {
            case 40:
                next_result();
                return false;
                break;
            case 38:
                prev_result();
                return false;
                break;
            case 9:
                hide_popup();
                return true;
                break;
            case 27:
                hide_popup();
                return false;
                break;
            case 13:
                if (event.shiftKey) {
                    open_current_link(true)
                } else {
                    open_current_link()
                }
                return false;
                break;
            default:
                setTimeout(function () {
                    if (input.val() != search_value) {
                        search_delayed();
                        search_value = input.val()
                    }
                }, 30);
                break
            }
        })
    })
};