<?php

  /**
   * Init quick_add_plus module
   *
   * @package custom.modules.quick_add_plus
   */
  
  define('QUICK_ADD_PLUS_MODULE', 'quick_add_plus');
  define('QUICK_ADD_PLUS_MODULE_PATH', CUSTOM_PATH . '/modules/quick_add_plus');
 
  AngieApplication::setForAutoload(array(
    'QuickAddPlus' => QUICK_ADD_PLUS_MODULE_PATH . '/models/QuickAddPlus.class.php',
  ));
  
  
  $is_quick_add_plus_settings_exists = ConfigOptions::exists('quick_add_plus_settings');
  if(!$is_quick_add_plus_settings_exists) {
  	$quick_add_plus_settings = array('object_type' => 'both');
  	ConfigOptions::addOption('quick_add_plus_settings', QUICK_ADD_PLUS_MODULE, $quick_add_plus_settings);
  }