<?php return array(
	'Quick Add Plus',
	'Quick Add screen enhancements. Add SubTasks or track time or track expense for specific Task, not just a project.',
	'Subtask',
	'There is no Task. Can not create new SubTask.',
	'Add Subtask to the Task',
	'Add Subtask to the :name Project',
	'No Milestone',
	'Value is required',
	'Select task type',
	'Select Parent',
	'Selected type of tasks will be available in select parent dropdown while adding subtask, time entry & expense',
	'Settings',
	'Save Changes',
); ?>