<?php

  /**
   * QuickAddPlus module definition
   *
   * @package custom.modules.quick_add_plus
   * @subpackage models
   */
  class QuickAddPlusModule extends AngieModule  {
    
    /**
     * Plain module name
     *
     * @var string
     */
    protected $name = 'quick_add_plus';
    
    /**
     * Module version
     *
     * @var string
     */
    protected $version = '4.1.1';
    
    // ---------------------------------------------------
    //  Events and Routes
    // ---------------------------------------------------
    
    /**
     * Define module routes
     */
    function defineRoutes() {
    	Router::map('quick_add_subtask', 'quick_add_subtask', array('controller' => 'quick_add_plus', 'action' => 'quick_add_subtask'));
    	Router::map('quick_add_time_records_add', 'quick_add_time_record', array('controller' => 'quick_add_plus', 'action' => 'quick_add_time_record'));
    	Router::map('quick_add_expenses_add', 'quick_add_tracking_expenses', array('controller' => 'quick_add_plus', 'action' => 'quick_add_tracking_expenses'));
    	Router::map('quick_add_plus_admin', 'admin/quick-add-plus', array('controller' => 'quick_add_plus_admin', 'action' => 'index'));
    } // defineRoutes
    
    /**
     * Define event handlers
     */
    function defineHandlers() {
    	EventsManager::listen('on_quick_add', 'on_quick_add');
    	EventsManager::listen('on_admin_panel', 'on_admin_panel');
    } // defineHandlers
    
   
    /**
     * Get module display name
     *
     * @return string
     */
    function getDisplayName() {
      return lang('Quick Add Plus');
    } // getDisplayName
    
    /**
     * Return module description
     *
     * @return string
     */
    function getDescription() {
      return lang('Quick Add screen enhancements. Add SubTasks or track time or track expense for specific Task, not just a project.');
    } // getDescription
    
    /**
     * Return module uninstallation message
     *
     * @return string
     */
    function getUninstallMessage() {
      return lang('Module will be deactivated.');
    } // getUninstallMessage
    
  }
