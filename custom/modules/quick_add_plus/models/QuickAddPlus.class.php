<?php

  /**
   * QuickAddPlus class
   *
   * @package activeCollab.modules.quick_add_plus
   * @subpackage models
   */
  class QuickAddPlus {

  	
		
  	/**
  	 * Get a list of tasks orderby task name for user.
  	 *
  	 * @param User $user
  	 * @return array
  	 */
  	static function getProjectObjectsIdNameMap ($project_id, $map_field, $order_by, $sort_order = 'ASC') {
	  	if($project_id != '') {
	  		  $quick_add_plus_data = ConfigOptions::getValue('quick_add_plus_settings');
	  		  $type = trim($quick_add_plus_data['object_type']);
	  		  $query = 'SELECT id, milestone_id, name FROM ' . TABLE_PREFIX . 'project_objects WHERE project_id = ? AND type = ? AND state >= ? ';
	  		  
	  		  if($map_field == 'Task') {
		  		  if($type == 'active') {
		  		  	$query .= 'AND completed_on IS NULL ';
		  		  } elseif ($type == 'completed') {
		  		  	$query .= 'AND completed_on IS NOT NULL ';
		  		  }
	  		  }	  
	  		  
	  		  $query .= 'ORDER BY ' . $order_by . ' ' . $sort_order;
	  		  $rows = DB::execute($query, $project_id, $map_field,STATE_VISIBLE);
			  
	          if($rows instanceof DBResult) {
		      		$result = array();
			        foreach($rows as $row) {
			          $result[(integer) $row['id']] = array('name' => $row['name'], 'milestone_id' => (integer) $row['milestone_id']);
			        } // foreach
			        
		        	return $result;
		      } // if
	   	}
	   	
	   	return null;
  	}
  	
  }