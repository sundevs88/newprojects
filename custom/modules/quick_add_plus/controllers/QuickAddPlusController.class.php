<?php

// We need backend controller
AngieApplication::useController('backend');

/**
 * 
 * 
 * @package appsmagnet.modules.quick_add_plus
 * @subpackage controllers
 * 
 */
class QuickAddPlusController extends BackendController {

    var $active_subtask;
    var $active_project;
	var $default_billable_status;
    function __construct($request) {
        parent::__construct($request);

        $params = $this->request->getUrlParams();
        $this->active_project = Projects::findBySlug(array_var($params, 'project_slug'));
        $milestone_id_name_map = $tasks_grouped_by_milestone = array();
        if ($this->active_project instanceof Project) {
            $milestone_id_name_map = QuickAddPlus::getProjectObjectsIdNameMap($this->active_project->getId(), 'Milestone', 'name');
            $milestone_id_name_map [0] = array('name' => 'No Milestone');

            $tasks = QuickAddPlus::getProjectObjectsIdNameMap($this->active_project->getId(), 'Task', 'name');

            if (is_foreachable($tasks)) {
                if (is_foreachable($milestone_id_name_map)) {
                    foreach ($milestone_id_name_map as $milestone_id => $milestone) {
                        foreach ($tasks as $task_id => $task) {
                            if ($task['milestone_id'] == $milestone_id) {
                                $tasks_grouped_by_milestone[$milestone_id][$task_id] = $task['name'];
                            }
                        }
                    }
                }
            }
                if(AngieApplication::isModuleLoaded('tracking') && method_exists($this->active_project->tracking(), 'getDefaultBillableStatus')){
                    $this->default_billable_status = $this->active_project->tracking()->getDefaultBillableStatus() ;
                }else{
                    $this->default_billable_status = BILLABLE_STATUS_BILLABLE;
                }
	       $this->smarty->assign (array('active_project' => $this->active_project,
                'tasks_grouped_by_milestone' => $tasks_grouped_by_milestone,
                'milestone_id_name_map' => $milestone_id_name_map
            ));
        }
    }// __construct

    function quick_add_subtask() {
        $task = new Task ();
        $task->setProject($this->active_project);
        $this->active_subtask = $task->subtasks()->newSubtask();
        if ($this->request->isSubmitted()) {
            try {
                DB::beginWork('Creating new subtask @ ' . __CLASS__);

                $subtask_data = $this->request->post('subtask');

                $this->active_subtask->setAttributes($subtask_data);
                $this->active_subtask->setCreatedBy($this->logged_user);
                $this->active_subtask->setState(STATE_VISIBLE);
                $this->active_subtask->setPosition(Subtasks::nextPositionByParent($this->active_project));
                $this->active_subtask->save();

               $subscribers = array($this->logged_user->getId());

              $assignee = $this->active_subtask->assignees()->getAssignee();
              if ($assignee instanceof IUser && !$assignee->is($this->logged_user)) {
                $subscribers[] = $assignee;
              } // if
              
              $this->active_subtask->subscriptions()->set($subscribers);

              if($this->active_object instanceof ISubscriptions) {
                AngieApplication::notifications()
                  ->notifyAbout(SUBTASKS_FRAMEWORK_INJECT_INTO . '/new_subtask', $this->active_object, $this->logged_user)
                  ->setSubtask($this->active_subtask)
                  ->sendToGroupsOfUsers(array($this->active_object->subscriptions()->get(), $this->active_subtask->subscriptions()->get()));
              } // if
              

                DB::commit('Subtask created @ ' . __CLASS__);

                if ($this->request->isPageCall()) {
                    $this->flash->success('Subtask has been created');
                    $this->response->redirectToUrl($task->getViewUrl());
                } else {
                    $this->response->respondWithData($this->active_subtask, array('as' => 'subtask', 'detailed' => true));
                } // if
            } catch (Exception $e) {
                DB::rollback('Failed to save subtask @ ' . __CLASS__);

                if ($this->request->isPageCall()) {
                    $this->smarty->assign('errors', $e);
                } else {
                    $this->response->exception($e);
                } // if
            } // try
        }

        $this->smarty->assign(array('quick_add_subtask_url' => Router::assemble('quick_add_subtask', array('project_slug' => $this->active_project->getSlug())),
            'task' => $task,
            'subtask' => $this->active_subtask,
        ));
    }

    function quick_add_time_record() {
        
        $default_record_data = array ('user_id' => $this->logged_user->getId (), 'record_date' => DateValue::now (),  'billable_status' => $this->default_billable_status );
        $time_record_data = $this->request->post('time_record', $default_record_data);

        if ($this->request->isSubmitted()) {
            $time_record_data = $this->request->post('time_record');

            if (array_var($time_record_data, 'parent_id') != '') {
                $this->active_tracking_object = new Task ();
                $this->active_tracking_object->load(array_var($time_record_data, 'parent_id'));
            } else {
                $this->active_tracking_object = $this->active_project;
            }
            if ($this->active_tracking_object->tracking()->canAdd($this->logged_user)) {
                $this->response->assign('time_record_data', $time_record_data);

                if ($this->request->isSubmitted()) {
                    try {
                        DB::beginWork('Creating time record @ ' . __CLASS__);

                        $time_record_data ['value'] = isset($time_record_data ['value']) ? time_to_float($time_record_data ['value']) : 0;
                        if (empty($time_record_data ['value']) || $time_record_data ['value'] < 0) {
                            throw new ValidationErrors(array('value' => lang('Value is required')));
                        } // if
                        $this->active_time_record = new TimeRecord ();
                        $this->active_time_record->setAttributes($time_record_data);
                        $this->active_time_record->setCreatedBy($this->logged_user);

                        $this->active_time_record->setState(STATE_VISIBLE);

                        if ($this->active_time_record->getParent() == null) {
                            $this->active_time_record->setParent($this->active_tracking_object);
                        } // if
                        $this->active_time_record->save();

                        DB::commit('Time record created @ ' . __CLASS__);

                        if($this->request->isPageCall()) {
                            $this->response->redirectToUrl($this->active_time_record->getViewUrl());
                          } else {
                            $this->response->respondWithData($this->active_time_record, array(
                            'as' => 'time_record', 
                              'detailed' => true, 
                          ));
                  } // if
                    } catch (Exception $e) {
                        DB::rollback('Failed to create time record @ ' . __CLASS__);

                        if ($this->request->isPageCall()) {
                            $this->smarty->assign('errors', $e);
                        } else {
                            $this->response->exception($e);
                        } // if
                    } // try
                } // if
            }
        }

        $this->smarty->assign(array('quick_add_time_record_url' => Router::assemble('quick_add_time_records_add', array('project_slug' => $this->active_project->getSlug())),
            'time_record_data' => $time_record_data
        ));
    }

    function quick_add_tracking_expenses() {
    $expense_data = $this->request->post ( 'expense', array ('category_id' => ExpenseCategories::getDefaultId (), 'user_id' => $this->logged_user->getId (), 'record_date' => DateValue::now (),  'billable_status' => $this->default_billable_status  ) );		
        if ($this->request->isSubmitted()) {
            if (array_var($expense_data, 'parent_id') != '') {
                $this->active_tracking_object = new Task ();
                $this->active_tracking_object->load(array_var($expense_data, 'parent_id'));
            } else {
                $this->active_tracking_object = $this->active_project;
            }

            if ($this->active_tracking_object->tracking()->canAdd($this->logged_user)) {

                $this->response->assign('expense_data', $expense_data);

                if ($this->request->isSubmitted()) {
                    try {
                        DB::beginWork('Creating expense @ ' . __CLASS__);
                        $this->active_expense = new Expense ();
                        $this->active_expense->setAttributes($expense_data);
                        $this->active_expense->setCreatedBy($this->logged_user);

                        $this->active_expense->setState(STATE_VISIBLE);

                        if ($this->active_expense->getParent() == null) {
                            $this->active_expense->setParent($this->active_tracking_object);
                        } // if 


                        $this->active_expense->save();

                        DB::commit('Expense created @ ' . __CLASS__);

                        if ($this->request->isPageCall()) {
                            $this->response->redirectToUrl($this->active_expense->getViewUrl());
                        } else {
                            $this->response->respondWithData($this->active_expense, array('as' => 'expense', 'detailed' => true));
                        } // if
                    } catch (Exception $e) {
                        DB::rollback('Failed to create expense @ ' . __CLASS__);
                    } // try
                } // if
            }
        } // if


        $this->smarty->assign(array(
            'quick_add_tracking_expenses_url' => Router::assemble('quick_add_expenses_add', array('project_slug' => $this->active_project->getSlug())),
            'expense_data' => $expense_data,
        ));
    }

}