<?php

  /**
   * select_parent helper implementation
   * 
   * @package custom.modules.quick_add_plus
   * @subpackage helpers
   */

  /**
   * Render select parent user widget
   *
   * @param array $params
   * @param Smarty $smarty
   * @return string
   */
  function smarty_function_select_parent($params, &$smarty) {
  
    $items = array_var($params, 'items', true);
    $group_id_name_map = array_var($params, 'group_id_name_map', true);
    $project = array_var($params, 'project', true);
    $include_project = array_var($params, 'include_project', false);
    $user = array_required_var($params, 'user', true, 'IUser');
    
    if(isset($params['class'])) {
      $params['class'] .= ' select_user';
    } else {
      $params['class'] = 'select_user';
    } // if
    
    if(empty($params['id'])) {
      $attributes['id'] = HTML::uniqueId('select_parent');
    } // if
    
    $attributes['label'] = $params['label'];
      
    $name = array_var($params, 'name', null, true);  
    $value = array_var($params, 'value', null, true);
    $options = array();
    
    if(is_foreachable($group_id_name_map)) {
	    foreach ($group_id_name_map as $id => $milestone) {
	    	if(is_foreachable($items[$id])) {
	    		$objects = array();
	    		foreach ($items[$id] as $task_id => $task) {
	    			$objects[] = option_tag($task, $task_id, array('selected' => $task_id == $value));
	    		}
	    	
	    		$options[] = option_group_tag($milestone['name'], $objects, $attributes);
	    	}
	    }	
    } // if
    if($include_project) {
    	if($project instanceof Project) {
	    	array_unshift($options, option_tag($project->getName(), '',  array('selected' => $value == '',)));
    	}	
    }
    
    return HTML::select($name, $options, $attributes);
    
  } // smarty_function_select_parent