{title}Log Time{/title}
{add_bread_crumb}Log Time{/add_bread_crumb}

<div id="add_time_record">
  {form action=$quick_add_time_record_url method=post}
    <div class="fields_wrapper">
	        {wrap field=parent_id}
                {select_parent name='time_record[parent_id]' value='$time_record.parent_id' label='Select Parent' items=$tasks_grouped_by_milestone group_id_name_map=$milestone_id_name_map project=$active_project user=$logged_user optional=false required=true include_project=true }
            {/wrap}
                    
		    {wrap field=value}
		    	{text_field name='time_record[value]' value=$time_record_data.value class=short label='Hours' required=yes} {lang}of{/lang} {select_job_type name='time_record[job_type_id]' value=$time_record_data.job_type_id user=$logged_user required=true}
		    	<span class="details block">{lang}Possible formats: 3:30 or 3.5{/lang}</span>
		    {/wrap}
	  
		    {wrap field=summary}
		      {text_field name='time_record[summary]' value=$time_record_data.summary label="Summary"}
		    {/wrap}
	  
		    {wrap field=record_date}
		    	{select_date name='time_record[record_date]' value=$time_record_data.record_date label="Date" required=true}
		    {/wrap}

	      {wrap field=record_date}
	      	{select_project_user name='time_record[user_id]' value=$time_record_data.user_id label="User" project=$active_project user=$logged_user optional=false required=true}
	      {/wrap}
		    
		    {wrap field=billable_status}
		      {select_billable_status name='time_record[billable_status]' value=$time_record_data.billable_status label='Is Billable?'}
		    {/wrap}
    </div>
  
    {wrap_buttons}
    	{submit}Log Time{/submit}
    {/wrap_buttons}
  {/form}
</div>
 