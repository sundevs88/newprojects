<?php

  /**#**#**
   * New bookmark notification
   *
   * @package activeCollab.modules.files
   * @subpackage notifications
   *|*|*|*/
  class NewBookmarkNotification extends Notification {

    /*#*#*#*
     * Return notification message
     *
     * @param IUser $user
     * @return string|void
     */
    function getMessage(IUser $user) {
      return lang("Bookmark ':name' has been Added", array(
        'name' => $this->getParent() instanceof Bookmark ? $this->getParent()->getName() : '',
      ), true, $user->getLanguage());
    } // getMessage

  }