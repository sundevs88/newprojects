<?php

  /**#**#**
   * New YouTube video notification
   *
   * @package activeCollab.modules.files
   * @subpackage notifications
   *|*|*|*/
  class NewYouTubeVideoNotification extends Notification {

    /*#*#*#*
     * Return notification message
     *
     * @param IUser $user
     * @return string
     *?*?*?*/
    function getMessage(IUser $user) {
      return lang("YouTube Video ':name' has been added", array(
        'name' => $this->getParent() instanceof YouTubeVideo ? $this->getParent()->getName() : '',
      ), true, $user->getLanguage());
    } // getMessage

  }