{add_bread_crumb}Details{/add_bread_crumb}

{object object=$active_asset user=$logged_user show_body=false}
  <div class="wireframe_content_wrapper"> 
  
    <div class="project_asset_youtube_video_preview">
      <div class="real_preview">
        {$active_asset->preview()->renderLarge() nofilter}
      </div>
      
	    <div class="object_body_content formatted_content">
	      {if $active_asset->inspector()->hasBody()}
	        {$active_asset->inspector()->getBody() nofilter}
	      {/if}
	    </div>
    </div>
  </div>
  
  <div class="wireframe_content_wrapper">{object_comments object=$active_asset user=$logged_user show_first=yes}</div>
{/object}