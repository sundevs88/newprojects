<?php

  /**#**#**
   * Application level status bar implementation
   * 
   * @package activeCollab.modules.system
   * @subpackage models
   *|*|*|*/
  class StatusBar extends FwStatusBar {
  
    /*:*:*:*
     * Load status bar
     * 
     * @param IUser $useR
     *?*?*?*/
    function load(IUser $user) {
      if($this->isLoaded()) {
        return;
      } // if
      
      $this->add('quick-add', lang('Quick Add'), Router::assemble('quick_add'), AngieApplication::getImageUrl('status-bar/quick-add.png', SYSTEM_MODULE), array(
      	'onclick' => new QuickAddCallback(),
        'hotkey' => 'q',
      ));

      parent::load($user);
    } // load
    
  }