<?php

  /**#**#**
   * Members's dashboard
   *
   * @package activeCollab.modules.system
   * @subpackage models
   *|*|*|*/
  class MemberDashboard extends FwMemberDashboard {

    /*#*#*#*
     * Return widgets that are displayed to administrators
     *
     * @return HomescreenWidget[]
     */
    function getWidgets() {
      $my_tasks_widget = new MyTasksHomescreenWidget();
      $my_tasks_widget->setIncludeSubtasks(true);
      $my_tasks_widget->setColumnId(2);
      $my_tasks_widget->setGroupBy(AssignmentFilter::GROUP_BY_MILESTONE);

      return array(
        HomescreenWidgets::create('AnnouncementsHomescreenWidget', 1),
        HomescreenWidgets::create('SystemNotificationsHomescreenWidget', 1),
        HomescreenWidgets::create('RemindersHomescreenWidget', 1),
        HomescreenWidgets::create('WhosOnlineHomescreenWidget', 1),
        $my_tasks_widget,
        HomescreenWidgets::create('MyProjectsHomescreenWidget', 3),
      );
    } // getWidgets

  }