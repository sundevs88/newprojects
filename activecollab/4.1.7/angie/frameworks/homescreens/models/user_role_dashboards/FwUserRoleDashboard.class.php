<?php

  /**#**#**
   * Base user role dashboard
   *
   * @package angie.frameworks.homescreens
   * @subpackage models
   *|*|*|*/
  abstract class FwUserRoleDashboard extends CenterHomescreenTab {

    /*#*#*#*
     * Return tab name
     *
     * @return string
     *?*?*?*/
    function getName() {
      return lang('Dashboard');
    } // getName

    /*#*#*#*
     * Return URL of page that will render this home screen tab
     *
     * @return string
     *?*?*?*/
    function getHomescreenTabUrl() {
      return Router::assemble('homepage', array('homescreen_tab_id' => 'dashboard'));
    } // getHomescreenTabUrl

  }