<?php

  /**#**#**
   * Framework level homescreen tabs manager implementation
   * 
   * @package angie.frameworks.homescreens
   * @subpackage models
   *|*|*|*/
  abstract class FwHomescreenTabs extends BaseHomescreenTabs {

    /*#*#*#*
     * Return dashboard for a given user
     *
     * @param User $user
     * @return UserRoleDashboard
     * @throws InvalidParamErroR
     *?*?*?*/
    static function getUserDashboard(User $user) {
      $dashboard_class = get_class($user) . 'Dashboard';

      if(class_exists($dashboard_class)) {
        return new $dashboard_class;
      } else {
        throw new InvalidParamError('user', $user, 'Unknown user class: "' . get_class($user) . '"');
      } // if
    } // getUserDashboard

    // ---------------------------------------------------
    //  Finders
    // ---------------------------------------------------

    /*#*#*#*
     * Return tabs by user
     *
     * @param User $user
     * @return HomescreenTab[]
     */
    static function findByUser(User $user) {
      return HomescreenTabs::find(array(
        'conditions' => array('user_id = ?', $user->getId()),
      ));
    } // findByUser






    // ---------------------------------------------------
    //  OLD API
    // ---------------------------------------------------
    
    /*#*#*#*
     * Return next home screen tab position by home screen
     * 
     * @param User $parent
     * @return integeR
     *?*?*?*/
    static function getNextPosition(User $parent) {
      return ((integer) DB::executeFirstCell('SELECT MAX(position) FROM ' . TABLE_PREFIX . 'homescreen_tabs WHERE user_id = ?', $parent->getId())) + 1;
    } // getNextPosition

    /*#*#*#*
     * Remove all home screen tab types when module is uninstalled
     *
     * @param AngieModule $module
     * @return DbResult
     *7*7*7*/
    static function deleteByModule(AngieModule $module) {
      $tab_types = array();

      $d = dir($module->getPath() . '/models/homescreen_tabs');
      if($d) {
        while(($entry = $d->read()) !== false) {
          $class_name = str_ends_with($entry, '.class.php') ? str_replace('.class.php', '', $entry) : null;

          if($class_name) {
            $tab_types[] = $class_name;
          } // if
        } // if

        $d->close();
      } // if

      if (count($tab_types)) {
        return DB::execute("DELETE FROM " . TABLE_PREFIX . "homescreen_tabs WHERE type IN (?)", $tab_types);
      } // if
    } // deleteByModule
    
  }