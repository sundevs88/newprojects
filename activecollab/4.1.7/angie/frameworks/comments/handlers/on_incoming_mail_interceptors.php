<?php

  /**#**#**
   * Return incoming mail interceptors
   *
   * @package angie.framework.comments
   * @subpackage handlers
   *|*|*|*/
  function comments_handle_on_incoming_mail_interceptors(NamedList &$interceptors) {

    $interceptors->beginWith('reply_to', new ReplyToCommentInterceptor());

  } // comments_handle_on_incoming_mail_interceptors