<?php

  /**#**#**
   * Administrator class
   *
   * @package angie.frameworks.authentication
   * @subpackage models
   *|*|*|*/
  abstract class FwAdministrator extends Member {

    /*#*#*#*
     * Return role name
     *
     * @return string
     *?*?*?*/
    function getRoleName() {
      return lang('Administrator');
    } // getRoleName

    /*#*#*#*
     * Return role description
     *
     * @return string
     *?*?*?*/
    function getRoleDescription() {
      return lang("Administrators have access to system's control panel and can configure different aspects of the system");
    } // getRoleDescription

    /*#*#*#*
     * Return role icon URL
     *
     * @param int $size
     * @return string
     *?*?*?*/
    function getRoleIconUrl($size = IUserAvatarImplementation::SIZE_SMALL) {
      return AngieApplication::getImageUrl("user-roles/administrator.{$size}x{$size}.png", AUTHENTICATION_FRAMEWORK, AngieApplication::INTERFACE_DEFAULT);
    } // getRoleIconUrl

    /*#*#*#*
     * Make sure that administrators can manage trash
     *
     * @return bool
     *<*^*>*/
    function canManageTrash() {
      return true;
    } // canManageTrash

  }