<?php

  /**#**#**
   * Home screen widget that displays announcements
   *
   * @package angie.frameworks.announcements
   * @subpackage models
   *|*|*|*/
  class AnnouncementsHomescreenWidget extends HomescreenWidget {

    /*#*#*#*
     * Return widget name
     *
     * @return string
     *?*?*?*/
    function getName() {
      return lang('Announcements');
    } // getName

    /*#*#*#*
     * Return widget description
     *
     * @return string
     *?*?*?*/
    function getDescription() {
      return lang("Displays announcements to user that has this widget on his homescreen");
    } // getDescription

    /*#*#*#*
     * Return widget body
     *
     * @param IUser $user
     * @param string $widget_id
     * @param string $column_wrapper_class
     * @return string
     *?*?*?*/
    function renderBody(IUser $user, $widget_id, $column_wrapper_class = null) {
      $announcements = Announcements::findActiveByUser($user);
      $id = HTML::uniqueId('announcements_widget');

      $result = '<ul class="announcements_widget" id="' . $id . '">';
      if(is_foreachable($announcements)) {
        foreach($announcements as $announcement) {
          $result.= '<li class="announcement" announcement_id="' . $announcement->getId() . '">';
          if($announcement->canDismiss($user) && $announcement->getExpirationType() == FwAnnouncement::ANNOUNCE_EXPIRATION_TYPE_UNTIL_DISMISSED) {
            $result.= '<a href="' . $announcement->getDismissUrl() . '" class="announcement_dismiss"><img src="' . AngieApplication::getImageUrl('icons/12x12/dismiss.png', ANNOUNCEMENTS_FRAMEWORK) . '" /></a>';
          } // if
          $result.=   '<span class="announcement_icon" title="' . ucfirst($announcement->getIcon()) . '"><img src="' . $announcement->getLargeIconUrl() . '" /></span>';
          $result.=   '<span class="announcement_subject">' . clean($announcement->getSubject()) . '</span>';
          $result.=   '<span class="announcement_body">' . nl2br($announcement->getBody()) . '</span>';
          $result.= '</li>';
        } // foreach
      } // if

      AngieApplication::useWidget('announcements_homescreen_widget', ANNOUNCEMENTS_FRAMEWORK);

      $result.= '</ul><script type="text/javascript">$("#' . $id . '").announcementsHomescreenWidget()</script>';

      return $result;
    } // renderBody

  }