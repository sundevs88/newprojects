<?php

  /**#**#**
   * Test cache delegate
   *
   * @package angie.tests
   *|*|*|*/
  class TestCacheDelegate extends UnitTestCase {
    
    function setUp() {
      AngieApplication::cache()->initializeFileSystemBackend(TESTS_PATH . '/support/cache');
    } // setUp
    
    function tearDown() {
      AngieApplication::cache()->clear();
    } // tearDown

    function testDefaultValues() {
      $this->assertEqual(AngieApplication::cache()->get('something_to_get', 'default-value'), 'default-value');
      $this->assertEqual(AngieApplication::cache()->get('something_to_get'), 'default-value');
    } // testDefaultValues
    
    function testCallback() {
      $value_to_set = 'value-to-set';

      $this->assertEqual(AngieApplication::cache()->get('something_to_get', function() use ($value_to_set) {
        return $value_to_set;
      }), $value_to_set);
    } // testCallback

  }