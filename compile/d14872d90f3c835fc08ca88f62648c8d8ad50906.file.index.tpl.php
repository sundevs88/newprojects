<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:44:02
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/announcements/views/default/fw_announcements_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:791889935742ed02a2ad13-97631255%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd14872d90f3c835fc08ca88f62648c8d8ad50906' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/announcements/views/default/fw_announcements_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '791889935742ed02a2ad13-97631255',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'announcements' => 0,
    'announcements_per_page' => 0,
    'total_announcements' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742ed02a83a32_65187065',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742ed02a83a32_65187065')) {function content_5742ed02a83a32_65187065($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Announcements<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Announcements<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"ui_sortable",'module'=>"environment"),$_smarty_tpl);?>


<div id="announcements"></div>

<script type="text/javascript">
  $('#announcements').pagedObjectsList({
    'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'admin_announcements'),$_smarty_tpl);?>
',
    'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['announcements']->value);?>
,
    'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['announcements_per_page']->value,$_smarty_tpl);?>
,
    'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_announcements']->value,$_smarty_tpl);?>
,
    'list_items_are' : 'tr',
    'list_item_attributes' : { 'class' : 'announcement' },
    'class' : 'admin_list',
    'columns' : {
      'drag' : '',
      'is_enabled' : '',
      'icon' : App.lang('Icon'),
      'subject' : App.lang('Subject'),
      'created_on' : App.lang('Created On / By'),
      'dismissals' : App.lang('Visible To / Dismissed By'),
      'expires_on' : App.lang('Expires On'),
      'options' : ''
    },
    'empty_message' : App.lang('There are no announcements defined'),
    'listen' : 'announcement',
    'on_add_item' : function(item) {
      var announcement = $(this);

      announcement.append(
        '<td class="drag_icon"></td>' +
        '<td class="is_enabled"></td>' +
        '<td class="icon"></td>' +
        '<td class="subject"></td>' +
        '<td class="created_on"></td>' +
        '<td class="dismissals"></td>' +
        '<td class="expires_on"></td>' +
        '<td class="options"></td>'
      );

      announcement.find('.drag_icon').append('<img src="<?php echo smarty_function_image_url(array('name'=>"layout/bits/handle-drag.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" style="cursor: move;">');

      var checkbox = $('<input type="checkbox" />').attr({
        'on_url' : item['urls']['enable'],
        'off_url' : item['urls']['disable']
      }).asyncCheckbox({
        'success_event' : 'announcement_updated',
        'success_message' : [ App.lang('Announcement has been disabled'), App.lang('Announcement has been enabled') ]
      }).appendTo(announcement.find('td.is_enabled'));

      if(item['is_enabled']) {
        checkbox[0].checked = true;
      } // if

      announcement.find('td.icon').append('<img src="' + item['icon_url'] + '" />');

      if(typeof(item['subject']) == 'string' && item['subject']) {
        if(item['subject'].length > 50) {
          announcement.find('td.subject').append(App.clean(item['subject'].substr(0, 50)) + '...');
        } else {
          announcement.find('td.subject').append(App.clean(item['subject']));
        } // if
      } // if

      announcement.find('td.created_on').append(item['created_by']['created_ago']);
      announcement.find('td.created_on').append(App.lang(' by <a href=":user_link">:user_name</a>', {
        'user_link' : item['created_by']['urls']['view'],
        'user_name' : item['created_by']['short_display_name']
      }));

      announcement.find('td.dismissals').append(App.lang(':visible_to_count / :dismissed_by_count', {
        'visible_to_count' : item['visible_to_count'],
        'dismissed_by_count' : item['dismissed_by_count']
      }));

      announcement.find('td.expires_on').append(item['expires_on']);

      var options_cell = announcement.find('td.options');

      options_cell
        .append('<a href="' + item['urls']['edit'] + '" class="edit_announcement" title="' + App.lang('Edit Announcement') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_announcement" title="' + App.lang('Remove Announcement') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
      ;

      options_cell.find('a.edit_announcement').flyoutForm({
        'success_event' : 'announcement_updated',
        'width' : 565
      });
      options_cell.find('a.delete_announcement').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this announcement?'),
        'success_event' : 'announcement_deleted',
        'success_message' : App.lang('Announcement has been deleted successfully')
      });
    }
  }).sortable({
    'axis' : 'y',
    'handle' : 'td.drag_icon',
    'items' : 'tr.announcement',
    'helper' : function(event, ui) {
      ui.siblings().andSelf().children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    'start' : function(event, ui) {
      ui.item.css('background', '#E6E6E6');
    },
    'stop' : function(event, ui) {
      ui.item.css('background', 'transparent');
    },
    'update' : function(event, ui) {
      var data = {
        'submitted' : 'submitted'
      };

      var counter = 1;

      $(this).find('table.admin_list tr.announcement').each(function() {
        data['announcements[' + $(this).attr('list_item_id') + ']'] = counter++;
      });

      $.ajax({
        'url' : '<?php echo smarty_function_assemble(array('route'=>'admin_announcements_reorder'),$_smarty_tpl);?>
',
        'type' : 'post',
        'data' : data,
        'error' : function() {
          App.Wireframe.Flash.error('Failed to reorder announcements. Please try again later.');
        }
      });
    }
  }).disableSelection();
</script><?php }} ?>