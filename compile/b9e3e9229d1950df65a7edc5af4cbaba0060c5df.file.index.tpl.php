<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 15:15:41
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/task_segments/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15382504705728c09dd369b6-29208512%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b9e3e9229d1950df65a7edc5af4cbaba0060c5df' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/task_segments/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15382504705728c09dd369b6-29208512',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'all_tasks' => 0,
    'all_open_tasks' => 0,
    'all_completed_tasks' => 0,
    'task_segments' => 0,
    'task_segments_per_page' => 0,
    'total_task_segments' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728c09ddafab7_47546622',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728c09ddafab7_47546622')) {function content_5728c09ddafab7_47546622($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_empty_slate')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.empty_slate.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task Segments<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Task Segments<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="task_segments">
  <div id="task_segments_list"></div>
  <div id="task_segment_totals"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo clean($_smarty_tpl->tpl_vars['all_tasks']->value,$_smarty_tpl);?>
 &middot; <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Open<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo clean($_smarty_tpl->tpl_vars['all_open_tasks']->value,$_smarty_tpl);?>
 &middot; <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Completed<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo clean($_smarty_tpl->tpl_vars['all_completed_tasks']->value,$_smarty_tpl);?>
</div>
  <div id="task_segment_empty_slate"><?php echo smarty_function_empty_slate(array('name'=>'task_segments','module'=>@TASKS_MODULE),$_smarty_tpl);?>
</div>
</div>

<script type="text/javascript">
  $('#task_segments_list').each(function() {
    var all_tasks = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['all_tasks']->value);?>
;
    var all_open_tasks = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['all_open_tasks']->value);?>
;
    var all_completed_tasks = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['all_completed_tasks']->value);?>
;

    $(this).pagedObjectsList({
      'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'task_segments'),$_smarty_tpl);?>
',
      'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_segments']->value);?>
,
      'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['task_segments_per_page']->value,$_smarty_tpl);?>
,
      'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_task_segments']->value,$_smarty_tpl);?>
,
      'list_items_are' : 'tr',
      'list_item_attributes' : { 'class' : 'task_segment' },
      'columns' : {
        'name' : App.lang('Segment Name'),
        'total_tasks' : App.lang('Total Tasks'),
        'open_tasks' : App.lang('Open Tasks'),
        'completed_tasks' : App.lang('Completed Tasks'),
        'options' : ''
      },
      'sort_by' : 'name',
      'empty_message' : App.lang('There are no task segments defined'),
      'listen' : 'task_segment',
      'on_add_item' : function(item) {
        var task_segment = $(this);

        task_segment.append(
          '<td class="name"></td>' +
            '<td class="total_tasks tasks_count"></td>' +
            '<td class="open_tasks tasks_count"></td>' +
            '<td class="completed_tasks tasks_count"></td>' +
            '<td class="options"></td>'
        );

        task_segment.find('td.name').text(item['name']);
        task_segment.find('td.total_tasks').html(App.clean(item['total_tasks']) + ' <span class="percent_of_all" title="' + App.lang('% of All Tasks') + '">(' + App.clean(App.percentOfTotal(item['total_tasks'], all_tasks)) + ')</span>');
        task_segment.find('td.open_tasks').html(App.clean(item['open_tasks']) + ' <span class="percent_of_all" title="' + App.lang('% of All Open Tasks') + '">(' + App.clean(App.percentOfTotal(item['open_tasks'], all_open_tasks)) + ')</span>');
        task_segment.find('td.completed_tasks').html(App.clean(item['completed_tasks']) + ' <span class="percent_of_all" title="' + App.lang('% of All Completed Tasks') + '">(' + App.clean(App.percentOfTotal(item['completed_tasks'], all_completed_tasks)) + ')</span>');

        task_segment.find('td.options')
          .append('<a href="' + item['urls']['edit'] + '" class="edit_task_segment" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Edit') + '" /></a>')
        ;

        if(item['permissions']['can_delete']) {
          task_segment.find('td.options').append('<a href="' + item['urls']['delete'] + '" class="delete_task_segment" title="' + App.lang('Remove Task Segment') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Delete') + '" /></a>');
        } // if

        task_segment.find('td.options a.edit_task_segment').flyoutForm({
          'success_event' : 'task_segment_updated',
          'width' : 650
        });

        task_segment.find('td.options a.delete_task_segment').asyncLink({
          'confirmation' : App.lang('Are you sure?'),
          'success_event' : 'task_segment_deleted',
          'success_message' : App.lang('Object has been deleted successfully')
        });
      }
    });
  });
</script><?php }} ?>