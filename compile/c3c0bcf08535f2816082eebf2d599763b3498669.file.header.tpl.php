<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:24:10
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8609657595742e85aaa4090-79088949%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c3c0bcf08535f2816082eebf2d599763b3498669' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/header.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8609657595742e85aaa4090-79088949',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_url' => 0,
    'template_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e85ab657a8_24149159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e85ab657a8_24149159')) {function content_5742e85ab657a8_24149159($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_checkbox')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox.php';
if (!is_callable('smarty_function_file_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.file_field.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_block_textarea_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.textarea_field.php';
if (!is_callable('smarty_function_select_font')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_font.php';
if (!is_callable('smarty_function_color_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.color_field.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_header_form",'enctype'=>"multipart/form-data")); $_block_repeat=true; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_header_form",'enctype'=>"multipart/form-data"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="content_stack_wrapper">
  
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
      
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"body_layout")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"body_layout"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Choose layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	        <?php echo smarty_function_radio_field(array('name'=>"template[header_layout]",'label'=>"Logo on the left, company details on the right",'value'=>0,'checked'=>!$_smarty_tpl->tpl_vars['template_data']->value['header_layout']),$_smarty_tpl);?>
<br/>
	        <?php echo smarty_function_radio_field(array('name'=>"template[header_layout]",'label'=>"Company details on the left, logo on the right",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['header_layout']),$_smarty_tpl);?>

         <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"body_layout"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
  
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <div class="content_stack_optional"><?php echo smarty_function_checkbox(array('name'=>"template[print_logo]",'label'=>"Print",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_logo']),$_smarty_tpl);?>
</div>
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logo<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
		     <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"company_name")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"company_name"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

           <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Upload New Company Logo<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		       <?php echo smarty_function_file_field(array('name'=>"company_logo",'class'=>"company_name"),$_smarty_tpl);?>

		     <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"company_name"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <div class="content_stack_optional"><?php echo smarty_function_checkbox(array('name'=>"template[print_company_details]",'label'=>"Print",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_company_details']),$_smarty_tpl);?>
</div>
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Company Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
		     <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"company_name")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"company_name"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		       <?php echo smarty_function_text_field(array('name'=>"template[company_name]",'label'=>"Company Name",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['company_name'],'class'=>"company_name"),$_smarty_tpl);?>

		     <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"company_name"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    
		     <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"company_details")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"company_details"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		       <?php $_smarty_tpl->smarty->_tag_stack[] = array('textarea_field', array('name'=>"template[company_details]",'label'=>"Company Address and Details",'rows'=>"7",'class'=>"company_details")); $_block_repeat=true; echo smarty_block_textarea_field(array('name'=>"template[company_details]",'label'=>"Company Address and Details",'rows'=>"7",'class'=>"company_details"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['template_data']->value['company_details'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_textarea_field(array('name'=>"template[company_details]",'label'=>"Company Address and Details",'rows'=>"7",'class'=>"company_details"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		     <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"company_details"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Appearance<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
				<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"header_text_color")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"header_text_color"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

					<?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		      <?php echo smarty_function_select_font(array('name'=>"template[header_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['header_font']),$_smarty_tpl);?>

			  	<?php echo smarty_function_color_field(array('name'=>"template[header_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['header_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

				<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"header_text_color"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"header_text_color")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"header_text_color"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[print_header_border]",'label'=>"Show header border",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_header_border'],'id'=>"border_toggler"),$_smarty_tpl);?>
&nbsp;
          <?php echo smarty_function_color_field(array('name'=>"template[header_border_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['header_border_color'],'class'=>"inline_color_picker border_property"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"header_text_color"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
        
      </div>
    </div>
        
  </div>

	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_header_form",'enctype'=>"multipart/form-data"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


  <script type="text/javascript">
    var wrapper = $('#invoice_header_form');

    var border_toggler = wrapper.find('#border_toggler');
    var border_properties = wrapper.find('.border_property');
    var check_border_properties = function () {
      var is_checked = border_toggler.is(':checked');
      if (is_checked) {
        border_properties.show();
      } else {
        border_properties.hide();
      } // if      
    };    
    border_toggler.bind('click', check_border_properties);
    check_border_properties();

  </script><?php }} ?>