<?php /* Smarty version Smarty-3.1.12, created on 2016-03-07 02:53:44
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/task_schedule/reschedule.tpl" */ ?>
<?php /*%%SmartyHeaderCode:165567279756dced38df45e8-57824712%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2a84fec69d8876b0e930ecd540cc89ea7f5457c' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/task_schedule/reschedule.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '165567279756dced38df45e8-57824712',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_object' => 0,
    'reschedule_url' => 0,
    'reschedule_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56dced38e6b860_77921291',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56dced38e6b860_77921291')) {function content_56dced38e6b860_77921291($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_select_due_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_due_on.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array('object_name'=>$_smarty_tpl->tpl_vars['active_object']->value->getName())); $_block_repeat=true; echo smarty_block_title(array('object_name'=>$_smarty_tpl->tpl_vars['active_object']->value->getName()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reschedule: :object_name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array('object_name'=>$_smarty_tpl->tpl_vars['active_object']->value->getName()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="reschedule_task" class="reschedule_popup base_reschedule_popup">
	<?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['reschedule_url']->value)); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['reschedule_url']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	  <div class="fields_wrapper">
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'date_range')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'date_range'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Due Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <div class="tbd_off"><?php echo smarty_function_radio_field(array('name'=>"reschedule[tbd]",'value'=>"1",'label'=>"To Be Determined",'checked'=>$_smarty_tpl->tpl_vars['reschedule_data']->value['tbd']),$_smarty_tpl);?>
</div>
        <div class="tbd_on"><?php echo smarty_function_radio_field(array('name'=>"reschedule[tbd]",'value'=>"0",'label'=>"Set Now",'checked'=>!$_smarty_tpl->tpl_vars['reschedule_data']->value['tbd']),$_smarty_tpl);?>
</div>
        
        <div class="reschedule_controls">
		      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'due_on')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'due_on'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		        <?php echo smarty_function_select_due_on(array('name'=>"reschedule[due_on]",'value'=>$_smarty_tpl->tpl_vars['reschedule_data']->value['due_on']),$_smarty_tpl);?>

		      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'due_on'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		      
		      <?php echo smarty_function_checkbox_field(array('name'=>"reschedule[reschedule_subtasks]",'label'=>"Also reschedule all subtasks for this task",'value'=>"1",'checked'=>$_smarty_tpl->tpl_vars['reschedule_data']->value['reschedule_subtasks']),$_smarty_tpl);?>

        </div>
	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'date_range'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
	    
	  </div>
	  
	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reschedule<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['reschedule_url']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#reschedule_task').each(function() {
    var wrapper = $(this);
    
    var tbd_on = wrapper.find('div.tbd_on input[type=radio]:first');
    var tbd_off = wrapper.find('div.tbd_off input[type=radio]:first');
    var controls = wrapper.find('div.reschedule_controls');

    tbd_on.click(function () {
      controls.show();
    });

    tbd_off.click(function () {
      controls.hide();
    });

    if(tbd_off.prop('checked')) {
      controls.hide();
    } // if
  });
</script><?php }} ?>