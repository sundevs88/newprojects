<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 14:44:48
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/views/default/fw_globalization_admin/workweek.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5980554975728b9603e5755-97056737%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7975240d5fd85171b6eafc8c7b1325652ea8de32' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/views/default/fw_globalization_admin/workweek.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5980554975728b9603e5755-97056737',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'workweek_data' => 0,
    'day_off' => 0,
    'day_off_key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728b9604bfc74_93919862',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728b9604bfc74_93919862')) {function content_5728b9604bfc74_93919862($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_select_week_day')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_week_day.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_week_days')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_week_days.php';
if (!is_callable('smarty_function_cycle')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/vendor/smarty/smarty/plugins/function.cycle.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Workweek Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Workweek Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"days_off",'module'=>"globalization"),$_smarty_tpl);?>


<div id="workweek_settings">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>Router::assemble('workweek_settings'),'method'=>'post')); $_block_repeat=true; echo smarty_block_form(array('action'=>Router::assemble('workweek_settings'),'method'=>'post'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="content_stack_wrapper">
    
      <!-- Work days -->
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Workdays<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'first_week_day')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'first_week_day'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_select_week_day(array('name'=>"workweek[time_first_week_day]",'value'=>$_smarty_tpl->tpl_vars['workweek_data']->value['time_first_week_day'],'label'=>'First Day in a Week'),$_smarty_tpl);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'first_week_day'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'workdays')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'workdays'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Workdays<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php echo smarty_function_select_week_days(array('name'=>"workweek[time_workdays]",'value'=>$_smarty_tpl->tpl_vars['workweek_data']->value['time_workdays'],'required'=>'yes'),$_smarty_tpl);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'workdays'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
      
      <!-- Days Off -->
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Days Off<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div id="days_off_wrapper" class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'days_off')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'days_off'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <table class="form form_field" id="workweek_days_off" style="<?php if (!is_foreachable($_smarty_tpl->tpl_vars['workweek_data']->value['days_off'])){?>display: none<?php }?>">
              <tr>
                <th class="name"><?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Event Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th class="date"><?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th class="yearly center"><?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Repeat Yearly?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th></th>
              </tr>
            <?php if (is_foreachable($_smarty_tpl->tpl_vars['workweek_data']->value['days_off'])){?>
              <?php  $_smarty_tpl->tpl_vars['day_off'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day_off']->_loop = false;
 $_smarty_tpl->tpl_vars['day_off_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['workweek_data']->value['days_off']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day_off']->key => $_smarty_tpl->tpl_vars['day_off']->value){
$_smarty_tpl->tpl_vars['day_off']->_loop = true;
 $_smarty_tpl->tpl_vars['day_off_key']->value = $_smarty_tpl->tpl_vars['day_off']->key;
?>
                <tr class="day_off_row <?php echo smarty_function_cycle(array('values'=>'odd,even'),$_smarty_tpl);?>
">
                  <td class="name"><?php echo smarty_function_text_field(array('name'=>"workweek[days_off][".((string)$_smarty_tpl->tpl_vars['day_off_key']->value)."][name]",'value'=>$_smarty_tpl->tpl_vars['day_off']->value['name']),$_smarty_tpl);?>
</td>
                  <td class="date"><?php echo smarty_function_select_date(array('name'=>"workweek[days_off][".((string)$_smarty_tpl->tpl_vars['day_off_key']->value)."][event_date]",'value'=>$_smarty_tpl->tpl_vars['day_off']->value['date']),$_smarty_tpl);?>
</td>
                  <td class="yearly center"><input name="workweek[days_off][<?php echo clean($_smarty_tpl->tpl_vars['day_off_key']->value,$_smarty_tpl);?>
][repeat_yearly]" type="checkbox" value="1" class="inline" <?php if ($_smarty_tpl->tpl_vars['day_off']->value['repeat_yearly']){?>checked="checked"<?php }?> /></td>
                  <td class="options right"><a href="#" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Remove Day Off<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" class="remove_day_off"><img src='<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
' alt='' /></a></td>
                </tr>
              <?php } ?>
            <?php }?>
            </table>
            <p id="no_days_off_message" style="<?php if (is_foreachable($_smarty_tpl->tpl_vars['workweek_data']->value['days_off'])){?>display: none<?php }?>"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no days off defined<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
            <p><a href="#" class="button_add"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
New Day Off<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'days_off'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
      
      <div class="content_stack_element last">
		    <div class="content_stack_element_info">
		      <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Effective Work Hours<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
		    </div>
		    <div class="content_stack_element_body">
		      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'workweek_effective_work_hours')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'workweek_effective_work_hours'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		        <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'workweekEffectiveWorkHours')); $_block_repeat=true; echo smarty_block_label(array('for'=>'workweekEffectiveWorkHours'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Number of Effective Work Hours per Week<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'workweekEffectiveWorkHours'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		        <?php echo smarty_function_text_field(array('name'=>"workweek[effective_work_hours]",'value'=>$_smarty_tpl->tpl_vars['workweek_data']->value['effective_work_hours'],'class'=>'short','id'=>'workweekEffectiveWorkHours'),$_smarty_tpl);?>

		      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'workweek_effective_work_hours'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		    </div>
		  </div>
    </div>
    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>Router::assemble('workweek_settings'),'method'=>'post'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  App.widgets.daysOff.init('days_off_wrapper');
</script><?php }} ?>