<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:15:12
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoice_based_on/add_invoice.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3312326215742e640872000-14759582%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bec5c9729bb5e2ec85fa342b5dc75d1b6d691161' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoice_based_on/add_invoice.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3312326215742e640872000-14759582',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'is_based_on_quote' => 0,
    'wireframe' => 0,
    'preview_items_url' => 0,
    'active_object' => 0,
    'filter_data' => 0,
    'logged_user' => 0,
    'invoice_data' => 0,
    'is_based_on_tracking_report' => 0,
    'js_company_details_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e640a15e44_98632225',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e640a15e44_98632225')) {function content_5742e640a15e44_98632225($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_sum_time')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.sum_time.php';
if (!is_callable('smarty_function_sum_expenses')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.sum_expenses.php';
if (!is_callable('smarty_function_when_invoice_is_based_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.when_invoice_is_based_on.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_select_company')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_company.php';
if (!is_callable('smarty_function_address_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.address_field.php';
if (!is_callable('smarty_function_select_language')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.select_language.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_tax_rate')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_tax_rate.php';
if (!is_callable('smarty_function_select_project')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project.php';
if (!is_callable('smarty_function_select_currency')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.select_currency.php';
if (!is_callable('smarty_function_select_payments_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/payments/helpers/function.select_payments_type.php';
if (!is_callable('smarty_block_invoice_note')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/block.invoice_note.php';
if (!is_callable('smarty_block_invoice_comment')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/block.invoice_comment.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php echo smarty_function_use_widget(array('name'=>'create_invoice_from_tracked_data','module'=>@INVOICING_MODULE),$_smarty_tpl);?>


<?php if (!$_smarty_tpl->tpl_vars['is_based_on_quote']->value){?>
<script type="text/javascript">
  App.widgets.FlyoutDialog.front().addButton('change_description_formats', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['wireframe']->value->actions->get('change_description_formats'));?>
);
</script>
<?php }?>

<div id="create_invoice_from_tracked_data" data-preview-items-url="<?php echo clean($_smarty_tpl->tpl_vars['preview_items_url']->value,$_smarty_tpl);?>
">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->invoice()->getUrl())); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->invoice()->getUrl()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <input type="hidden" name="filter_data" value="<?php echo clean($_smarty_tpl->tpl_vars['filter_data']->value,$_smarty_tpl);?>
">

  	<div class="content_stack_wrapper">
      <?php if (!$_smarty_tpl->tpl_vars['is_based_on_quote']->value){?>
      <div class="content_stack_element full_width with_columns three_columns">
        <div class="content_stack_element_body">
          <div class="content_stack_element_body_column" style="width: 30%">
            <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
            <ul>
              <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo smarty_function_sum_time(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</li>
              <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo smarty_function_sum_time(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'mode'=>'billable'),$_smarty_tpl);?>
</li>
            </ul>
          </div>

          <div class="content_stack_element_body_column" style="width: 30%">
            <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
            <ul>
              <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo smarty_function_sum_expenses(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</li>
              <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo smarty_function_sum_expenses(array('object'=>$_smarty_tpl->tpl_vars['active_object']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'mode'=>'billable'),$_smarty_tpl);?>
</li>
            </ul>
          </div>

          <div id="tracked_data_sum_by_wrapper" class="content_stack_element_body_column" style="width: 40%">
            <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
How Would You Like this Data to be Grouped?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
            <?php echo smarty_function_when_invoice_is_based_on(array('name'=>"invoice_data[sum_by]",'id'=>'tracked_data_sum_by','mode'=>'select','required'=>true),$_smarty_tpl);?>

            <img src="<?php echo smarty_function_image_url(array('name'=>'create-invoice-arrow.png','module'=>@INVOICING_MODULE),$_smarty_tpl);?>
">
          </div>
        </div>
      </div>
      <div class="content_stack_element full_width" id="create_invoice_from_tracked_data_items_section">
        <div class="content_stack_element_body">
          <div id="invoice_items_preview">
            <table class="common" cellspacing="0">
              <thead>
              <tr>
                <th class="num"></th>
                <th class="description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th class="quantity center"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Quantity<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th class="unit_cost center"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unit Cost<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
      <?php }?>

      <div class="content_stack_element full_width with_columns two_columns">
        <div class="content_stack_element_body wrap_invoice_settings">
          <div class="content_stack_element_body_column">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'company_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'company_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_company(array('name'=>"invoice_data[company_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['company_id'],'id'=>'client_company_id','user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'can_create_new'=>false,'label'=>'Client','required'=>true,'exclude_owner_company'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'company_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'company_address','class'=>'companyAddressContainer')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'company_address','class'=>'companyAddressContainer'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_address_field(array('name'=>"invoice_data[company_address]",'id'=>'client_company_address','label'=>'Address','required'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'company_address','class'=>'companyAddressContainer'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'language')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'language'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_language(array('name'=>"invoice_data[language_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['language_id'],'label'=>'Language','preselect_default'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'language'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'po_number')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'po_number'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_text_field(array('name'=>"invoice_data[purchase_order_number]",'label'=>'Purchase Order Number','value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['purchase_order_number'],'optional'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'po_number'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </div>
          <div class="content_stack_element_body_column">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'first_tax_rate_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'first_tax_rate_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_tax_rate(array('name'=>'invoice_data[first_tax_rate_id]','label'=>'Tax Rate #1','optional'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'first_tax_rate_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php if (Invoices::isSecondTaxEnabled()){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'second_tax_rate_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'second_tax_rate_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_tax_rate(array('name'=>'invoice_data[second_tax_rate_id]','label'=>'Tax Rate #2','optional'=>true,'first_tax_rate'=>false),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'second_tax_rate_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php }?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'project_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'project_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_project(array('name'=>"invoice[project_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['project_id'],'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'label'=>'Project','optional'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'project_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php if ($_smarty_tpl->tpl_vars['is_based_on_tracking_report']->value){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'currency')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'currency'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_currency(array('name'=>"invoice_data[currency_id]",'label'=>'Currency','optional'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'currency'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php }?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'allow_partial')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'allow_partial'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_payments_type(array('name'=>"invoice_data[payments_type]",'label'=>'Payments','required'=>true),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'allow_partial'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </div>
        </div>
      </div>
         
      <div class="content_stack_element full_width with_columns two_columns">
        <div class="content_stack_element_body wrap_invoice_note_and_comment">
          <div class="content_stack_element_body_column">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_note','class'=>'wrap_invoice_note')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_note','class'=>'wrap_invoice_note'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php $_smarty_tpl->smarty->_tag_stack[] = array('invoice_note', array('name'=>"invoice_data[note]",'class'=>'long','label'=>'Invoice Note')); $_block_repeat=true; echo smarty_block_invoice_note(array('name'=>"invoice_data[note]",'class'=>'long','label'=>'Invoice Note'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['invoice_data']->value['note'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_invoice_note(array('name'=>"invoice_data[note]",'class'=>'long','label'=>'Invoice Note'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_note','class'=>'wrap_invoice_note'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </div>
          <div class="content_stack_element_body_column">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'comment','class'=>'wrap_invoice_comment')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'comment','class'=>'wrap_invoice_comment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php $_smarty_tpl->smarty->_tag_stack[] = array('invoice_comment', array('name'=>"invoice_data[private_note]",'label'=>'Our Comment (Private, Not Visibile to the Client)')); $_block_repeat=true; echo smarty_block_invoice_comment(array('name'=>"invoice_data[private_note]",'label'=>'Our Comment (Private, Not Visibile to the Client)'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_invoice_comment(array('name'=>"invoice_data[private_note]",'label'=>'Our Comment (Private, Not Visibile to the Client)'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'comment','class'=>'wrap_invoice_comment'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          </div>
        </div>
      </div>
    </div>
    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	  	<?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Create Invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_object']->value->invoice()->getUrl()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#create_invoice_from_tracked_data').createInvoiceFromTrackedData();

  $('#create_invoice_from_tracked_data').each(function() {
    var wrapper = $(this);
    
    var company_id = wrapper.find('#client_company_id');
    var company_address = wrapper.find('#client_company_address');
    var company_details_url = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_company_details_url']->value);?>
;
    
    var ajax_request;    
    company_id.change(function() {
  	  add_address();
    });
    add_address();

    function add_address() {
  	   if(company_id.length > 0) {
  	    var ajax_url = App.extendUrl(company_details_url, {
  	      'company_id' : company_id.val(),
  	      'skip_layout' : 1
  	    });
  	    
  	    // abort request if already exists and it's active
  	    if ((ajax_request) && (ajax_request.readyState !=4)) {
  	      ajax_request.abort();
  	    } // if
  	    
  	    if (!company_address.is('loading')) {
  	      company_address.addClass('loading');
  	    } // if
  	    
  	    company_address.attr("disabled", true);
  	    company_id.attr("disabled", true);
  	    
  	    ajax_request = $.ajax({
  	      'url' : ajax_url,
  	      'success' : function (response) {
  	        company_address.val(response);
  	        company_address.removeClass('loading');
  	        company_address.attr("disabled", false);
  	        company_id.attr("disabled", false);
  	      }
  	    });
  	   }
    }
  });
</script><?php }} ?>