<?php /* Smarty version Smarty-3.1.12, created on 2016-03-16 09:46:17
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/calendars/views/default/fw_calendar_events/_calendar_event_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:142036609756e92b695b0081-27811149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a6e957e3e3d9bbb5137fad6edf26325cf31ec26' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/calendars/views/default/fw_calendar_events/_calendar_event_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '142036609756e92b695b0081-27811149',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'calendar_event_data' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56e92b696152e0_87953025',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e92b696152e0_87953025')) {function content_56e92b696152e0_87953025($_smarty_tpl) {?><?php if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_calendar')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/calendars/helpers/function.select_calendar.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_calendar_event_time')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/calendars/helpers/function.select_calendar_event_time.php';
if (!is_callable('smarty_function_select_repeat_option')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/calendars/helpers/function.select_repeat_option.php';
?><div id="calendar_event_form">
	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'calendar_event_name')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'calendar_event_name'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<?php echo smarty_function_text_field(array('name'=>'calendar_event[name]','value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['name'],'label'=>'Name','required'=>true),$_smarty_tpl);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'calendar_event_name'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'calendar_event_parent_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'calendar_event_parent_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<?php echo smarty_function_select_calendar(array('name'=>'calendar_event[parent_id]','value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['parent_id'],'label'=>'Calendar','user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'required'=>true),$_smarty_tpl);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'calendar_event_parent_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'all_day_event')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'all_day_event'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'all_day_event'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'calendar_event_starts_on','class'=>"calendar_event_starts_on")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'calendar_event_starts_on','class'=>"calendar_event_starts_on"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<div class="col-left">
			<?php echo smarty_function_select_date(array('name'=>'calendar_event[starts_on]','value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['starts_on'],'skip_days_off'=>false,'label'=>'Starts','class'=>'starts_on'),$_smarty_tpl);?>

		</div>
		<div class="col-right">
			<?php echo smarty_function_select_calendar_event_time(array('name'=>"calendar_event[starts_on_time]",'value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['starts_on_time'],'label'=>'Time','class'=>"calendar_event_starts_on_time",'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>

		</div>
	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'calendar_event_starts_on','class'=>"calendar_event_starts_on"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'calendar_event_ends_on','class'=>"calendar_event_ends_on")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'calendar_event_ends_on','class'=>"calendar_event_ends_on"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<?php echo smarty_function_select_date(array('name'=>'calendar_event[ends_on]','value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['ends_on'],'skip_days_off'=>false,'label'=>'Ends','class'=>'ends_on'),$_smarty_tpl);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'calendar_event_ends_on','class'=>"calendar_event_ends_on"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'calendar_event_repeat_event','class'=>"calendar_event_repeat")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'calendar_event_repeat_event','class'=>"calendar_event_repeat"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<?php echo smarty_function_select_repeat_option(array('name'=>'calendar_event','value'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['repeat_event'],'label'=>'Repeat','class'=>"repeat_event",'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'repeat_event_option'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['repeat_event_option'],'repeat_until'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['repeat_until'],'starts_on'=>$_smarty_tpl->tpl_vars['calendar_event_data']->value['starts_on']),$_smarty_tpl);?>

	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'calendar_event_repeat_event','class'=>"calendar_event_repeat"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
	$('div#calendar_event_form').each(function() {
		var wrapper = $(this);

		var select = wrapper.find('select.select_calendar');
		var parent_form = select.parents('form').first();
		var submit_button = parent_form.find('button[type="submit"]');

		// disable submit button if there is no options to select
		if (!select.children().length) {
			select.attr('required', false);
			select.hide();
			submit_button.attr('disabled', true);
		} // if

		var checkbox = wrapper.find('input.all_day_event');
		var select_time = wrapper.find('select.calendar_event_starts_on_time');
		var starts_on = wrapper.find('input.starts_on');
		var ends_on = wrapper.find('input.ends_on');

		if (checkbox.is(':checked')) {
			select_time.hide();
		} // if

		wrapper.on('change', 'select.repeat_event', function() {
			var repeat_until = $(this).val();
			if (repeat_until != "<?php echo clean(CalendarEvent::DONT_REPEAT,$_smarty_tpl);?>
") {
				wrapper.find('ul.repeat_until').show();
			} else {
				wrapper.find('ul.repeat_until').hide();
			} // if

			var text = App.lang('days');
			if (repeat_until == "<?php echo clean(CalendarEvent::REPEAT_YEARLY,$_smarty_tpl);?>
") {
				text = App.lang('years');
			} else if (repeat_until == "<?php echo clean(CalendarEvent::REPEAT_MONTHLY,$_smarty_tpl);?>
") {
				text = App.lang('months');
			} else if (repeat_until == "<?php echo clean(CalendarEvent::REPEAT_WEEKLY,$_smarty_tpl);?>
") {
				text = App.lang('weeks');
			} // if

			wrapper.find('span.repeat_until_period_text').text(text);
		});

		wrapper.on('change', 'input.starts_on, input.ends_on', function() {
			var date_picker = $(this);
			var starts_on_date = new Date(starts_on.val());
			var ends_on_date = new Date(ends_on.val());

			// set ends_on date
			if (starts_on_date > ends_on_date) {
				if (date_picker.is('.starts_on')) {
					ends_on.val(starts_on.val());
				} else {
					starts_on.val(ends_on.val());
				} // if
			} // if

			// different dates
			if (starts_on_date != ends_on_date) {
				select_time.hide();
				checkbox.prop('checked', true);
			} // if
		});

		wrapper.on('click', 'input.all_day_event', function(event) {
			var checkbox = $(this);

			if (checkbox.is(':checked')) {
				select_time.hide();
			} else {
				ends_on.val(starts_on.val());
				select_time.show();
			} // if
		});

		wrapper.on('click', 'a.add_new_calendar', function(event) {
			var url = $(this).attr('href');

			App.Delegates.flyoutFormClick.apply(this, [event, {
				'width'             : '350',
				'title'             : App.lang('New Calendar'),
				'href'              : url,
				'success_event'     : 'calendar_created'
			}]);

			return false;
		});

		/**
		 * On calendar created
		 */
		App.Wireframe.Events.bind('calendar_created.content', function(event, response) {
			// find my option groups
			var my_optgroup = select.find('optgroup.mine');

			// prepare new option
			var option = $('<option value="' + response['id'] + '">' + response['name'] + '</option>');

			// if exist then append new option and return else create new group
			if (my_optgroup.length) {
				my_optgroup.append(option);
			} else {
				var optgroup = $('<optgroup class="mine" label="' + App.lang('My Calendars') + '"></optgroup>');
				optgroup.append(option);
				select.prepend(optgroup);
			} // if

			select.val(response['id']);
			select.trigger('change');
			select.attr('required', true);
			submit_button.attr('disabled', false);
			select.show();
		});
	});
</script><?php }} ?>