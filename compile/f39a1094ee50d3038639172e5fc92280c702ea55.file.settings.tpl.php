<?php /* Smarty version Smarty-3.1.12, created on 2016-04-03 15:15:37
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mail_admin/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1103071719570133993dfc79-89131770%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f39a1094ee50d3038639172e5fc92280c702ea55' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mail_admin/settings.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1103071719570133993dfc79-89131770',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_57013399460338_04383187',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57013399460338_04383187')) {function content_57013399460338_04383187($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_select_successive_connection_attempts')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/function.select_successive_connection_attempts.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_function_select_conflict_notification_delivery')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/function.select_conflict_notification_delivery.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Incoming Email Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Incoming Email Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="incoming_mail_settings">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>Router::assemble('incoming_email_admin_change_settings'),'method'=>'post','id'=>"incoming_mail_settings_admin")); $_block_repeat=true; echo smarty_block_form(array('action'=>Router::assemble('incoming_email_admin_change_settings'),'method'=>'post','id'=>"incoming_mail_settings_admin"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="content_stack_wrapper">


    <?php if (!AngieApplication::mailer()->isConnectionConfigurationLocked()){?>
    	<div class="content_stack_element even">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
General Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
        	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"disable_mailbox_settings")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"disable_mailbox_settings"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        		<div>
          		<?php echo smarty_function_radio_field(array('name'=>"settings[disable_mailbox_on_successive_connection_failures]",'id'=>"disable_mailbox_off",'class'=>"disable_mailbox",'pre_selected_value'=>$_smarty_tpl->tpl_vars['settings_data']->value['disable_mailbox_on_successive_connection_failures'],'value'=>IncomingMail::AUTO_DISABLE_MAILBOX_OFF,'label'=>'Never Disable Mailboxes on Successive Connection Failures'),$_smarty_tpl);?>

          	</div>
          	<div>  
          		<?php echo smarty_function_radio_field(array('name'=>"settings[disable_mailbox_on_successive_connection_failures]",'id'=>"disable_mailbox_on",'class'=>"disable_mailbox",'pre_selected_value'=>$_smarty_tpl->tpl_vars['settings_data']->value['disable_mailbox_on_successive_connection_failures'],'value'=>IncomingMail::AUTO_DISABLE_MAILBOX_ON,'label'=>'Automatically Disable Mailbox on Successive Connection Failures'),$_smarty_tpl);?>

          	</div>
          	<div id="successive_connection_attempts">
          		<div>
          			<?php echo smarty_function_select_successive_connection_attempts(array('name'=>"settings[successive_connection_attempts]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['successive_connection_attempts']),$_smarty_tpl);?>

          		</div>
          		<div>
          		<?php echo smarty_function_checkbox_field(array('name'=>"settings[notify_administrator_when_mailbox_is_disabled]",'value'=>1,'label'=>'Notify administrators when mailbox is disabled','class'=>"notify_administrator",'checked'=>$_smarty_tpl->tpl_vars['settings_data']->value['notify_administrator_when_mailbox_is_disabled']),$_smarty_tpl);?>

          		</div>
          	</div>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"disable_mailbox_settings"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
    <?php }?>
      
      <div class="content_stack_element odd">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Conflict Notifications<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
        	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"notify_instantly")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"notify_instantly"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_select_conflict_notification_delivery(array('name'=>"settings[conflict_notifications_delivery]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['conflict_notifications_delivery']),$_smarty_tpl);?>

        	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"notify_instantly"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
    
        </div>
      </div>

    </div>
    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>Router::assemble('incoming_email_admin_change_settings'),'method'=>'post','id'=>"incoming_mail_settings_admin"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#incoming_mail_settings').each(function() {
    var wrapper = $(this);

		var successive_connection_attempts_container = wrapper.find('div#successive_connection_attempts'); 
		
    wrapper.find('#disable_mailbox_off').each(function() {
        $(this).click(function() {
        	successive_connection_attempts_container.slideUp('fast');
        });
        
        if(this.checked) {
        	successive_connection_attempts_container.hide();
        } // if 
      });

    wrapper.find('#disable_mailbox_on').each(function() {
        $(this).click(function() {
        	successive_connection_attempts_container.slideDown('fast');
        });
        
        if(this.checked) {
        	successive_connection_attempts_container.show();
        } // if 
      });
    
  });
</script><?php }} ?>