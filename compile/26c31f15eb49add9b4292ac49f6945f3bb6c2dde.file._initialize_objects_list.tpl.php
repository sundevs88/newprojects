<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 15:25:03
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/files/views/default/assets/_initialize_objects_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:119809283356d8574f933196-13931456%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26c31f15eb49add9b4292ac49f6945f3bb6c2dde' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/files/views/default/assets/_initialize_objects_list.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '119809283356d8574f933196-13931456',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'assets' => 0,
    'active_project' => 0,
    'categories' => 0,
    'milestones' => 0,
    'letters' => 0,
    'created_on_dates' => 0,
    'updated_on_dates' => 0,
    'types' => 0,
    'types_detailed' => 0,
    'print_url' => 0,
    'mass_manager' => 0,
    'in_archive' => 0,
    'active_asset' => 0,
    'active_text_document_version' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d8574f990ee9_31425042',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d8574f990ee9_31425042')) {function content_56d8574f990ee9_31425042($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_modifier_map')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.map.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
?><?php echo smarty_function_use_widget(array('name'=>"objects_list",'module'=>"environment"),$_smarty_tpl);?>


<script type="text/javascript">
  $('#assets_new_text_document').flyoutForm({
    'title' : App.lang('Text Document'),
    'success_event' : 'asset_created'
  });

  $('#assets_new_upload_files').flyoutFileForm({
    'title' : App.lang('Upload Files'),
    'success_event' : 'multiple_assets_created'
  });

  $('#project_assets_bookmark_add').flyoutForm({
    'title' : App.lang('Bookmark'),
    'success_event' : 'asset_created'
  });

  $('#assets_new_youtube').flyoutForm({
    'title' : App.lang('YouTube Video'),
    'success_event' : 'asset_created'
  });

  $('#assets').each(function() {
    var wrapper = $(this);

    var items = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['assets']->value);?>
;
    var project_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
;
    var categories_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['categories']->value);?>
;
    var milestones_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['milestones']->value);?>
;
    var letters_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['letters']->value);?>
;
    var created_on_dates_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['created_on_dates']->value);?>
;
    var updated_on_dates_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['updated_on_dates']->value);?>
;
    var type_map = <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['types']->value);?>
;
    var types_detailed = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['types_detailed']->value);?>
;

    var print_url = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['print_url']->value);?>
;

    var type_filter = [];
    type_filter.push({ label : App.lang('All types'), value : '', 'default' : true, icon : App.Wireframe.Utils.imageUrl('objects-list/all-assets.png', 'files')});
    App.each(type_map, function (type, title) {
      type_filter.push({
        'label' : title,
        'value' : type,
        'icon' : types_detailed[type].icon
      });
    });

    var init_options = {
      'id' : 'project_' + <?php echo clean($_smarty_tpl->tpl_vars['active_project']->value->getId(),$_smarty_tpl);?>
 + '_assets',
      'items' : items,
      'required_fields' : ['id', 'name', 'category_id', 'milestone_id', 'first_letter', 'created_on_date', 'updated_on_date', 'type', 'preview', 'permalink', 'is_archived'],
      'requirements' : {},
      'objects_type' : 'assets',
      'events' : App.standardObjectsListEvents(),
      'multi_title' : App.lang(':num Assets Selected'),
      'multi_url' : '<?php echo smarty_function_assemble(array('route'=>'project_assets_mass_edit','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
',
      'multi_actions' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['mass_manager']->value);?>
,
      'print_url' : print_url,
      'prepare_item' : function (item) {
        var result = {
          'id'              : item['id'],
          'name'            : item['name'],
          'first_letter'    : item['first_letter'],
          'created_on_date' : item['created_on_date'],
          'updated_on_date' : item['updated_on_date'],
          'type'            : item['type'],
          'icon'            : item['preview']['icons']['small'],
          'is_archived'     : item['state'] == 2 ? 1 : 0,
          'permalink'       : item['permalink'],
          'is_favorite'     : item['is_favorite'],
          'is_trashed'      : item['state'] == '1' ? 1 : 0,
          'visibility'      : item['visibility']
        };

        if(typeof(item['category']) == 'undefined') {
          result['category_id'] = item['category_id'];
        } else {
          result['category_id'] = item['category'] ? item['category']['id'] : 0;
        } // if

        if(typeof(item['milestone']) == 'undefined') {
          result['milestone_id'] = item['milestone_id'];
        } else {
          result['milestone_id'] = item['milestone'] ? item['milestone']['id'] : 0;
        } // if

        return result;
      },

      'render_item' : function (item) {
        return '<td class="icon small"><img src="' + App.clean(item['icon']) + '" alt=""></td><td class="name">' + App.clean(item.name) + App.Wireframe.Utils.renderVisibilityIndicator(item['visibility']) + '</td><td class="asset_options"></td>';
      },

      'grouping' : [{
        'label' : App.lang("Don't group"),
        'property' : '',
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      }, {
        'label' : App.lang('By Category'),
        'property' : 'category_id' ,
        'map' : categories_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-category.png', 'categories'),
        'default' : true,
        'uncategorized_label' : App.lang('Uncategorized')
      }, {
        'label' : App.lang('By Milestone'),
        'property' : 'milestone_id',
        'map' : milestones_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-milestones.png', 'system'),
        'uncategorized_label' : App.lang('Unknown Milestone')
      }, {
        'label' : App.lang('By Name'),
        'property' : 'first_letter',
        'map' : letters_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-name.png', 'environment'),
        'uncategorized_label' : App.lang('*')
      }, {
        'label' : App.lang('By Date Created'),
        'property' : 'created_on_date',
        'map' : created_on_dates_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/date-created.png', 'files'),
        'uncategorized_label' : App.lang('Unknown Date of Creation')
      }, {
        'label' : App.lang('By Date Modified'),
        'property' : 'updated_on_date',
        'map' : updated_on_dates_map,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/date-modified.png', 'files'),
        'uncategorized_label' : App.lang('Not Modified')
      }],

      'filtering' : [{
        'label' : App.lang('Type'),
        'property'  : 'type',
        'values'  : type_filter
      }]
    };

    if (<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['in_archive']->value);?>
) {
      init_options.requirements.is_archived = 1;
    } else {
      init_options.requirements.is_archived = 0;
    } // if

    wrapper.objectsList(init_options);

    // handle multiple assets added
    App.Wireframe.Events.bind('multiple_assets_created.content', function (event, data) {
      var counter = 0;
      var last_asset = null;

      $.each(data, function (key, asset) {
        App.Wireframe.Events.trigger('asset_created', [asset, true]);
        last_asset = asset;
      });

      if (last_asset) {
        wrapper.objectsList('load_item', last_asset.id, last_asset.permalink);
      } // if
    });

    // Asset added
    App.Wireframe.Events.bind('asset_created.content', function (event, asset, skip_loading) {
      if (asset.project_id == project_id) {
        wrapper.objectsList('add_item', asset, true, skip_loading);
      } else {
        if ($.cookie('ac_redirect_to_target_project')) {
          App.Wireframe.Content.setFromUrl(asset['urls']['view']);
        } // if
      } // if
    });

    // Asset updated
    App.Wireframe.Events.bind('asset_updated.content', function (event, asset) {
      if (asset['project_id'] == project_id) {
        wrapper.objectsList('update_item', asset);
      } else {
        if ($.cookie('ac_redirect_to_target_project')) {
          App.Wireframe.Content.setFromUrl(asset['urls']['view']);
        } else {
          wrapper.objectsList('delete_selected_item');
        } // if
      } // if
    });

    // Asset deleted
    App.Wireframe.Events.bind('asset_deleted.content', function (event, asset) {
      if (asset['project_id'] == project_id) {
        if (wrapper.objectsList('is_loaded', asset['id'], false)) {
          wrapper.objectsList('load_empty');
        } // if
        wrapper.objectsList('delete_item', asset['id']);
      } // if
    });

    // Manage maps
    App.objects_list_keep_milestones_map_up_to_date(wrapper, 'milestone_id', project_id);

    // Kepp categories map up to date
    App.objects_list_keep_categories_map_up_to_date(wrapper, 'category_id', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_asset']->value->category()->getCategoryContextString());?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_asset']->value->category()->getCategoryClass());?>
);

  <?php if ($_smarty_tpl->tpl_vars['active_text_document_version']->value&&$_smarty_tpl->tpl_vars['active_text_document_version']->value->isLoaded()){?>
    wrapper.objectsList('load_item', null, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_text_document_version']->value->getViewUrl());?>
);
    <?php }elseif($_smarty_tpl->tpl_vars['active_asset']->value->isLoaded()){?>
    wrapper.objectsList('load_item', <?php echo clean($_smarty_tpl->tpl_vars['active_asset']->value->getId(),$_smarty_tpl);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_asset']->value->getViewUrl());?>
);
  <?php }?>
  });
</script><?php }} ?>