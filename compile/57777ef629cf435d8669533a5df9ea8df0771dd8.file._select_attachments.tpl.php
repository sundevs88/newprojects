<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 04:40:17
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/attachments/views/default/_select_attachments.tpl" */ ?>
<?php /*%%SmartyHeaderCode:43589454356d7c0317289a0-96245370%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '57777ef629cf435d8669533a5df9ea8df0771dd8' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/attachments/views/default/_select_attachments.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43589454356d7c0317289a0-96245370',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_select_object_attachments_id' => 0,
    '_select_object_attachments_uploader_options' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7c0317659d2_59293356',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7c0317659d2_59293356')) {function content_56d7c0317659d2_59293356($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_max_file_size_warning')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.max_file_size_warning.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"select_attachments",'module'=>@FILE_UPLOADER_FRAMEWORK),$_smarty_tpl);?>


<div class="select_attachments" id="<?php echo $_smarty_tpl->tpl_vars['_select_object_attachments_id']->value;?>
">
  <table class="select_attachments_list" cellspacing="0"></table>

  <div class="upload_button" id="<?php echo $_smarty_tpl->tpl_vars['_select_object_attachments_id']->value;?>
_attach_file_button_wrapper">
    <a href="#" id="<?php echo $_smarty_tpl->tpl_vars['_select_object_attachments_id']->value;?>
_attach_file_button" class="link_button"><span class="inner"><span class="icon button_add"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Attach Files<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></span></a>
  </div>

  <p class="select_object_attachments_max_size details"><?php echo smarty_function_max_file_size_warning(array(),$_smarty_tpl);?>
</p>
</div>

<script type="text/javascript">
  (function () {
    $('#<?php echo clean($_smarty_tpl->tpl_vars['_select_object_attachments_id']->value,$_smarty_tpl);?>
').selectAttachments(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['_select_object_attachments_uploader_options']->value);?>
);
  }());
</script><?php }} ?>