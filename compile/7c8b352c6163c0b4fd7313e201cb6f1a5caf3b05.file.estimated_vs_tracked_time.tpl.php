<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 15:09:19
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/estimated_vs_tracked_time/estimated_vs_tracked_time.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7556866905728bf1f147d73-01562679%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c8b352c6163c0b4fd7313e201cb6f1a5caf3b05' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/estimated_vs_tracked_time/estimated_vs_tracked_time.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7556866905728bf1f147d73-01562679',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'projects_exist' => 0,
    'logged_user' => 0,
    'group_by' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728bf1f20d566_55580806',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728bf1f20d566_55580806')) {function content_5728bf1f20d566_55580806($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_select_project')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project.php';
if (!is_callable('smarty_function_select_group_assignments_by')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_group_assignments_by.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Estimated vs Tracked Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Estimated vs Tracked Time Report<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="estimated_vs_tracked_time_report" class="filter_criteria project_tasks_picker_for_reports">
<?php if ($_smarty_tpl->tpl_vars['projects_exist']->value){?>
  <form action="<?php echo smarty_function_assemble(array('route'=>'estiamted_vs_tracked_time_report_run'),$_smarty_tpl);?>
" method="get">
    <!-- Project Picker -->

    <div class="criteria_head">
      <div class="criteria_head_inner">
        <div class="criteria_project_picker">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_project(array('name'=>'task_report[project_id]','user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'show_all'=>true,'class'=>'long'),$_smarty_tpl);?>

        </div>
        <div class="criteria_group_by">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Group by<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php ob_start();?><?php echo clean(@AssignmentFilter::GROUP_BY_PROJECT,$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo clean(@AssignmentFilter::GROUP_BY_PROJECT_CLIENT,$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php echo smarty_function_select_group_assignments_by(array('name'=>'task_report[group_by]','value'=>$_smarty_tpl->tpl_vars['group_by']->value,'exclude'=>$_tmp1.",".$_tmp2),$_smarty_tpl);?>

        </div>
        <div class="criteria_run"><?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('type'=>"submit",'class'=>"default")); $_block_repeat=true; echo smarty_block_button(array('type'=>"submit",'class'=>"default"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Run<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('type'=>"submit",'class'=>"default"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
      </div>
    </div>
  </form>
<?php }else{ ?>
  <p class="empty_page"><span class="inner"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
You are not assigned to any project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</span></p>
<?php }?>

  <div class="filter_results"></div>
</div>

<script type="text/javascript">
  $('#estimated_vs_tracked_time_report').each(function() {
    var wrapper = $(this);

    wrapper.find('form').each(function() {
      var form = $(this);

      form.find('button').click(function() {
        var data = {
          'project_id' : form.find('.criteria_project_picker option:selected').val(),
          'group_by' : form.find('.criteria_group_by option:selected').val(),
          'async' : 1
        };

        $.get(form.attr('action'), data, function(response) {
          wrapper.find('div.filter_results').html(response);
        });

        return false;
      });
    });
  });
</script><?php }} ?>