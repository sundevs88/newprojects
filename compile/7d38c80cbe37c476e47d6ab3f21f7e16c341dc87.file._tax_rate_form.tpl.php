<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:29:45
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/tax_rates_admin/_tax_rate_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3860509375742e9a93f9777-49180384%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d38c80cbe37c476e47d6ab3f21f7e16c341dc87' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/tax_rates_admin/_tax_rate_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3860509375742e9a93f9777-49180384',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tax_rate_data' => 0,
    'active_tax_rate' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e9a943faf5_02813210',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e9a943faf5_02813210')) {function content_5742e9a943faf5_02813210($_smarty_tpl) {?><?php if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_decimal_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.decimal_field.php';
?><div class="tax_rate_form">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'name')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'name'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'tax_rateName','required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>'tax_rateName','required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'tax_rateName','required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php echo smarty_function_text_field(array('name'=>"tax_rate[name]",'value'=>$_smarty_tpl->tpl_vars['tax_rate_data']->value['name'],'id'=>'tax_rateName','class'=>'required'),$_smarty_tpl);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'name'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'percentage')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'percentage'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'tax_ratePercentage')); $_block_repeat=true; echo smarty_block_label(array('for'=>'tax_ratePercentage'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax Percentage <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'tax_ratePercentage'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php echo smarty_function_decimal_field(array('name'=>"tax_rate[percentage]",'value'=>$_smarty_tpl->tpl_vars['tax_rate_data']->value['percentage'],'id'=>'tax_ratePercentage','class'=>'short','min'=>"0",'max'=>"99.999",'step'=>"0.001",'disabled'=>$_smarty_tpl->tpl_vars['active_tax_rate']->value->isUsed()),$_smarty_tpl);?>
 %
  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'percentage'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div><?php }} ?>