<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:40:01
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11564831025742ec11bbdbb0-13152375%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c9d04b5a59b942c3890ae9acc035ae89ab1be76' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/body.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11564831025742ec11bbdbb0-13152375',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_url' => 0,
    'template_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742ec11d0e2b5_48354295',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742ec11d0e2b5_48354295')) {function content_5742ec11d0e2b5_48354295($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_select_font')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_font.php';
if (!is_callable('smarty_function_color_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.color_field.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_body_form",'enctype'=>"multipart/form-data")); $_block_repeat=true; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_body_form",'enctype'=>"multipart/form-data"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="content_stack_wrapper">
  
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
      
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"body_layout")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"body_layout"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Choose layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	        <?php echo smarty_function_radio_field(array('name'=>"template[body_layout]",'label'=>"Client details on the left, invoice details on the right",'value'=>0,'checked'=>!$_smarty_tpl->tpl_vars['template_data']->value['body_layout']),$_smarty_tpl);?>
<br/>
	        <?php echo smarty_function_radio_field(array('name'=>"template[body_layout]",'label'=>"Invoice details on the left, client details on the right",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['body_layout']),$_smarty_tpl);?>

         <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"body_layout"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Client Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"client_details_font")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"client_details_font"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_font(array('name'=>"template[client_details_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['client_details_font']),$_smarty_tpl);?>

          <?php echo smarty_function_color_field(array('name'=>"template[client_details_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['client_details_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"client_details_font"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
            
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"invoice_details_font")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"invoice_details_font"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_font(array('name'=>"template[invoice_details_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['invoice_details_font']),$_smarty_tpl);?>

          <?php echo smarty_function_color_field(array('name'=>"template[invoice_details_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['invoice_details_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"invoice_details_font"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
            
      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Items<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"items_font")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"items_font"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_font(array('name'=>"template[items_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['items_font']),$_smarty_tpl);?>

          <?php echo smarty_function_color_field(array('name'=>"template[items_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['items_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"items_font"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"header_text_color")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"header_text_color"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[print_table_border]",'label'=>"Show table bottom and top border",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_table_border'],'id'=>"table_border_toggler"),$_smarty_tpl);?>
&nbsp;
          <?php echo smarty_function_color_field(array('name'=>"template[table_border_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['table_border_color'],'class'=>"inline_color_picker table_border_property"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"header_text_color"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
        
        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"header_text_color")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"header_text_color"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[print_items_border]",'label'=>"Show border between invoice items",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_items_border'],'id'=>"border_toggler"),$_smarty_tpl);?>
&nbsp;
          <?php echo smarty_function_color_field(array('name'=>"template[items_border_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['items_border_color'],'class'=>"inline_color_picker border_property"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"header_text_color"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"item_columns")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"item_columns"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Columns to Display<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <ul>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_item_order]",'label'=>"Item #",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_item_order'],'id'=>"display_item_order_toggler"),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>'','label'=>"Description",'value'=>1,'checked'=>true,'disabled'=>'disabled'),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_quantity]",'label'=>"Quantity",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_quantity'],'id'=>"display_quantity_toggler"),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_unit_cost]",'label'=>"Unit Price",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_unit_cost'],'id'=>"display_unit_cost_toggler"),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_subtotal]",'label'=>"Subtotal",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_subtotal'],'id'=>"display_subtotal_toggler"),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_tax_rate]",'label'=>"Tax Rate",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_tax_rate'],'id'=>"display_tax_rate_toggler"),$_smarty_tpl);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_checkbox_field(array('name'=>"template[display_tax_amount]",'label'=>"Tax Amount",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_tax_amount'],'id'=>"display_tax_amount_toggler"),$_smarty_tpl);?>
</li>
            <li><?php echo smarty_function_checkbox_field(array('name'=>"template[display_total]",'label'=>"Total",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['display_total'],'id'=>"display_total_toggler"),$_smarty_tpl);?>
</li>
          </ul>

          <script type="text/javascript">
            var tax_rate_toggler = $('#display_tax_rate_toggler');
            var tax_rate_amount = $('#display_tax_amount_toggler');

            tax_rate_toggler.click(function () {
              if (tax_rate_toggler.is(':checked')) {
                tax_rate_amount.removeAttr('checked');
              } // if
            });

            tax_rate_amount.click(function () {
              if (tax_rate_amount.is(':checked')) {
                tax_rate_toggler.removeAttr('checked');
              } // if
            });
          </script>
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"item_columns"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"summarize_tax")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"summarize_tax"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax Summarizing<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[summarize_tax]",'label'=>"Summarize Tax",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['summarize_tax'],'id'=>"summarize_tax_toggler"),$_smarty_tpl);?>

          <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
When checked, tax of every item will be summed and shown as one item in totals section. If not, tax information will be shown summed by tax type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[hide_tax_subtotal]",'label'=>"Hide Tax Subtotal if it is 0",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['hide_tax_subtotal'],'id'=>"hide_tax_subtotal_toggler"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"summarize_tax"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Note<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"items_font")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"items_font"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_font(array('name'=>"template[note_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['note_font']),$_smarty_tpl);?>

          <?php echo smarty_function_color_field(array('name'=>"template[note_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['note_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"items_font"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
  </div>

  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_body_form",'enctype'=>"multipart/form-data"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


  <script type="text/javascript">
    var wrapper = $('#invoice_body_form');

    var border_toggler = wrapper.find('#border_toggler');
    var border_properties = wrapper.find('.border_property');
    var check_border_properties = function () {
      var is_checked = border_toggler.is(':checked');
      if (is_checked) {
        border_properties.show();
      } else {
        border_properties.hide();
      } // if      
    };    
    border_toggler.bind('click', check_border_properties);
    check_border_properties();

    var table_border_toggler = wrapper.find('#table_border_toggler');
    var table_border_properties = wrapper.find('.table_border_property');
    var check_table_border_properties = function () {
      var is_checked = table_border_toggler.is(':checked');
      if (is_checked) {
        table_border_properties.show();
      } else {
        table_border_properties.hide();
      } // if            
    };

    table_border_toggler.bind('click', check_table_border_properties);
    check_table_border_properties();
  </script>

<?php }} ?>