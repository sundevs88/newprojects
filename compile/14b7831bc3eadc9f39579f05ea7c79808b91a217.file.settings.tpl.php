<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 08:40:24
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/project/settings.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32830801856da9b78989976-55207804%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14b7831bc3eadc9f39579f05ea7c79808b91a217' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/project/settings.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32830801856da9b78989976-55207804',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'settings_data' => 0,
    'default_clients_can_delegate_to_employees' => 0,
    'default_default_billable_status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56da9b78a26e42_41010276',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56da9b78a26e42_41010276')) {function content_56da9b78a26e42_41010276($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_select_project_tabs')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project_tabs.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_yes_no_default')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.yes_no_default.php';
if (!is_callable('smarty_function_select_visibility')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_visibility.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="project_settings">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_project']->value->getSettingsUrl(),'method'=>'post')); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_project']->value->getSettingsUrl(),'method'=>'post'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="content_stack_wrapper">
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tabs<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <div>
            <input type="radio" name="settings[use_custom_tabs]" value="0" id="projectSettingsDontUseCustomTabs" class="inline" <?php if (!$_smarty_tpl->tpl_vars['settings_data']->value['use_custom_tabs']){?>checked="checked"<?php }?> /> <label for="projectSettingsDontUseCustomTabs" class="inline"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Use default set of tabs<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</label>
          </div>
          <div>
            <input type="radio" name="settings[use_custom_tabs]" value="1" id="projectSettingsUseCustomTabs" class="inline" <?php if ($_smarty_tpl->tpl_vars['settings_data']->value['use_custom_tabs']){?>checked="checked"<?php }?> /> <label for="projectSettingsUseCustomTabs" class="inline"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Configure tabs for this project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</label>
          </div>

          <div id="project_tabs_settings_custom" style="display: <?php if ($_smarty_tpl->tpl_vars['settings_data']->value['use_custom_tabs']){?>block<?php }else{ ?>none<?php }?>">
            <?php echo smarty_function_select_project_tabs(array('name'=>"settings[project_tabs]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['project_tabs']),$_smarty_tpl);?>

          </div>
        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Assignment Delegation<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'clients_can_delegate_to_employees')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'clients_can_delegate_to_employees'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_yes_no_default(array('name'=>"settings[clients_can_delegate_to_employees]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['clients_can_delegate_to_employees'],'label'=>'Clients can Delegate Assignments to all Project Members','default'=>$_smarty_tpl->tpl_vars['default_clients_can_delegate_to_employees']->value),$_smarty_tpl);?>

            <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
When this option is set to Yes, client can delegate assignments to all project members. Set this option to No to limit delegation list only to project members from client's company<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'clients_can_delegate_to_employees'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

    <?php if (AngieApplication::isModuleLoaded('tracking')){?>
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'default_billable_status')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'default_billable_status'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_yes_no_default(array('name'=>"settings[default_billable_status]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['default_billable_status'],'label'=>'Default Billable Status for New Entries','yes_label'=>'Billable','no_label'=>'Non-Billable','default'=>$_smarty_tpl->tpl_vars['default_default_billable_status']->value),$_smarty_tpl);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'default_billable_status'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
    <?php }?>

      <div class="content_stack_element last">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Visibility<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'default_project_object_visibility')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'default_project_object_visibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_select_visibility(array('name'=>"settings[default_project_object_visibility]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['default_project_object_visibility'],'label'=>'Default Visibility','optional'=>true),$_smarty_tpl);?>

            <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
When creating a new object in this project, set its visibility to selected value by default<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'default_project_object_visibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
    </div>
  
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_project']->value->getSettingsUrl(),'method'=>'post'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#projectSettingsDontUseCustomTabs').click(function() {
    $('#project_tabs_settings_custom').hide();
  });
  
  $('#projectSettingsUseCustomTabs').click(function() {
    $('#project_tabs_settings_custom').show();
  });
</script><?php }} ?>