<?php /* Smarty version Smarty-3.1.12, created on 2016-04-11 04:45:51
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/notifications/email/user_failed_login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1727242135570b2bff92c1a9-04432059%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81af275a80763e0a0ead45e5b5b134df74f48ece' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/notifications/email/user_failed_login.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1727242135570b2bff92c1a9-04432059',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'language' => 0,
    'context' => 0,
    'context_view_url' => 0,
    'recipient' => 0,
    'sender' => 0,
    'max_attempts' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_570b2bff9de201_98767487',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570b2bff9de201_98767487')) {function content_570b2bff9de201_98767487($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_notification_wrapper')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrapper.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Failed Login<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

================================================================================
<?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrapper', array('title'=>'Failed Login','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value,'inspect'=>false,'open_in_browser'=>false)); $_block_repeat=true; echo smarty_block_notification_wrapper(array('title'=>'Failed Login','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value,'inspect'=>false,'open_in_browser'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('max_attempts'=>$_smarty_tpl->tpl_vars['max_attempts']->value,'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('max_attempts'=>$_smarty_tpl->tpl_vars['max_attempts']->value,'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
More than :max_attempts failed login detected with your account<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('max_attempts'=>$_smarty_tpl->tpl_vars['max_attempts']->value,'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrapper(array('title'=>'Failed Login','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value,'inspect'=>false,'open_in_browser'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>