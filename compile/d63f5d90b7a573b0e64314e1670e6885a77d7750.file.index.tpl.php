<?php /* Smarty version Smarty-3.1.12, created on 2016-04-03 15:15:34
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_outgoing_messages_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:76027621257013396197801-61067479%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd63f5d90b7a573b0e64314e1670e6885a77d7750' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_outgoing_messages_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76027621257013396197801-61067479',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'messages' => 0,
    'messages_per_page' => 0,
    'total_messages' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_570133961e6e72_73789437',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570133961e6e72_73789437')) {function content_570133961e6e72_73789437($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Outgoing Queue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Queue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="outgoing_mail_queue"></div>

<script type="text/javascript">
	$('#outgoing_mail_queue').pagedObjectsList({
    'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'outgoing_messages_admin'),$_smarty_tpl);?>
', 
    'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['messages']->value);?>
,
    'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['messages_per_page']->value,$_smarty_tpl);?>
, 
    'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_messages']->value,$_smarty_tpl);?>
, 
    'empty_message' : App.lang('There are no email messages in the queue'),   
    'on_add_item' : function(item) {
      var message = $(this);
      
      message.append('<div class="info">' + 
        '<div class="subject"></div>' + 
        '<div class="recipient"></div>' + 
        '<div class="created_on"></div>' +  
      '</div>' + 
      '<div class="retries"></div>' + 
      '<div class="options"></div>');

      message.find('div.subject').append(App.lang('Subject: <a href=":url">:subject</a>', {
        'url' : item['urls']['view'], 
        'subject' : item['subject']
      }));

      message.find('div.recipient').append(App.lang('Recipient: <a href=":url">:recipient</a>', {
        'url' : item['recipient']['urls']['view'],
        'recipient' : item['recipient']['short_display_name'] 
      }));

      message.find('div.created_on').append(App.lang('Added to Queue: :date', {
        'date' : item['created_on']['formatted']
      }));

      if(item['send_retries']) {
        message.find('div.retries').append(App.lang('Retries: <span>:retries</span>', {
          'retries' : item['send_retries']
        }));

        message.addClass('has_retries');
      } else {
        message.find('div.retries').append('--');
      } // if

      message.find('div.options')
        .append('<a href="' + item['urls']['view'] + '" class="preview_message" title="' + App.lang('Preview Message') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/preview.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['send'] + '" class="send_message" title="' + App.lang('Send Now') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/email.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_message" title="' + App.lang('Remove from Queue') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
      ;

      message.find('div.options a.preview_message').flyout();

      // Send message
      message.find('div.options a.send_message').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to send selected message now?'), 
        'success' : function(response) {
          if(message.find('div.retries span').length == 1) {
          	var retries = parseInt(message.find('div.retries span').text());
          } else {
            var retries = 0;
          } // if

          if(typeof(response) == 'object' && response && response['send_retries'] > retries) {
            if(message.find('div.retries span').length > 0) {
              message.find('div.retries span').text(response['send_retries']);
            } else {
              message.find('div.retries').empty().append(App.lang('Retries: <span>:retries</span>', {
                'retries' : response['send_retries']
              }));

              message.addClass('has_retries');
            } // if
            
            App.Wireframe.Flash.error(App.lang('Failed to send message. Reason: :reason', {
              'reason' : response['last_send_error']
            }));
          } else {
            message.addClass('sent');
            App.Wireframe.Flash.success(App.lang('Message has been sent successfully'));
          } // if
        }, 
        'error' : function() {
          App.Wireframe.Flash.error(App.lang('Failed to send selected message'));
        }
      });

      // Delete message
      message.find('div.options a.delete_message').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this message? If you delete it, message will not be sent and there is no undo!'), 
        'success' : function() {
          message.addClass('deleted');
          App.Wireframe.Flash.success(App.lang('Message has been deleted successfully'));
        }, 
        'error' : function() {
          App.Wireframe.Flash.error(App.lang('Failed to delete selected message'));
        }
      });
    }
  });
</script><?php }} ?>