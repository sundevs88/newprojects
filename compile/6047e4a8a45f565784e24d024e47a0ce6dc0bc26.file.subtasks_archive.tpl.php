<?php /* Smarty version Smarty-3.1.12, created on 2016-05-29 01:50:00
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/subtasks/views/phone/fw_subtasks/subtasks_archive.tpl" */ ?>
<?php /*%%SmartyHeaderCode:974555377574a4ac8cd0d41-14489240%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6047e4a8a45f565784e24d024e47a0ce6dc0bc26' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/subtasks/views/phone/fw_subtasks/subtasks_archive.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '974555377574a4ac8cd0d41-14489240',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'subtasks' => 0,
    'subtask' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_574a4ac8d1d325_77713282',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574a4ac8d1d325_77713282')) {function content_574a4ac8d1d325_77713282($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Completed Subtasks<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Complete<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="archived_subtasks">
	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
		<li data-role="list-divider"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/listviews/navigate-icon.png",'module'=>@SYSTEM_MODULE,'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
" class="divider_icon" alt=""><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Navigate<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
		<?php if (is_foreachable($_smarty_tpl->tpl_vars['subtasks']->value)){?>
	    <?php  $_smarty_tpl->tpl_vars['subtask'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subtask']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subtasks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subtask']->key => $_smarty_tpl->tpl_vars['subtask']->value){
$_smarty_tpl->tpl_vars['subtask']->_loop = true;
?>
	    	<li><a href="<?php echo clean($_smarty_tpl->tpl_vars['subtask']->value->getViewUrl(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['subtask']->value->getName(),$_smarty_tpl);?>
</a></li>
	    <?php } ?>
		<?php }else{ ?>
			<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No completed subtasks<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
		<?php }?>
	</ul>
</div><?php }} ?>