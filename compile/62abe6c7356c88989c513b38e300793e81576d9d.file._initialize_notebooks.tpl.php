<?php /* Smarty version Smarty-3.1.12, created on 2016-03-21 13:13:55
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/notebooks/views/default/notebooks/_initialize_notebooks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:98640770956eff393dbd2b5-74493522%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62abe6c7356c88989c513b38e300793e81576d9d' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/notebooks/views/default/notebooks/_initialize_notebooks.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '98640770956eff393dbd2b5-74493522',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'notebooks' => 0,
    'active_project' => 0,
    'in_archive' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56eff393dfe0e5_22822741',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56eff393dfe0e5_22822741')) {function content_56eff393dfe0e5_22822741($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"ui_sortable",'module'=>"environment"),$_smarty_tpl);?>

<script type="text/javascript">

(function () {
  // @todo create function that will get this data effectively
  var notebooks = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['notebooks']->value);?>
;
  var project_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
;
  var notebooks_list = $('#notebooks .notebooks_list');
  var notebooks_shelves = $('#notebooks .notebooks_shelves');
  var single_notebook_shelve = $('<div class="notebooks_shelf"><div class="notebooks_shelf_inner"><div class="notebooks_shelf_inner_2"></div></div></div>')
  var in_archive_view = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['in_archive']->value);?>
;

  if (in_archive_view) {
    var notebooks_message = $('<p class="empty_page notebooks_empty_page">' + App.lang('There are no notebooks in this archive') + '</p>').insertBefore(notebooks_shelves);
  } else {
    var notebooks_message = $('<p class="empty_page notebooks_empty_page">' + App.lang('This project does not contain any notebooks') + '</p>').insertBefore(notebooks_shelves);
  } // if


  /**
   * Render notebook
   *
   * @param notebook
   * @return object
   *|*|*|*/
  var render_notebook = function (notebook) {
    var notebook_jquery = $('<li class="notebook" notebook_id="' + notebook.id + '">' +
      '<span class="drag_handle"></span>' +
      '<a href="'+ notebook.permalink +'" class="notebook_anchor">' +
      '<span class="title_strap_left"></span><span class="title_strap_right"></span>' +
      '<span class="cover">' +
      '<img src="' + notebook.avatar.large + '" alt="" />' +
      '</span>' +
      '<span class="notebook_name"><span class="notebook_name_inner">' +
      App.clean(notebook.name) +
      '</span></span>' +
      '</a>'+
    '</li>');

    notebook_jquery = notebook_jquery.appendTo(notebooks_list);

    notebook_jquery.find('img').load(function () {
      var bottom_spacing = parseInt(notebook_jquery.find('.notebook_name').outerHeight());
      notebook_jquery.find('.title_strap_left').css('bottom', bottom_spacing);
      notebook_jquery.find('.title_strap_right').css('bottom', bottom_spacing);

      var drag_handle = notebook_jquery.find('.drag_handle');
      if (drag_handle.length) {
        drag_handle.css('bottom', Math.round(parseInt(bottom_spacing - drag_handle.height()) / 2)).hide();
        notebook_jquery.hover(function () {
          drag_handle.show();
        }, function(){
          drag_handle.hide();
        });
      } // if
    });

    update_shelves();

    if (notebooks_list.find('li').length) {
      notebooks_message.hide();
    } // if

    return notebook_jquery;
  } // render_notebook

  /**
   * Find notebook with id
   *
   * @param notebook_id
   * @return jQuery
   *|*|*|*/
  var find_notebook = function (notebook_id) {
    var notebook = notebooks_list.find('li.notebook[notebook_id="' + notebook_id + '"]');
    return notebook.length > 0 ? notebook : null;
  } // find_notebook

  /**
   * Delete existing notebook
   *
   * @param mixed notebook_id
   *|*|*|*/
  var delete_notebook = function (notebook_id) {
    if (!notebook_id) {
      return false;
    } // if

    if (typeof(notebook_id) == 'object') {
      var notebook = notebook_id;
    } else {
      var notebook = find_notebook(notebook_id);
    } // if

    notebook.remove();
    update_shelves();

    if (notebooks_list.find('li').length) {
      notebooks_message.hide();
    } // if
  } // delete_notebook

  // what happens when notebook is created
  App.Wireframe.Content.bind('notebook_created.content', function(event, notebook) {
    if (in_archive_view) {
      return false;
    } // if

    if (notebook.project_id != project_id) {
      return false;
    } // if

    render_notebook(notebook);
  });

  // what happens when notebook is updated
  App.Wireframe.Content.bind('notebook_updated.content', function(event, notebook) {
    var current_notebook = find_notebook(notebook.id);
    var notebook_state = notebook.state;

    if (current_notebook && current_notebook.length) {
      if (notebook.project_id == project_id) {
        // @todo update current notebook
      } else {
        // if project has been changed
        delete_notebook(current_notebook);
      } // if
    } else {
      if (notebook.project_id == project_id) {
        if ((in_archive_view && notebook_state == 2) || (!in_archive_view && notebook_state == 3)) {
          render_notebook(notebook);
        } // if
      } // if
    } // if
  });

  //notebooks reordering
  notebooks_list.sortable({
    items : 'li',
    handle : 'span.drag_handle',
    update : function() {
      var new_order = new Array();

      $(this).find('.notebook').each(function() {
        new_order.push($(this).attr('notebook_id'));
      });

      $.ajax({
        url : App.Config.get('reorder_notebooks_url'),
        type : 'POST',
        data : { 'new_order' : new_order.toString(), 'submitted' : 'submitted' },
        success : function(response) {
        }
      });
    }
  });

  /**
   * Function to update number of shelves on the page
   *
   * @param void
   * @return null
   *|*|*|*/
  var update_shelves = function () {
    notebooks_shelves.empty().append(single_notebook_shelve.clone());

    var number_of_rows = Math.ceil(notebooks_list.height() / notebooks_shelves.height()) - 1;

    if (number_of_rows > 0) {
      for (var counter = 0; counter < number_of_rows; counter ++) {
        notebooks_shelves.append(single_notebook_shelve.clone());
      } // for
    } // if
  }; // update_shelves

  /**
   * Init notebooks page
   *|*|*|*/
  var init_notebooks = function () {
    // add existing notebooks to the list
    if (notebooks && notebooks.length) {
      $.each(notebooks, function (index, notebook) {
        render_notebook(notebook);
      });
    } // if

    // hide empty page message if there are notebooks in the list
    if (notebooks_list.find('li').length) {
      notebooks_message.hide();
    } // if

    // initially update shelves
    update_shelves();

    // update shelves when browser resizes
    App.Wireframe.Content.bind('window_resize.content', update_shelves);
  }; // init_notebooks

  init_notebooks();
}())


</script><?php }} ?>