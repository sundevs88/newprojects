<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 18:09:00
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/project/_projects_progress.tpl" */ ?>
<?php /*%%SmartyHeaderCode:114166755856d9cf3c82b0b6-88710455%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ee4f0e4b7b8d5236854cf0e9d6fc4f68c1a7bec' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/project/_projects_progress.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114166755856d9cf3c82b0b6-88710455',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_project_progress' => 0,
    '_project_progress_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d9cf3c872410_26829548',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d9cf3c872410_26829548')) {function content_56d9cf3c872410_26829548($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><div class="project_progress">
<?php if ($_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount()){?>
  <div class="progress_wrapper" <?php if (!$_smarty_tpl->tpl_vars['_project_progress_info']->value){?>title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount(),'percent'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone())); $_block_repeat=true; echo smarty_block_lang(array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount(),'percent'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:completed of :total tasks completed (:percent%)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount(),'percent'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"<?php }?>>
    <div class="progress" style="width: <?php echo clean($_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone(),$_smarty_tpl);?>
%"><span><?php echo clean($_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone(),$_smarty_tpl);?>
%</span></div>
    <div class="progress_label"><?php echo clean($_smarty_tpl->tpl_vars['_project_progress']->value->getPercentsDone(),$_smarty_tpl);?>
%</div>
  </div>
  <?php if ($_smarty_tpl->tpl_vars['_project_progress_info']->value){?>
  	<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount())); $_block_repeat=true; echo smarty_block_lang(array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<strong>:completed</strong> of <strong>:total</strong> tasks completed<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('completed'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getCompletedTaskCount(),'total'=>$_smarty_tpl->tpl_vars['_project_progress']->value->getTotalTasksCount()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
  <?php }?>
<?php }else{ ?>
  <div class="progress_wrapper"></div>
  <?php if ($_smarty_tpl->tpl_vars['_project_progress_info']->value){?>
  	<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no tasks in this project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
  <?php }?>
<?php }?>
</div><?php }} ?>