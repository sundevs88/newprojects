<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:20:27
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_hourly_rates/edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9008798535742e77be91233-32998238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7dccea0beff562b00e0de2731cfc7eeb263bad6d' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_hourly_rates/edit.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9008798535742e77be91233-32998238',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'active_job_type' => 0,
    'project_hourly_rate_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e77beefd45_66011959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e77beefd45_66011959')) {function content_5742e77beefd45_66011959($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_money_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.money_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><div id="update_project_hourly_rate">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_job_type']->value->getProjectHourlyRateUrl($_smarty_tpl->tpl_vars['active_project']->value))); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_job_type']->value->getProjectHourlyRateUrl($_smarty_tpl->tpl_vars['active_project']->value)), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div>
      <input type="radio" name="project_hourly_rate[use_custom]" id="dont_use_custom_project_hourly_rate" <?php if (!$_smarty_tpl->tpl_vars['project_hourly_rate_data']->value['use_custom']){?>checked="checked"<?php }?> value="0"> <label for="dont_use_custom_project_hourly_rate"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Use Default Hourly Rate<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 (<?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_job_type']->value->getDefaultHourlyRate()),$_smarty_tpl);?>
)</label>
    </div>
    
    <div>
      <input type="radio" name="project_hourly_rate[use_custom]" id="use_custom_project_hourly_rate" <?php if ($_smarty_tpl->tpl_vars['project_hourly_rate_data']->value['use_custom']){?>checked="checked"<?php }?> value="1"> <label for="use_custom_project_hourly_rate"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Specify a Different Hourly Rate for this Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</label>
    </div>
    
    <div id="custom_project_hourly_rate_settings" class="slide_down_settings">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'hourly_rate')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'hourly_rate'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php echo smarty_function_money_field(array('name'=>"project_hourly_rate[hourly_rate]",'value'=>$_smarty_tpl->tpl_vars['project_hourly_rate_data']->value['hourly_rate'],'label'=>"Hourly Rate",'required'=>true),$_smarty_tpl);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'hourly_rate'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
  
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_job_type']->value->getProjectHourlyRateUrl($_smarty_tpl->tpl_vars['active_project']->value)), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#dont_use_custom_project_hourly_rate').each(function() {
    if(this.checked) {
      $('#update_project_hourly_rate #custom_project_hourly_rate_settings').hide();
    } else {
      $('#update_project_hourly_rate #custom_project_hourly_rate_settings input[type=number]:first').focus();
    } // if

    $(this).click(function() {
      $('#update_project_hourly_rate #custom_project_hourly_rate_settings').slideUp('fast');
    });
  });

  $('#use_custom_project_hourly_rate').click(function() {
    $('#update_project_hourly_rate #custom_project_hourly_rate_settings').slideDown('fast', function() {
      $(this).find('input[type=number]:first').focus();
    });
  });
  
  $('#update_project_hourly_rate form div:eq(0), #update_project_hourly_rate form div:eq(1)').css('padding', '0 20px');
</script><?php }} ?>