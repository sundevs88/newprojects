<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:18:07
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/_render_invoice.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7966566265742e6efe75358-10139171%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e91f0a7d70851ab30fb5d1a8d7b3ede3d318c11d' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/_render_invoice.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7966566265742e6efe75358-10139171',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_invoice' => 0,
    'logged_user' => 0,
    'invoice_class' => 0,
    'invoice_template' => 0,
    'invoice_item' => 0,
    'totals_colspan' => 0,
    'grouped_taxes' => 0,
    'grouped_tax' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e6f01f9931_95325418',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e6f01f9931_95325418')) {function content_5742e6f01f9931_95325418($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_object')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.object.php';
if (!is_callable('smarty_block_assign_var')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.assign_var.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_clean')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.clean.php';
if (!is_callable('smarty_function_object_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.object_link.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_function_company_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.company_link.php';
if (!is_callable('smarty_modifier_nl2br')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.nl2br.php';
if (!is_callable('smarty_function_cycle')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/vendor/smarty/smarty/plugins/function.cycle.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"invoice_update_property",'module'=>"invoicing"),$_smarty_tpl);?>

<?php echo smarty_function_use_widget(array('name'=>"payment_container",'module'=>"payments"),$_smarty_tpl);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('object', array('object'=>$_smarty_tpl->tpl_vars['active_invoice']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value)); $_block_repeat=true; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_invoice']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php $_smarty_tpl->smarty->_tag_stack[] = array('assign_var', array('name'=>'invoice_class')); $_block_repeat=true; echo smarty_block_assign_var(array('name'=>'invoice_class'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->isDraft()){?>
      invoice_draft
    <?php }elseif($_smarty_tpl->tpl_vars['active_invoice']->value->isIssued()){?>
      invoice_issued 
    <?php }elseif($_smarty_tpl->tpl_vars['active_invoice']->value->isPaid()){?>
      invoice_paid
    <?php }elseif($_smarty_tpl->tpl_vars['active_invoice']->value->isCanceled()){?>
      invoice_canceled
    <?php }?>
  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_assign_var(array('name'=>'invoice_class'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  
  <div class="invoice_paper_wrapper <?php echo clean(trim($_smarty_tpl->tpl_vars['invoice_class']->value),$_smarty_tpl);?>
_wrapper invoice_<?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getId(),$_smarty_tpl);?>
">
    <div class="invoice_paper <?php echo clean(trim($_smarty_tpl->tpl_vars['invoice_class']->value),$_smarty_tpl);?>
">
      <div class="invoice_paper_top"></div>
      <div class="invoice_paper_center">
        <div class="invoice_paper_area">
          <div class="invoice_paper_logo"></div>
          <?php if ($_smarty_tpl->tpl_vars['logged_user']->value->isFinancialManager()){?>
          <div class="invoice_comment property_wrapper" style="display: <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getPrivateNote()){?>block<?php }else{ ?>none<?php }?>"><span class="invoice_comment_paperclip"></span><span class="property_invoice_comment"><?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getPrivateNote(),$_smarty_tpl);?>
</span></div>
          <?php }?>
          <div class="invoice_paper_header">
            <div class="invoice_paper_details">
              <h2><span class="property_invoice_name"><?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getName(),$_smarty_tpl);?>
</span></h2>
              <ul>

                <li class="invoice_purchase_order_number property_wrapper" <?php if (!$_smarty_tpl->tpl_vars['active_invoice']->value->getPurchaseOrderNumber()){?>style="display: none"<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Purchase Order #<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <strong><span class="property_invoice_purchase_order_number"><?php echo clean(smarty_modifier_clean($_smarty_tpl->tpl_vars['active_invoice']->value->getPurchaseOrderNumber()),$_smarty_tpl);?>
</span></strong></li>
                <li class="invoice_currency property_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Currency<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <strong><span class="property_invoice_currency"><?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getCurrencyCode(),$_smarty_tpl);?>
</span></strong></li>
                <li class="invoice_project property_wrapper" <?php if (!($_smarty_tpl->tpl_vars['active_invoice']->value->getProject() instanceof Project)){?> style="display: none" <?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_project"><?php if (($_smarty_tpl->tpl_vars['active_invoice']->value->getProject() instanceof Project)){?><?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getProject()),$_smarty_tpl);?>
<?php }?></span></li>

                <li class="invoice_created_on property_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Created On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_created_on"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_invoice']->value->getCreatedOn()),$_smarty_tpl);?>
</span></li>
                <li class="invoice_issued_on property_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Issued On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_issued_on"><?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getIssuedOn()){?><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_invoice']->value->getIssuedOn(),0),$_smarty_tpl);?>
<?php }?></span></li>
                <li class="invoice_due_on property_wrapper" <?php if (!$_smarty_tpl->tpl_vars['active_invoice']->value->getDueOn()){?> style="display: none" <?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Payment Due On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_due_on"><?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getDueOn()){?><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_invoice']->value->getDueOn(),0),$_smarty_tpl);?>
<?php }?></span></li>
                <li class="invoice_paid_on property_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paid On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_paid_on"><?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getClosedOn()){?><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_invoice']->value->getClosedOn()),$_smarty_tpl);?>
<?php }?></span></li>
                <li class="invoice_closed_on property_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Closed On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <span class="property_invoice_closed_on"><?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getClosedOn()){?><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_invoice']->value->getClosedOn()),$_smarty_tpl);?>
<?php }?></span></li>
                
              </ul>
            </div>

            <div class="invoice_paper_client"><div class="invoice_paper_client_inner">
              <div class="invoice_paper_client_name property_wrapper"><span class="property_invoice_client_name"><?php echo smarty_function_company_link(array('company'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getCompany()),$_smarty_tpl);?>
</span></div>
              <div class="invoice_paper_client_address property_wrapper"><span class="property_invoice_client_address"><?php echo smarty_modifier_nl2br(smarty_modifier_clean($_smarty_tpl->tpl_vars['active_invoice']->value->getCompanyAddress()));?>
</span></div>
            </div></div>
          </div>

          <div class="invoice_paper_items">
            <?php if (is_foreachable($_smarty_tpl->tpl_vars['active_invoice']->value->getItems())){?>
              <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>
                <?php $_smarty_tpl->tpl_vars["totals_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['invoice_template']->value->getColumnsCount()+1, null, 0);?>
              <?php }else{ ?>
                <?php $_smarty_tpl->tpl_vars["totals_colspan"] = new Smarty_variable($_smarty_tpl->tpl_vars['invoice_template']->value->getColumnsCount(), null, 0);?>
              <?php }?>

              <table cellspacing="0">
                <thead>
                  <tr class='items_head'>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayItemOrder()){?><td class="num"></td><?php }?>
                    <td class="description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayQuantity()){?><td class="quantity"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Qty.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayUnitCost()){?><td class="unit_cost"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unit Cost<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplaySubtotal()){?><td class="subtotal"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subtotal<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>
                      <?php if (($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate()||$_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount())){?><td class="tax_rate"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax #1<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                      <?php if (($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate()||$_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount())){?><td class="tax_rate"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax #2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                    <?php }else{ ?>
                      <?php if (($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate()||$_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount())){?><td class="tax_rate"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTotal()){?><td class="total"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td><?php }?>
                  </tr>
                </thead>

                <tbody>
                <?php  $_smarty_tpl->tpl_vars['invoice_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['invoice_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['active_invoice']->value->getItems(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_foreach']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['invoice_item']->key => $_smarty_tpl->tpl_vars['invoice_item']->value){
$_smarty_tpl->tpl_vars['invoice_item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_foreach']['iteration']++;
?>
                  <tr class="<?php echo smarty_function_cycle(array('values'=>'odd,even'),$_smarty_tpl);?>
">
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayItemOrder()){?><td class="num">#<?php echo clean($_smarty_tpl->getVariable('smarty')->value['foreach']['item_foreach']['iteration'],$_smarty_tpl);?>
</td><?php }?>
                    <td class="description"><?php echo $_smarty_tpl->tpl_vars['invoice_item']->value->getFormattedDescription();?>
</td>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayQuantity()){?><td class="quantity"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getQuantity(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayUnitCost()){?><td class="unit_cost"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getUnitCost(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplaySubtotal()){?><td class="subtotal"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getSubtotal(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate()){?><td class="tax_rate"><?php echo clean($_smarty_tpl->tpl_vars['invoice_item']->value->getFirstTaxRatePercentageVerbose(),$_smarty_tpl);?>
</td><?php }elseif($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount()){?><td class="tax_amount"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getFirstTax(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>
                      <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate()){?><td class="tax_rate"><?php echo clean($_smarty_tpl->tpl_vars['invoice_item']->value->getSecondTaxRatePercentageVerbose(),$_smarty_tpl);?>
</td><?php }elseif($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount()){?><td class="tax_amount"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getSecondTax(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTotal()){?><td class="total"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['invoice_item']->value->getTotal(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</td><?php }?>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                  <tr class="subtotals_row">
                    <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subtotal<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <td class="value"><span class="property_wrapper property_invoice_subtotal"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getSubTotal(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                  </tr>

                  <?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getSummarizeTax()||!is_foreachable($_smarty_tpl->tpl_vars['active_invoice']->value->getTaxGroupedByType())){?>
                    <tr class="tax_row" style="<?php if ($_smarty_tpl->tpl_vars['invoice_template']->value->getHideTaxSubtotal()&&$_smarty_tpl->tpl_vars['active_invoice']->value->getTax()==0){?>display: none;<?php }?>">
                      <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                      <td class="value"><span class="property_wrapper property_invoice_tax"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getTax(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                    </tr>
                  <?php }else{ ?>
                    <?php $_smarty_tpl->tpl_vars['grouped_taxes'] = new Smarty_variable($_smarty_tpl->tpl_vars['active_invoice']->value->getTaxGroupedByType(), null, 0);?>
                    <?php  $_smarty_tpl->tpl_vars['grouped_tax'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grouped_tax']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['grouped_taxes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grouped_tax']->key => $_smarty_tpl->tpl_vars['grouped_tax']->value){
$_smarty_tpl->tpl_vars['grouped_tax']->_loop = true;
?>
                    <tr class="tax_row">
                      <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php echo clean($_smarty_tpl->tpl_vars['grouped_tax']->value['name'],$_smarty_tpl);?>
</td>
                      <td class="value"><span class="property_wrapper property_invoice_tax"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['grouped_tax']->value['amount'],$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                    </tr>
                    <?php } ?>
                  <?php }?>

                  <tr class="property_wrapper" style="<?php if (!$_smarty_tpl->tpl_vars['active_invoice']->value->requireRounding()){?>display: none<?php }?>">
                    <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rounding Difference<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <td class="value total"><span class="property_wrapper property_invoice_rounding_difference"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getRoundingDifference(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                  </tr>

                  <tr class="total">
                    <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <td class="value total"><span class="property_wrapper property_invoice_total"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getTotal(true),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                  </tr>

                  <tr class="amount_paid">
                    <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Amount Paid<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <td class="value total"><span class="property_wrapper property_invoice_amount_paid"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getPaidAmount(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                  </tr>

                  <tr class="balance_due">
                    <td colspan=<?php echo clean($_smarty_tpl->tpl_vars['totals_colspan']->value,$_smarty_tpl);?>
 class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Balance Due<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
                    <td class="value total"><span class="property_wrapper property_balance_due"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['active_invoice']->value->getBalanceDue(),$_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency(),$_smarty_tpl->tpl_vars['active_invoice']->value->getLanguage()),$_smarty_tpl);?>
</span></td>
                  </tr>

                </tfoot>
              </table>
            <?php }else{ ?>
              <p class="empty_page"><span class="inner"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This invoice has no items<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></p>
            <?php }?>
          </div>


          <div class="invoice_paper_notes property_wrapper" style="display: <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getNote()){?>block<?php }else{ ?>none<?php }?>">
            <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Note<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
            <p><span class="property_invoice_note"><?php echo smarty_modifier_nl2br(smarty_modifier_clean($_smarty_tpl->tpl_vars['active_invoice']->value->getNote()));?>
</span></p>
          </div>

        </div>
      </div>
      <div class="invoice_paper_bottom"></div>
      
      <div class="invoice_paper_peel_draft"></div>
      <div class="invoice_paper_stamp_paid"></div>
      <div class="invoice_paper_stamp_canceled"></div>
    </div>
  </div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_invoice']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
  var decimal_spaces = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_invoice']->value->getCurrency()->getDecimalSpaces());?>
;
  var second_tax_enabled = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled());?>
;

  var get_display_item_order = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayItemOrder());?>
;
  var get_display_quantity = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayQuantity());?>
;
  var get_display_unit_cost = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayUnitCost());?>
;
  var get_display_subtotal = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplaySubtotal());?>
;
  var get_display_tax_rate = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxRate());?>
;
  var get_display_tax_amount = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTaxAmount());?>
;
  var get_display_total = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getDisplayTotal());?>
;
  var summarize_tax = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getSummarizeTax());?>
;
  var hide_tax_subtotal = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_template']->value->getHideTaxSubtotal());?>
;

  $('.invoice_<?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getId(),$_smarty_tpl);?>
').each(function() {
    var wrapper = $(this);

    var wrapper_paper = wrapper.find('.invoice_paper:first');

  <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->isOverdue()&&$_smarty_tpl->tpl_vars['active_invoice']->value->isIssued()){?>
    wrapper.addClass('invoice_overdue_wrapper');
    wrapper_paper.addClass('invoice_overdue');
  <?php }?>

    App.Wireframe.Events.bind('resend_email.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function(event, invoice) {
      if(invoice['id']) {
        App.Wireframe.Flash.success('Email notification has been sent');
        App.Wireframe.Events.trigger('invoice_updated',invoice);
      } //if
    });

    /**
     * Handle what happens when invoices get issued
     */
    App.Wireframe.Events.bind('invoice_updated.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function(event, invoice) {
      $('#render_object_payments').paymentContainer('refresh', invoice);

      wrapper.removeClass('invoice_canceled_wrapper invoice_draft_wrapper invoice_issued_wrapper invoice_paid_wrapper invoice_overdue_wrapper');
      wrapper_paper.removeClass('invoice_canceled invoice_draft invoice_issued invoice_paid invoice_overdue');

      // update invoice properties
      wrapper.invoiceUpdateProperty([
        {
          'name' : 'invoice_name',
          'value' : App.clean(invoice.name),
          'auto_hide' : false
        }, {
          'name': 'invoice_currency',
          'value': App.clean(invoice.currency.code),
          'auto_hide': false
        }, {
          'name': 'invoice_purchase_order_number',
          'value': invoice.purchase_order_number ? App.clean(invoice.purchase_order_number) : null,
          'auto_hide': true
        }, {
          'name': 'invoice_project',
          'value': invoice.project ? '<a href="' + invoice.project.permalink + '">' + App.clean(invoice.project.name) + '</a>' : null
        }, {
          'name': 'invoice_created_on',
          'value' : invoice.created_on.formatted_date
        }, {
          'name': 'invoice_issued_on',
          'value' : typeof(invoice['issued_on']) == 'object' && invoice['issued_on'] ? invoice['issued_on']['formatted_date_gmt'] : null
        }, {
          'name': 'invoice_due_on',
          'value' : typeof(invoice['due_on']) == 'object' && invoice['due_on'] ? invoice['due_on']['formatted_date_gmt'] : null,
          'auto_hide' : true
        }, {
          'name': 'invoice_paid_on',
          'value' : typeof(invoice['paid_on']) == 'object' && invoice['paid_on'] ? invoice['paid_on']['formatted_date'] : null
        }, {
          'name': 'invoice_closed_on',
          'value' : typeof(invoice['closed_on']) == 'object' && invoice['closed_on'] ? invoice['closed_on']['formatted_date'] : null
        }, {
          'name': 'invoice_client_name',
          'value' : '<a href="' + invoice.client.permalink + '">' + App.clean(invoice.client.name) + '</a>'
        }, {
          'name': 'invoice_client_address',
          'value' : typeof(invoice['client_address']) == 'string' ? App.clean(invoice['client_address']).nl2br() : ''
        }, {
          'name': 'invoice_subtotal',
          'value' : App.moneyFormat(invoice.subtotal, invoice.currency, invoice.language),
          'auto_hide' : false
        }, {
          'name': 'invoice_total',
          'value' : App.moneyFormat(invoice.total, invoice.currency, invoice.language),
          'auto_hide' : false
        }, {
          'name': 'invoice_amount_paid',
          'value' : App.moneyFormat(invoice.paid_amount, invoice.currency, invoice.language),
          'auto_hide' : false
        }, {
          'name': 'balance_due',
          'value' : App.moneyFormat(invoice.balance_due, invoice.currency, invoice.language),
          'auto_hide' : false
        }, {
          'name': 'invoice_note',
          'value' : typeof(invoice['note']) == 'string' ? App.clean(invoice['note']).nl2br() : '',
          'auto_hide' : true
        }, {
          'name': 'invoice_comment',
          'value' : typeof(invoice['private_note']) == 'string' ? App.clean(invoice['private_note']) : '',
          'auto_hide' : true
        }, {
          'name' : 'invoice_rounding_difference',
          'value' : App.moneyFormat(invoice.rounding_difference, invoice.currency, invoice.language),
          'auto_hide' : true
        }
      ]);

      // update invoice items
      var items_parent = wrapper.find('.invoice_paper_items table tbody').empty();
      var new_items = '';

      if (invoice.items && invoice.items.length) {
        var counter = 1;
        $.each(invoice.items, function (item_index, item) {
          new_items += '<tr>';

          // item number
          if (get_display_item_order) {
            new_items += '<td class="num">#' + counter + '</td>';
          } // if

          // description
          new_items += '<td class="description">' + item['formatted_description'] + '</td>';

          // quantity
          if (get_display_quantity) {
            new_items += '<td class="quantity">' + App.moneyFormat(item['quantity'], invoice['currency'], invoice['language']) + '</td>';
          } // if

          // unit cost
          if (get_display_unit_cost) {
            new_items += '<td class="unit_cost">' + App.moneyFormat(item['unit_cost'], invoice['currency'], invoice['language']) + '</td>';
          } // if

          // item subtotal
          if (get_display_subtotal) {
            new_items += '<td class="subtotal">' + App.moneyFormat(item['subtotal'], invoice['currency'], invoice['language']) + '</td>';
          } // if

          // item tax
          if (get_display_tax_rate) {
            new_items += '<td class="tax_rate">' + item['first_tax']['verbose_percentage'] + '</td>';
            if (second_tax_enabled) {
              new_items += '<td class="tax_rate">' + item['second_tax']['verbose_percentage'] + '</td>';
            } // if
          } else if (get_display_tax_amount) {
            new_items += '<td class="tax_amount">' + App.moneyFormat(item['first_tax']['value'], invoice['currency'], invoice['language']) + '</td>';
            if (second_tax_enabled) {
              new_items += '<td class="tax_amount">' + App.moneyFormat(item['second_tax']['value'], invoice['currency'], invoice['language']) + '</td>';
            } // if
          } // if

          // item total
          if (get_display_total) {
            new_items += '<td class="total">' + App.moneyFormat(item['total'], invoice['currency'], invoice['language']) + '</td>';
          } // if

          new_items += '</tr>'

          counter++;
        });

        items_parent.html(new_items);
      } // if

      var tax_row = wrapper.find('tr.tax_row');

      if (summarize_tax) {
        wrapper.invoiceUpdateProperty([{
          'name': 'invoice_tax',
          'value' : App.moneyFormat(invoice.tax, invoice.currency, invoice.language),
          'auto_hide' : false
        }]);

        if(hide_tax_subtotal && invoice.tax == 0) {
          tax_row.hide();
        } else {
          tax_row.show();
        } // if
      } else {
        tax_row.remove();

        var insert_pointer = wrapper.find('tr.subtotals_row');
        var grouped_taxes = invoice['tax_grouped_by_type'];
        var col_span = insert_pointer.find('td.label').attr('colspan');
        var inserted = false;

        if (grouped_taxes && !$.isEmptyObject(grouped_taxes)) {
          $.each(grouped_taxes, function (index, grouped_tax) {
            if (grouped_tax['amount']) {
              insert_pointer = $('<tr class="tax_row"><td colspan="' + col_span + '" class="label">' + App.clean(grouped_tax['name']) + '</td><td class="value"><span class="property_wrapper property_invoice_tax">' + App.moneyFormat(grouped_tax['amount'], invoice.currency, invoice.language) + '</span></td></tr>').insertAfter(insert_pointer);
              inserted = true;
            } // if
          });
        } // if

        if (!inserted) {
          insert_pointer = $('<tr class="tax_row"><td colspan="' + col_span + '" class="label">' + App.lang('Tax') + '</td><td class="value"><span class="property_wrapper property_invoice_tax">' + App.moneyFormat(0, invoice.currency, invoice.language) + '</span></td></tr>').insertAfter(insert_pointer);
        } // if
      } // if

      // DRAFT
      if (invoice.status_conditions.is_draft) {
        wrapper.addClass('invoice_draft_wrapper');
        wrapper_paper.addClass('invoice_draft');

      // ISSUED
      } else if (invoice.status_conditions.is_issued) {
        wrapper.addClass('invoice_issued_wrapper');
        wrapper_paper.addClass('invoice_issued');

        if (invoice.status_conditions.is_overdue) {
          wrapper.addClass('invoice_overdue_wrapper');
          wrapper_paper.addClass('invoice_overdue');
        } // if

        wrapper.invoiceUpdateProperty([{
          'name' : 'invoice_name',
          'value' : App.clean(invoice.name),
          'auto_hide' : false
        }, {
          'name' : 'invoice_issued_on',
          'value' : invoice.issued_on.formatted_date_gmt
        }, {
          'name' : 'invoice_due_on',
          'value' : invoice.due_on.formatted_date_gmt,
          'auto_hide' : true
        }]);

      // Paid
      } else if (invoice.status_conditions.is_paid) {
        wrapper.addClass('invoice_paid_wrapper');
        wrapper_paper.addClass('invoice_paid');

      // CANCELED
      } else if (invoice.status_conditions.is_canceled) {
        wrapper.addClass('invoice_canceled_wrapper');
        wrapper_paper.addClass('invoice_canceled');

        wrapper.invoiceUpdateProperty([{
          'name' : 'invoice_name',
          'value' : App.clean(invoice.name),
          'auto_hide' : false
        }, {
          'name' : 'invoice_closed_on',
          'value' : invoice.closed_on.formatted_date
        }]);
      } // if

    });
  });
</script><?php }} ?>