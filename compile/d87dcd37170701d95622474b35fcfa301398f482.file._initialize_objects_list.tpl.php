<?php /* Smarty version Smarty-3.1.12, created on 2016-06-07 14:32:49
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/recurring_invoice/_initialize_objects_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15476912935756db11e66225-74329901%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd87dcd37170701d95622474b35fcfa301398f482' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/recurring_invoice/_initialize_objects_list.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15476912935756db11e66225-74329901',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'recurring_profiles' => 0,
    'companies_map' => 0,
    'print_url' => 0,
    'in_archive' => 0,
    'active_recurring_profile' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5756db11e89a46_33084139',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5756db11e89a46_33084139')) {function content_5756db11e89a46_33084139($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
?><script type="text/javascript">
  $('#new_recurring_profile').flyoutForm({
    'success_event' : 'recurring_profile_created',
    'title' : App.lang('New Recurring Profile')
  });

  $('#recurring_profile').each(function() {
    var objects_list_wrapper = $(this);

    var items = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['recurring_profiles']->value);?>
;
    var companies_map = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['companies_map']->value);?>
;
    var mass_edit_url = '<?php echo smarty_function_assemble(array('route'=>'recurring_profiles_mass_edit'),$_smarty_tpl);?>
';

    var print_url = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['print_url']->value);?>
;

    var in_archive = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['in_archive']->value);?>


    objects_list_wrapper.find('div#skipped_profiles_wrapper td.skipped_link a').asyncLink({
      'success_event' : 'recurring_profile_updated',
      'confirmation' : App.lang('Are you sure that you want to trigger this profile?'),
      'success_message' : App.lang('Recurring invoice profile has been successfully triggered')
    });

    objects_list_wrapper.objectsList({
      'id'                : 'recurring_invoices',
      'items'             : items,
      'required_fields'   : ['id', 'name', 'client_id', 'permalink'],
      'objects_type'      : 'recurring_profiles',
      'events'            : App.standardObjectsListEvents(),
      'multi_title'       : App.lang(':num Recurring Profiles Selected'),
      'multi_url'         : mass_edit_url,
      'print_url'          : print_url,
      'prepare_item'      : function (item) {
        return {
          'id' : item['id'],
          'name' : item['name'],
          'client_id' : item['client']['id'],
          'permalink' : item['urls']['view'],
          'is_archived' : item['state'] == '2' ? '1' : '0',
          'is_skipped' : item['is_skipped']
        };
      },
      'render_item'       : function (item) {
        var warning_image = '';
        if (item.is_skipped) {
          warning_image = '<img src="' + App.Wireframe.Utils.indicatorUrl('warning') + '" id="item_icon_' + item.id + '" />';
        }//if
        return '<td class="recurring_profile_name">' + App.clean(item.name) + '</td><td class="recurring_profile_attention_image"> ' + warning_image + '</td>';
      },

      'grouping'          : [{
        'label' : App.lang("Don't group"),
        'property' : '',
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/dont-group.png', 'environment')
      }, {
        'label' : App.lang('By Client'),
        'property' : 'company_id',
        'map' : companies_map ,
        'icon' : App.Wireframe.Utils.imageUrl('objects-list/group-by-client.png', 'system'),
        'uncategorized_label' : App.lang('Unknown Client'),
        'default' : true
      }]

    });
    // recurring profile added
    App.Wireframe.Events.bind('recurring_profile_created.content', function (event, profile) {
      objects_list_wrapper.objectsList('add_item', profile);
    });

    // recurring profile update
    App.Wireframe.Events.bind('recurring_profile_updated.content', function (event, profile) {
      if((in_archive && profile['is_archived'] == 0) || (!in_archive && profile['is_archived'] == 1)) {
        objects_list_wrapper.objectsList('delete_item', profile['id']);
        objects_list_wrapper.objectsList('load_empty');
        if(profile['is_archived'] == 1) {
          remove_skipped_table(profile);
        }//if
        return false;
      } //if

      objects_list_wrapper.objectsList('update_item', profile);
      remove_skipped_table(profile);
    });

    function remove_skipped_table(profile) {
      var skipped_profiles_wrapper = objects_list_wrapper.find('div#skipped_profiles_wrapper');
      skipped_profiles_wrapper.find('tr#skipped_profile_' + profile.id).remove();
      if(skipped_profiles_wrapper.find('.skipped_profiles_table tr').length == 0) {
        skipped_profiles_wrapper.remove();
      }
    } //remove_skiped_table

    // recurring profile deleted
    App.Wireframe.Events.bind('recurring_profile_deleted.content', function (event, profile) {
      objects_list_wrapper.objectsList('delete_item', profile['id']);
    });

    // keep company_id map up to date
    App.objects_list_keep_companies_map_up_to_date(objects_list_wrapper, 'client_id', 'content');

    // Pre select item if this is permalink
    <?php if ($_smarty_tpl->tpl_vars['active_recurring_profile']->value->isLoaded()){?>
      objects_list_wrapper.objectsList('load_item', <?php echo clean($_smarty_tpl->tpl_vars['active_recurring_profile']->value->getId(),$_smarty_tpl);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_recurring_profile']->value->getViewUrl());?>
);
    <?php }?>

  });
</script><?php }} ?>