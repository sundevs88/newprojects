<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 05:04:01
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking/object_tracking_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:178944195456d7c5c14456b0-69350452%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ddf84e75c434c06cf072924a86dfbec0a3b580e5' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking/object_tracking_list.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '178944195456d7c5c14456b0-69350452',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_tracking_object' => 0,
    'logged_user' => 0,
    'can_add' => 0,
    'can_track_for_others' => 0,
    'active_tracking_object_job_type_id' => 0,
    'items' => 0,
    'item' => 0,
    'logged_user_language' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7c5c159b049_48388774',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7c5c159b049_48388774')) {function content_56d7c5c159b049_48388774($_smarty_tpl) {?><?php if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_object_estimate')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.object_estimate.php';
if (!is_callable('smarty_function_select_project_user')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project_user.php';
if (!is_callable('smarty_function_user_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/helpers/function.user_link.php';
if (!is_callable('smarty_function_select_job_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_job_type.php';
if (!is_callable('smarty_function_select_expense_category')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_expense_category.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_billable_status')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_billable_status.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_modifier_hours')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.hours.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
?><div class="object_time_and_expenses" id="object_time_and_expenses_for_<?php echo clean($_smarty_tpl->tpl_vars['active_tracking_object']->value->getId(),$_smarty_tpl);?>
" add_time_url="<?php echo clean($_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddTimeUrl(),$_smarty_tpl);?>
" add_expense_url="<?php echo clean($_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddExpenseUrl(),$_smarty_tpl);?>
" time_icon_url="<?php echo smarty_function_image_url(array('name'=>'icons/16x16/time-record.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" expense_icon_url="<?php echo smarty_function_image_url(array('name'=>'icons/16x16/expense.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
">
  <div class="object_inspector_wrapper">
    
    <ul class="object_tracking_totals">
    
      <li class="object_tracking_totals_estimated_time">
        <span class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Estimated Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
        <span class="value"><?php echo smarty_function_object_estimate(array('object'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</span>
      </li>
      
      <li class="object_tracking_totals_tracked_time">
        <span class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logged Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
        <span class="value object_total_time"></span>      
      </li>
      
      <li class="object_tracking_totals_tracked_expenses">
        <span class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logged Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
        <span class="value object_total_expenses"></span>      
      </li>
      
    </ul>

  </div>
<?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
  <form action="<?php echo clean($_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddTimeUrl(),$_smarty_tpl);?>
" method="post">
<?php }?>
    <div class="table_wrapper">
    <table class="common" cellspacing="0">
      <thead>
        <tr>
          <th class="type"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="user"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="job_type"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Job<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 / <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Categ.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="record_date"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Day<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="value right"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="summary"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Summary<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="status"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
          <th class="options"></th>
        </tr>
      </thead>
      <tbody>
      <?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
        <tr class="add_time_or_expense highlighted">
          <td class="type">
            <span class="tracking_type_toggler"></span>
          </td>
          <td class="user">
            <?php if ($_smarty_tpl->tpl_vars['can_track_for_others']->value){?>
              <?php echo smarty_function_select_project_user(array('name'=>'item[user_id]','value'=>$_smarty_tpl->tpl_vars['logged_user']->value->getId(),'project'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->getProject(),'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'optional'=>false,'short'=>true),$_smarty_tpl);?>

            <?php }else{ ?>
              <?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'short'=>true),$_smarty_tpl);?>

              <input type="hidden" name="item[user_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['logged_user']->value->getId(),$_smarty_tpl);?>
" />
            <?php }?>
          </td>
          <td class="job_type">
            <?php if ($_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getEstimate() instanceof Estimate){?>
              <?php $_smarty_tpl->tpl_vars["active_tracking_object_job_type_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getEstimate()->getJobTypeId(), null, 0);?>
            <?php }?>
            <?php echo smarty_function_select_job_type(array('name'=>'item[job_type_id]','user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'class'=>"timerecord_control",'value'=>$_smarty_tpl->tpl_vars['active_tracking_object_job_type_id']->value),$_smarty_tpl);?>

            <?php echo smarty_function_select_expense_category(array('name'=>'item[category_id]','class'=>"expense_control"),$_smarty_tpl);?>

          </td>
          <td class="record_date"><?php echo smarty_function_select_date(array('name'=>'item[record_date]','value'=>DateTimeValue::now()->getForUser($_smarty_tpl->tpl_vars['logged_user']->value)),$_smarty_tpl);?>
</td>
          <td class="value right"><input type="text" name="item[value]" /></td>
          <td class="summary"><input type="text" name="item[summary]"></td>
          <td class="status">
            <?php echo smarty_function_select_billable_status(array('name'=>'item[billable_status]'),$_smarty_tpl);?>

          </td>
          <td class="options"><button type="submit"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</button></td>
        </tr>
      <?php }?>
      
    <?php if ($_smarty_tpl->tpl_vars['items']->value){?>
      <?php $_smarty_tpl->tpl_vars['logged_user_langauge'] = new Smarty_variable($_smarty_tpl->tpl_vars['logged_user']->value->getLanguage(), null, 0);?>

      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <tr class="item <?php if ($_smarty_tpl->tpl_vars['item']->value['class']=='TimeRecord'){?>time_record<?php }else{ ?>expense<?php }?>" record_value="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['value'],$_smarty_tpl);?>
">
        <?php if ($_smarty_tpl->tpl_vars['item']->value['class']=='TimeRecord'){?>
          <td class="type"><img src="<?php echo smarty_function_image_url(array('name'=>'icons/16x16/time-record.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time Record<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" /></td>
        <?php }else{ ?>
          <td class="type"><img src="<?php echo smarty_function_image_url(array('name'=>'icons/16x16/expense.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expense<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" /></td>
        <?php }?>
          <td class="user"><a class="user_link <?php if ($_smarty_tpl->tpl_vars['item']->value['user']['id']==0||!$_smarty_tpl->tpl_vars['item']->value['user']['id']){?>anonymous_user_link<?php }else{ ?>quick_view_item<?php }?>" href="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['user']['permalink'],$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['item']->value['user']['short_display_name'],$_smarty_tpl);?>
</a></td>
          <td class="job_type">
          <?php if ($_smarty_tpl->tpl_vars['item']->value['class']=='TimeRecord'){?>
            <?php echo clean($_smarty_tpl->tpl_vars['item']->value['job_type_name'],$_smarty_tpl);?>

          <?php }else{ ?>
          	<?php echo clean($_smarty_tpl->tpl_vars['item']->value['category_name'],$_smarty_tpl);?>

          <?php }?>
          </td>
          <td class="record_date"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['item']->value['record_date'],0),$_smarty_tpl);?>
</td>
          <td class="value right">
          <?php if ($_smarty_tpl->tpl_vars['item']->value['class']=='TimeRecord'){?>
            <?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['item']->value['value']),$_smarty_tpl);?>
h
          <?php }else{ ?>
            <?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['item']->value['value'],$_smarty_tpl->tpl_vars['item']->value['currency'],$_smarty_tpl->tpl_vars['logged_user_language']->value),$_smarty_tpl);?>

          <?php }?>
          </td>
          <td class="summary"><?php echo clean($_smarty_tpl->tpl_vars['item']->value['summary'],$_smarty_tpl);?>
</td>
          <td class="status">
            <?php if ($_smarty_tpl->tpl_vars['item']->value['billable_status']==@BILLABLE_STATUS_NOT_BILLABLE){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Not Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['item']->value['billable_status']==@BILLABLE_STATUS_BILLABLE){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['item']->value['billable_status']==@BILLABLE_STATUS_PENDING_PAYMENT){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Pending Payment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['item']->value['billable_status']==@BILLABLE_STATUS_PAID){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paid<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }else{ ?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unknown<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }?>
          </td>
          <td class="options">
          <?php if ($_smarty_tpl->tpl_vars['item']->value['permissions']['can_edit']){?>
            <a href="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['urls']['edit'],$_smarty_tpl);?>
" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Edit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" class="edit"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="" /></a>
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['item']->value['permissions']['can_trash']){?>
            <a href="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['urls']['trash'],$_smarty_tpl);?>
" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Move to Trash<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" class="trash"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/move-to-trash.png",'module'=>@SYSTEM_MODULE),$_smarty_tpl);?>
" alt="" /></a>
          <?php }?>
          </td>
        </tr>
      <?php } ?>
    <?php }?>
        <tr class="empty" style="display: none">
          <td colspan="7"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('type'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['logged_user']->value->getLanguage()))); $_block_repeat=true; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['logged_user']->value->getLanguage())), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There is no time logged for this :type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['logged_user']->value->getLanguage())), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
        </tr>
      </tbody>
    </table>
    </div>
<?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
  </form>
<?php }?>
</div><?php }} ?>