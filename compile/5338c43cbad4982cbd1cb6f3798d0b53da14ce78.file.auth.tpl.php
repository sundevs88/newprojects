<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 18:07:30
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/layouts/auth.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32145998156d9cee2db4be1-29980605%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5338c43cbad4982cbd1cb6f3798d0b53da14ce78' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/layouts/auth.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32145998156d9cee2db4be1-29980605',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wireframe' => 0,
    'content_for_layout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d9cee2dde039_97184749',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d9cee2dde039_97184749')) {function content_56d9cee2dde039_97184749($_smarty_tpl) {?><?php if (!is_callable('smarty_function_brand')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.brand.php';
if (!is_callable('smarty_function_touch')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.touch.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><!DOCTYPE html> 
<html> 
	<head>
		<?php if (AngieApplication::isInDevelopment()||AngieApplication::isInDebugMode()){?>
	    <meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="pragma" content="no-cache">
  	<?php }?>
		
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  	<title><?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getPageTitle(lang('Index')),$_smarty_tpl);?>
</title>
  	
  	<link rel="shortcut icon" href="<?php echo smarty_function_brand(array('what'=>'favicon'),$_smarty_tpl);?>
" type="image/x-icon" />
  	
  	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'144x144'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'114x114'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo smarty_function_touch(array('what'=>'icon','size'=>'72x72'),$_smarty_tpl);?>
" />
		<link rel="apple-touch-icon-precomposed" href="<?php echo smarty_function_touch(array('what'=>'icon'),$_smarty_tpl);?>
" />
  	
  	<link rel="apple-touch-startup-image" href="<?php echo smarty_function_touch(array('what'=>'startup'),$_smarty_tpl);?>
">
  	
  	<meta name="viewport" content="width=640, initial-scale=0.5, target-densitydpi=device-dpi" />
  	<meta name="apple-mobile-web-app-capable" content="yes" />
  	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
  	
  	<link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedStylesheetsUrl(AngieApplication::INTERFACE_PHONE,AngieApplication::CLIENT_IPHONE),$_smarty_tpl);?>
" type="text/css" media="screen" id="style_main_css"/>
    <script type="text/javascript" src="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedJavaScriptUrl(AngieApplication::INTERFACE_PHONE,AngieApplication::CLIENT_IPHONE),$_smarty_tpl);?>
"></script>
  </head>
  <body class="phone backend_login" onorientationchange="App.Wireframe.iPhoneOrientationChange.fix();">
    <div data-role="page" data-theme="f">
    	<div id="login_splash">
    		<img src="<?php echo smarty_function_touch(array('what'=>'login-logo'),$_smarty_tpl);?>
" alt="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logo<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
">
    	</div>
    	
    	<div data-role="content" class="wireframe_content top_shadow"><?php echo $_smarty_tpl->tpl_vars['content_for_layout']->value;?>
</div>
    </div>
  </body>
</html>

<script type="text/javascript">
	// prevents links from full-screen apps from opening in mobile safari
	(function(document,navigator,standalone) {
		if((standalone in navigator) && navigator[standalone]) {
			var curnode, location=document.location, stop=/^(a|html)$/i;
			document.addEventListener('click', function(e) {
				curnode=e.target;
				while(!(stop).test(curnode.nodeName)) {
					curnode=curnode.parentNode;
				} // while
				
				// Conditions to do this only on links to your own app. If you want all links, use if('href' in curnode) instead.
				if(
					'href' in curnode && 																					 // is a link
					(chref=curnode.href).replace(location.href,'').indexOf('#') && // is not an anchor
					(!(/^[a-z\+\.\-]+:/i).test(chref) ||                       		 // either does not have a proper scheme (relative links)
						chref.indexOf(location.protocol+'//'+location.host)===0) 		 // or is in the same protocol and domain
				) {
					e.preventDefault();
					location.href = curnode.href;
				} // if
			}, false);
		} // if
	})(document, window.navigator, 'standalone');
</script><?php }} ?>