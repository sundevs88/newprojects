<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 08:52:41
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mailboxes_admin/_mailbox_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36648188456d7fb59b4b233-19964053%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a2391ab54064b5cb26c576c0aae81029ad2fb4d' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_incoming_mailboxes_admin/_mailbox_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '36648188456d7fb59b4b233-19964053',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mailbox_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7fb59bd4792_93119907',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7fb59bd4792_93119907')) {function content_56d7fb59bd4792_93119907($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_email_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.email_field.php';
if (!is_callable('smarty_function_select_mailbox_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/function.select_mailbox_type.php';
if (!is_callable('smarty_function_port_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.port_field.php';
if (!is_callable('smarty_function_password_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.password_field.php';
if (!is_callable('smarty_function_select_mailbox_security')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/function.select_mailbox_security.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?>  <div class="content_stack_wrapper">
    <div class="content_stack_element odd">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Account<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
      	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'name')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'name'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_text_field(array('name'=>"mailbox[name]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['name'],'label'=>'Account Name','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'name'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'email','id'=>'incoming_mailbox_email')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'email','id'=>'incoming_mailbox_email'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_email_field(array('name'=>"mailbox[email]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['email'],'id'=>'fromEmail','label'=>'Email Address','required'=>true),$_smarty_tpl);?>

          <p class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<b>Warning:</b> Please <u>do not use a personal address</u>. Depending on server settings and connection type, system will probably delete all messages from this mailbox after it imports them. It is recommended that you use special addresses that are checked by the system only (ex. projects@company.com, support@company.com etc)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'email','id'=>'incoming_mailbox_email'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <!-- Connection -->
    <div class="content_stack_element even">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Connection<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'type')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_select_mailbox_type(array('name'=>"mailbox[server_type]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['server_type'],'id'=>'mailboxType','label'=>'Server Type','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'type'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'host')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'host'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_text_field(array('name'=>"mailbox[host]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['host'],'id'=>'hostName','label'=>'Server Address','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'host'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'port')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'port'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_port_field(array('name'=>"mailbox[port]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['port'],'id'=>'mailboxPort','label'=>'Port','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'port'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'username')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'username'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_text_field(array('name'=>"mailbox[username]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['username'],'id'=>'username','label'=>'Username','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'username'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'password')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'password'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_password_field(array('name'=>"mailbox[password]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['password'],'id'=>'password','label'=>'Password','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'password'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'security')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'security'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_select_mailbox_security(array('name'=>"mailbox[security]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['security'],'id'=>'mailboxSecurity','label'=>'Security','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'security'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'mailbox')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'mailbox'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_text_field(array('name'=>"mailbox[mailbox]",'value'=>$_smarty_tpl->tpl_vars['mailbox_data']->value['mailbox'],'id'=>'mailboxName','label'=>'Mailbox Name','required'=>true),$_smarty_tpl);?>

          <p class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This is mailbox name on your POP3/IMAP server. In most cases it should be left as default value ('INBOX') unless you want to check some other mailbox<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'mailbox'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <div id="test_connection">
          <button type="button"><span><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Test Connection<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></button>
          <span class="test_connection_results">
            <img src="<?php echo smarty_function_image_url(array('name'=>"layout/bits/indicator-pending.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt='' />
            <span></span>
          </span>
        </div>
      </div>
    </div>
  </div>
<?php }} ?>