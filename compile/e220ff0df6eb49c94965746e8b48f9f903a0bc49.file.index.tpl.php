<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 14:45:09
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoice_item_templates_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14387992485728b9758bb2c6-21653905%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e220ff0df6eb49c94965746e8b48f9f903a0bc49' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoice_item_templates_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14387992485728b9758bb2c6-21653905',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'request' => 0,
    'wireframe' => 0,
    'invoice_item_templates' => 0,
    'items_per_page' => 0,
    'total_items' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728b97591ef17_63886231',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728b97591ef17_63886231')) {function content_5728b97591ef17_63886231($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Item Templates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
View All<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<?php if ($_smarty_tpl->tpl_vars['request']->value->get('flyout')){?>
<script type="text/javascript">
  App.widgets.FlyoutDialog.front().addButton('new_invoice_item', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['wireframe']->value->actions->get('new_invoice_item'));?>
);
</script>
<?php }?>

<div id="invoice_items"></div>

<script type="text/javascript">
  var flyout_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['request']->value->get('flyout'));?>
;

  $('#invoice_items').pagedObjectsList({
    'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'admin_invoicing_items'),$_smarty_tpl);?>
', 
    'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_item_templates']->value);?>
,
    'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['items_per_page']->value,$_smarty_tpl);?>
, 
    'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_items']->value,$_smarty_tpl);?>
, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'item_templates' },
    'class' : 'admin_list',
    'columns' : {
      'description' : App.lang('Description'), 
      'first_tax_rate' : App.lang('First Tax Rate'),
      'second_tax_rate' : App.lang('Second Tax Rate'),
      'quantity' : App.lang('Quantity'),
      'unit_cost' : App.lang('Unit Cost'), 
      'options' : '' 
    }, 
    'sort_by' : 'description', 
    'empty_message' : App.lang('There are no invoice items defined'), 
    'listen' : 'invoice_item_template',
    'listen_scope' : flyout_id ? flyout_id : 'content',
    'on_add_item' : function(item) {
      var invoice_item_template = $(this);
      
      invoice_item_template.append(
        '<td class="description"></td>' +
       	'<td class="tax_rate first_tax_rate"></td>' +
       	'<td class="tax_rate second_tax_rate"></td>' +
       	'<td class="quantity"></td>' +
       	'<td class="unit_cost"></td>' + 
       	'<td class="options"></td>'
      );

      invoice_item_template.attr('id',item['id']);
      invoice_item_template.find('td.description').text(item['description']);

      if (item['first_tax_rate']) {
      	invoice_item_template.find('td.first_tax_rate').text(App.clean(item['first_tax_rate']['name']));
      } else {
        invoice_item_template.find('td.first_tax_rate').html('<i>' + App.lang('No Tax') + '</i>');
      } // if

      if (item['second_tax_rate']) {
        invoice_item_template.find('td.second_tax_rate').text(App.clean(item['second_tax_rate']['name']));
      } else {
        invoice_item_template.find('td.second_tax_rate').html('<i>' + App.lang('No Tax') + '</i>');
      } // if
      
      invoice_item_template.find('td.quantity').text(item['quantity']);
      invoice_item_template.find('td.unit_cost').text(item['unit_cost']);
      
      invoice_item_template.find('td.options')
        .append('<a href="' + item['urls']['edit'] + '" class="edit_note" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
        .append('<a href="' + item['urls']['delete'] + '" class="delete_note" title="' + App.lang('Remove Item') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
      ;

      invoice_item_template.find('td.options a.note_details').flyout();
      invoice_item_template.find('td.options a.edit_note').flyoutForm({
        'success_event' : 'invoice_item_template_updated',
        'width' : 480
      });
      invoice_item_template.find('td.options a.delete_note').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this item?'), 
        'success_event' : 'invoice_item_template_deleted', 
        'success_message' : App.lang('Item has been deleted successfully')
      });
    }
  });
</script><?php }} ?>