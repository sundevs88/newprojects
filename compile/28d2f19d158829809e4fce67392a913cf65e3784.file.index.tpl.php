<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:29:41
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/tax_rates_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2345883825742e9a5e37b95-91241814%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '28d2f19d158829809e4fce67392a913cf65e3784' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/tax_rates_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2345883825742e9a5e37b95-91241814',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'request' => 0,
    'wireframe' => 0,
    'tax_rates' => 0,
    'items_per_page' => 0,
    'total_items' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e9a5ebd768_63848900',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e9a5ebd768_63848900')) {function content_5742e9a5ebd768_63848900($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax Rates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
List All<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<?php if ($_smarty_tpl->tpl_vars['request']->value->get('flyout')){?>
  <script type="text/javascript">
    App.widgets.FlyoutDialog.front().addButton('new_tax_rate', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['wireframe']->value->actions->get('new_tax_rate'));?>
);
  </script>
<?php }?>

<div id="tax_rates"></div>

<script type="text/javascript">
  $('#tax_rates').each(function() {
    var tax_rates_widget = $(this);
    var flyout_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['request']->value->get('flyout'));?>
;

    tax_rates_widget.pagedObjectsList({
      'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'admin_tax_rates'),$_smarty_tpl);?>
',
      'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tax_rates']->value);?>
,
      'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['items_per_page']->value,$_smarty_tpl);?>
,
      'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_items']->value,$_smarty_tpl);?>
,
      'list_items_are' : 'tr',
      'list_item_attributes' : { 'class' : 'tax_rates' },
      'class' : 'admin_list',
      'columns' : {
        'default'     : '',
        'name'        : App.lang('Name'),
        'tax_rate'    : App.lang('Tax Rate (%)'),
        'options'     : ''
      },
      'sort_by' : 'name',
      'empty_message' : App.lang('There are no tax rates defined'),
      'listen' : 'tax_rate', // created, updated, deleted
      'listen_scope' : flyout_id ? flyout_id : 'content',
      'on_add_item' : function(item) {
        var tax_rate = $(this);

        tax_rate.append(
          '<td class="default_toggler"></td>',
          '<td class="name"></td>' +
            '<td class="percentage"></td>' +
            '<td class="options"></td>'
        );

        var toggler_cell = tax_rate.find('td.default_toggler');
        var checkbox_wrapper = $('<span class="checkbox_wrapper"></span>').appendTo(toggler_cell);
        var checkbox_label_set = $('<span class="checkbox_label checkbox_label_set">' + App.lang('Set as Default') + '</span>').appendTo(checkbox_wrapper);
        var checkbox_label_remove = $('<span class="checkbox_label checkbox_label_remove">' + App.lang('Remove Default') + '</span>').appendTo(checkbox_wrapper);
        var checkbox = $('<input type="checkbox" set_as_default_url="' + item['urls']['set_as_default'] + '" remove_default_url="' + item['urls']['remove_default'] + '" />').appendTo(checkbox_wrapper);

        if (item['is_default']) {
          tax_rate.addClass('default');
          checkbox.attr('checked', true);
        } // if

        tax_rate.attr('id',item['id']);
        tax_rate.find('td.name').text(App.clean(item['name']));
        tax_rate.find('td.percentage').text(App.numberFormat(item['percentage'], null, 3, true));

        if(item['permissions']['can_edit']) {
          tax_rate.find('td.options').append('<a href="' + item['urls']['edit'] + '" class="edit_tax_rate" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>');
          tax_rate.find('td.options a.edit_tax_rate').flyoutForm({
            'success_event' : 'tax_rate_updated',
            'width' : 500
          });
        } // if

        if(item['permissions']['can_delete']) {
          tax_rate.find('td.options').append('<a href="' + item['urls']['delete'] + '" class="delete_tax_rate" title="' + App.lang('Remove Item') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>');
          tax_rate.find('td.options a.delete_tax_rate').asyncLink({
            'confirmation' : App.lang('Are you sure that you want to permanently delete this item?'),
            'success_event' : 'tax_rate_deleted',
            'success_message' : App.lang('Tax rate has been deleted successfully'),
            'error' : function() {
              App.Wireframe.Flash.error(App.lang('Failed to delete selected item'));
            }
          });
        } // if
      }
    }).on('click', 'input[type="checkbox"]', function (event) {
      var checkbox = $(this).hide();
      var indicator = $('<img src="' + App.Wireframe.Utils.indicatorUrl('small') + '" />').insertAfter(checkbox);

      var ajax_url = checkbox.attr('set_as_default_url');
      if (!checkbox.is(':checked')) {
        ajax_url = checkbox.attr('remove_default_url');
      } // if

      $.ajax({
        'url' : ajax_url,
        'success' : function (response) {
          if (checkbox.is(':checked')) {
            tax_rates_widget.find('tr.default').each(function () {
              $(this).removeClass('default').find('input[type="checkbox"]').removeAttr('checked');
            });
          } // if

          App.Wireframe.Events.trigger('tax_rate_updated', [response]);
          indicator.remove();
          checkbox.show();
        },
        'error' : function (response) {
          indicator.remove();
          checkbox.show();
          if (checkbox.is(':checked')) {
            checkbox.removeAttr('checked');
          } else {
            checkbox.attr('checked', true);
          } // if
        }
      })
    });
  });
</script><?php }} ?>