<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 05:04:10
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_time_records/_time_record_thin_form_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:180225223556d7c5ca7a6f67-84378044%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f6b0af8903c2797cbad5ec667294eb513d2c84de' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_time_records/_time_record_thin_form_row.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '180225223556d7c5ca7a6f67-84378044',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_project_time_form_id' => 0,
    '_project_time_form_row_record' => 0,
    'can_track_for_others' => 0,
    'time_record_data' => 0,
    'active_project' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7c5ca8336d0_00783680',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7c5ca8336d0_00783680')) {function content_56d7c5ca8336d0_00783680($_smarty_tpl) {?><?php if (!is_callable('smarty_function_select_project_user')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project_user.php';
if (!is_callable('smarty_function_select_job_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_job_type.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_billable_status')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_billable_status.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><tr class="item_form time_record_form edit_time_record" id="<?php echo clean($_smarty_tpl->tpl_vars['_project_time_form_id']->value,$_smarty_tpl);?>
">
  <td colspan="8" style="padding: 0 !important;">
    <form action="<?php echo clean($_smarty_tpl->tpl_vars['_project_time_form_row_record']->value->getEditUrl(),$_smarty_tpl);?>
" method="post" class="time_record_form">
      <div class="item_attributes">
        <?php if ($_smarty_tpl->tpl_vars['can_track_for_others']->value){?>
          <div class="item_attribute time_record_user user">
            <?php echo smarty_function_select_project_user(array('name'=>'time[user_id]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['user_id'],'project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'optional'=>false,'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_user"),$_smarty_tpl);?>

          </div>
        <?php }else{ ?>
          <input type="hidden" name="time[user_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['time_record_data']->value['user_id'],$_smarty_tpl);?>
"/>
        <?php }?>

        <div class="item_attribute item_value_wrapper time_record_job_type job_type">
          <?php echo smarty_function_select_job_type(array('name'=>'time[job_type_id]','id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_job_type_id",'value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['job_type_id'],'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'required'=>true),$_smarty_tpl);?>

        </div>

        <div class="item_attribute time_record_date date">
          <?php echo smarty_function_select_date(array('name'=>'time[record_date]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['record_date'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_date"),$_smarty_tpl);?>

        </div>

        <div class="item_attribute item_value_wrapper time_record_value value">
          <?php echo smarty_function_text_field(array('name'=>'time[value]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['value'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_value"),$_smarty_tpl);?>

        </div>

        <div class="item_attribute item_summary_wrapper item_summary time_record_summary summary">
          <?php echo smarty_function_text_field(array('name'=>'time[summary]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['summary'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_summary"),$_smarty_tpl);?>

        </div>

        <div class="item_attribute time_record_billable billable">
          <?php echo smarty_function_select_billable_status(array('name'=>'time[billable_status]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['billable_status'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_billable"),$_smarty_tpl);?>

        </div>
      </div>

      <div class="item_form_buttons">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <a href="#" class="item_form_cancel"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a>
      </div>
    </form>
  </td>
</tr><?php }} ?>