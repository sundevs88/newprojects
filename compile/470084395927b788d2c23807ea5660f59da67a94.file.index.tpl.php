<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 18:08:52
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/backend/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:50675958056d9cf34bf8ce0-10860388%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '470084395927b788d2c23807ea5660f59da67a94' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/backend/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '50675958056d9cf34bf8ce0-10860388',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'homescreen_items' => 0,
    'homescreen_item' => 0,
    'iteration' => 0,
    'quick_tracking_data' => 0,
    'logout_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d9cf34c541c7_62411382',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d9cf34c541c7_62411382')) {function content_56d9cf34c541c7_62411382($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_assign_var')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.assign_var.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Welcome<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Home Screen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div class="homescreen">
	<div id="global_search">
		<form action="<?php echo clean(Router::assemble('quick_backend_search'),$_smarty_tpl);?>
" method="post" class="quick_search">
			<input type="search" placeholder="Search" name="q" />
		</form>
	</div>
	
	<div id="homescreen_items" class="ui-grid-b">
		<?php if (is_foreachable($_smarty_tpl->tpl_vars['homescreen_items']->value)){?>
			<?php  $_smarty_tpl->tpl_vars['homescreen_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['homescreen_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['homescreen_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['homescreen_item']->key => $_smarty_tpl->tpl_vars['homescreen_item']->value){
$_smarty_tpl->tpl_vars['homescreen_item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item']['iteration']++;
?>
				<?php $_smarty_tpl->smarty->_tag_stack[] = array('assign_var', array('name'=>'iteration')); $_block_repeat=true; echo smarty_block_assign_var(array('name'=>'iteration'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean($_smarty_tpl->getVariable('smarty')->value['foreach']['item']['iteration'],$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_assign_var(array('name'=>'iteration'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			  <a href="<?php echo clean($_smarty_tpl->tpl_vars['homescreen_item']->value['url'],$_smarty_tpl);?>
" class="ui-block-<?php if ($_smarty_tpl->tpl_vars['iteration']->value%3==1){?>a<?php }elseif($_smarty_tpl->tpl_vars['iteration']->value%3==2){?>b<?php }elseif($_smarty_tpl->tpl_vars['iteration']->value%3==0){?>c<?php }?>"><img src="<?php echo clean($_smarty_tpl->tpl_vars['homescreen_item']->value['icon'],$_smarty_tpl);?>
" alt="" /><span><?php echo clean($_smarty_tpl->tpl_vars['homescreen_item']->value['text'],$_smarty_tpl);?>
</span></a>
			<?php } ?>
		<?php }?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.homescreen').closest('div[data-role="page"]').css('background-color', '#E8E8E8');
		
		<?php if (AngieApplication::isModuleLoaded('tracking')){?>
	    App.Wireframe.QuickTracking.init('ui-navbar', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['quick_tracking_data']->value);?>
);
	  <?php }?>
	  
	  App.Wireframe.Logout.init(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['logout_url']->value);?>
);
	});
</script><?php }} ?>