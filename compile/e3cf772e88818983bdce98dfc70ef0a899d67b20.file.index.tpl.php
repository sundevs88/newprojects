<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:20:39
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/job_types_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8626858575742e78799f0b8-83923809%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3cf772e88818983bdce98dfc70ef0a899d67b20' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/job_types_admin/index.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8626858575742e78799f0b8-83923809',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'request' => 0,
    'wireframe' => 0,
    'job_types' => 0,
    'job_types_per_page' => 0,
    'total_job_types' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e787a615b2_56864396',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e787a615b2_56864396')) {function content_5742e787a615b2_56864396($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Job Types & Hourly Rates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<?php if ($_smarty_tpl->tpl_vars['request']->value->get('flyout')){?>
  <script type="text/javascript">
    App.widgets.FlyoutDialog.front().addButton('add_job_type_form', <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['wireframe']->value->actions->get('add_job_type_form'));?>
);
  </script>
<?php }?>

<div id="job_types"></div>

<script type="text/javascript">
  var flyout_id = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['request']->value->get('flyout'));?>
;

  $('#job_types').pagedObjectsList({
    'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'job_types_admin'),$_smarty_tpl);?>
', 
    'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['job_types']->value);?>
,
    'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['job_types_per_page']->value,$_smarty_tpl);?>
, 
    'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_job_types']->value,$_smarty_tpl);?>
, 
    'list_items_are' : 'tr', 
    'list_item_attributes' : { 'class' : 'job_type' },
    'class' : 'admin_list',
    'columns' : {
      'is_default' : '', 
      'name' : App.lang('Job Type'), 
      'default_hourly_rate' : App.lang('Default Hourly Rate'), 
      'options' : '' 
    },
    'empty_message' : App.lang('There are no job types defined'),
    'listen' : {
      'create' : 'job_type_created',
      'update' : 'job_type_updated job_type_archived job_type_unarchived',
      'delete' : 'job_type_deleted'
    },
    'listen_scope' : flyout_id ? flyout_id : 'content',
    'on_add_item' : function(item) {
      var job_type = $(this);
      
      job_type.append(
       	'<td class="is_default"></td>' + 
        '<td class="name"></td>' + 
        '<td class="default_hourly_rate"></td>' + 
        '<td class="options"></td>'
      );

      var radio = $('<input name="set_default_job_type" type="radio" value="' + item['id'] + '" />');

      if(item['is_active']) {
        radio.click(function() {
          if(!job_type.is('tr.is_default')) {
            if(confirm(App.lang('Are you sure that you want to set this job type as default job type?'))) {
              var cell = radio.parent();

              $('#job_types td.is_default input[type=radio]').hide();

              cell.append('<img src="' + App.Wireframe.Utils.indicatorUrl() + '">');

              $.ajax({
                'url' : item['urls']['set_as_default'],
                'type' : 'post',
                'data' : { 'submitted' : 'submitted' },
                'success' : function(response) {
                  cell.find('img').remove();
                  radio[0].checked = true;

                  $('#job_types td.is_default input[type=radio]').show();
                  $('#job_types tr.is_default').find('td.options a.delete_job_type').show();
                  $('#job_types tr.is_default').removeClass('is_default');

                  job_type.addClass('is_default').highlightFade();
                  job_type.find('td.options a.delete_job_type').hide();

                  App.Wireframe.Flash.success('Default job type has been changed successfully');
                },
                'error' : function(response) {
                  cell.find('img').remove();
                  $('#job_types td.is_default input[type=radio]').show();

                  App.Wireframe.Flash.error('Failed to set selected job type as default');
                }
              });
            } // if
          } // if

          return false;
        }).appendTo(job_type.find('td.is_default'));
      } // if

      if(!item['is_active']) {
        job_type.addClass('archived');
      } // if

      if(item['is_default']) {
        job_type.addClass('is_default');
        radio[0].checked = true;
      } // if
      
      job_type.find('td.name').html('<a href="' + item['urls']['view'] + '">' + App.clean(item['name']) + '</a>');
      job_type.find('td.default_hourly_rate').html(App.numberFormat(item['default_hourly_rate']));

      var options_cell = job_type.find('td.options');

      // Edit
      options_cell.append('<a href="' + item['urls']['edit'] + '" class="edit_job_type" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Edit') + '" /></a>').find('a.edit_job_type').flyoutForm({
        'success_event' : 'job_type_updated',
        'width' : 550
      });

      // Archiving
      if(item['is_active']) {
        options_cell.append('<a href="' + item['urls']['archive'] + '" class="archive_job_type" title="' + App.lang('Archive Job Type') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/archive.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Archive') + '" /></a>').find('a.archive_job_type').flyoutForm({
          'success_event' : 'job_type_archived',
          'success_message' : App.lang('Job type has been archived successfully'),
          'width' : 400
        });
      } else {
        options_cell.append('<a href="' + item['urls']['unarchive'] + '" class="unarchive_job_type" title="' + App.lang('Unarchive Job Type') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/unarchive.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Unarchive') + '" /></a>').find('a.unarchive_job_type').asyncLink({
          'confirmation' : App.lang('Are you sure that you want to unarchive this job type?'),
          'success_event' : 'job_type_unarchived',
          'success_message' : App.lang('Job type has been unarchived successfully')
        });
      } // if

      // Delete
      if(item['permissions']['can_delete']) {
        options_cell.append('<a href="' + item['urls']['delete'] + '" class="delete_job_type" title="' + App.lang('Remove Job Type') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Delete') + '" /></a>').find('a.delete_job_type').asyncLink({
          'confirmation' : App.lang('Are you sure that you want to permanently delete this job type?'),
          'success_event' : 'job_type_deleted', 
          'success_message' : App.lang('Job type has been deleted successfully')
        });
      } // if
    }
  });
</script><?php }} ?>