<?php /* Smarty version Smarty-3.1.12, created on 2016-03-07 03:58:25
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/source/views/default/source_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:179184825556dcfc61496721-53342119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a3dcd8d1ccd7104f335133b543e0213bef4daa17' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/source/views/default/source_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '179184825556dcfc61496721-53342119',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'source_data' => 0,
    'repositories' => 0,
    'repositories_per_page' => 0,
    'total_repositories' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56dcfc615a9346_35132207',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56dcfc615a9346_35132207')) {function content_56dcfc615a9346_35132207($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.link.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Source Repositories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Control Panel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="source_admin" class="wireframe_content_wrapper settings_panel">
  <div class="settings_panel_header">
    <table class="settings_panel_header_cell_wrapper two_cells">
      <tr>
        <td class="settings_panel_header_cell">
			    <h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subversion<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2>
			  	<div class="properties">
			  	  <div class="property" id="svn_engine">
			  	    <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Engine<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			        <div class="data"><?php echo clean(RepositoryEngine::getName(),$_smarty_tpl);?>
</div>
			  	  </div>
			  	  <div class="property" id="svn_exec_path">
			        <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Executable Path<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			        <div class="data"><?php echo clean($_smarty_tpl->tpl_vars['source_data']->value['svn_path'],$_smarty_tpl);?>
</div>
			  	  </div>
			  	  <div class="property" id="svn_config_path">
			  	    <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Config Path<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			        <div class="data">
              <?php if ($_smarty_tpl->tpl_vars['source_data']->value['svn_config_dir']){?>
                <?php echo clean($_smarty_tpl->tpl_vars['source_data']->value['svn_config_dir'],$_smarty_tpl);?>

              <?php }else{ ?>
                <span class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Default<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
              <?php }?>
			        </div>
			  	  </div>
            <div class="property" id="svn_trust_server_cert">
              <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Trust Certificate<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
              <div class="data">
                <?php if ($_smarty_tpl->tpl_vars['source_data']->value['svn_trust_server_cert']){?>
                  <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Yes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <?php }else{ ?>
                  <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <?php }?>
              </div>
            </div>
			    </div>
          <ul class="settings_panel_header_cell_actions">
            <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('admin_source_svn_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Subversion Settings','success_event'=>'svn_updated','class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('admin_source_svn_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Subversion Settings','success_event'=>'svn_updated','class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Subversion Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('admin_source_svn_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Subversion Settings','success_event'=>'svn_updated','class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
          </ul>
        </td>
        <td class="settings_panel_header_cell">
          <h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mercurial<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2>
          <div class="properties">
			      <div class="property" id="mercurial_exec_path">
			        <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mercurial Engine<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			        <div class="data"><?php echo clean($_smarty_tpl->tpl_vars['source_data']->value['mercurial_path'],$_smarty_tpl);?>
</div>
			      </div>
          </div>
          <ul class="settings_panel_header_cell_actions">
		  	   <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('admin_source_mercurial_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Mercurial Settings','success_event'=>'mercurial_updated','class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('admin_source_mercurial_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Mercurial Settings','success_event'=>'mercurial_updated','class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Mercurial Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('admin_source_mercurial_settings'),'async'=>true,'mode'=>'flyout_form','title'=>'Mercurial Settings','success_event'=>'mercurial_updated','class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
          </ul>
        </td>
      </tr>
    </table>
  </div>
  
  <div class="settings_panel_body" id="repositories"></div>
</div>

<script type="text/javascript">

	var wrapper = $('#source_admin');
	var min_height = 0;
	wrapper.find('td.settings_panel_header_cell div.properties').each(function () { min_height = Math.max(min_height, $(this).height()); });
	wrapper.find('td.settings_panel_header_cell div.properties').css('min-height' , min_height + 'px');
	
	App.Wireframe.Events.bind('svn_updated.content', function(event, svn_source_data) {
	  if (svn_source_data['svn_type'] == "exec") {
	    $("#svn_engine .data").html(App.lang("Executable"));
	    $("#svn_exec_path .data").html(App.lang(svn_source_data["svn_path"]));
	    $("#svn_config_path .data").html(App.lang(svn_source_data["svn_config_dir"]));
      $("#svn_trust_server_cert .data").html(svn_source_data["svn_trust_server_cert"] == "1" ? App.lang("Yes") : App.lang("No"));
	  } else if (svn_source_data['svn_type'] == "extension") {
	    $("#svn_engine .data").html(App.lang("PHP Extension"));
	    $("#svn_exec_path .data").html("-");
	    $("#svn_config_path .data").html("-");
      $("#svn_trust_server_cert .data").html("-");
	  }
	});
	
	App.Wireframe.Events.bind('mercurial_updated.content', function(event, mercurial_source_data) {
	  $("#mercurial_exec_path .data").html(App.lang(mercurial_source_data["mercurial_path"]));
	});

	$('#repositories').pagedObjectsList({
	  'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'admin_source'),$_smarty_tpl);?>
',
	  'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['repositories']->value);?>
,
	  'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['repositories_per_page']->value,$_smarty_tpl);?>
, 
	  'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_repositories']->value,$_smarty_tpl);?>
, 
	 
	  'list_items_are' : 'tr', 
	  'list_item_attributes' : { 'class' : 'repository' }, 
	  'columns' : {
	    'name' : App.lang('Repository Name'), 
	    'type' : App.lang('Repository Type'), 
	    'usage' : App.lang('Project Usage'), 
	    'options' : '' 
	  },
	  'empty_message' : App.lang('There are no repositories'),
	  'listen' : 'repository', 
	  'on_add_item' : function(item) {
	    var repository = $(this);
	    repository.append('<td class="name">' + 
	      '<td class="type"></td>' + 
	      '<td class="usage"></td>' + 
	      '<td class="options"></td>'
	    );
	    
	    repository.find('td.name').text(App.clean(item['name']));
	    repository.find('td.type').text(App.clean(item['type']));
	    
	      $('<a></a>').attr({'href' : item['urls']['usage'] , 'title' : item['name'] + ' repository usage'}).text(item['project_count']).appendTo(repository.find('td.usage')).flyout();
	    
	    repository.find('td.options')
	      .append('<a href="' + item['urls']['edit'] + '" class="edit_repository" title="' + App.lang('Edit Repository') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
	      .append('<a href="' + item['urls']['delete'] + '" class="delete_repository" title="' + App.lang('Delete Repository') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" /></a>')
	    ;
	    repository.find('td.options a.edit_repository').flyoutForm({
	      'success_event' : 'repository_updated'
	    });
      
	    repository.find('td.options a.delete_repository').asyncLink({
        'confirmation' : App.lang('Are you sure that you want to permanently delete this repository? It will also be removed from all the projects.'),
        'success_event' : 'repository_deleted',
        'success_message' : App.lang('Repository has been deleted successfully')
	    });
	  }
	});
</script><?php }} ?>