<?php /* Smarty version Smarty-3.1.12, created on 2016-03-26 07:14:25
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/complete/notifications/email/object_reopened.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167898200556f636d1a93609-47632537%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '995774ee2be0bdb022e2e78fd7f7cbfa653e90cb' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/complete/notifications/email/object_reopened.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167898200556f636d1a93609-47632537',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'context' => 0,
    'language' => 0,
    'context_view_url' => 0,
    'recipient' => 0,
    'sender' => 0,
    'style' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56f636d1b02b85_93230519',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f636d1b02b85_93230519')) {function content_56f636d1b02b85_93230519($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_excerpt')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.excerpt.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_notification_wrapper')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrapper.php';
if (!is_callable('smarty_block_notification_wrap_body')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/helpers/block.notification_wrap_body.php';
?><?php echo clean($_smarty_tpl->tpl_vars['context']->value->complete()->getNotificationSubjectPrefix(),$_smarty_tpl);?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
':name' :type Reopened<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(false,$_smarty_tpl->tpl_vars['language']->value),'name'=>smarty_modifier_excerpt($_smarty_tpl->tpl_vars['context']->value->getName()),'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

================================================================================
<?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrapper', array('title'=>':type Reopened','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value)); $_block_repeat=true; echo smarty_block_notification_wrapper(array('title'=>':type Reopened','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('reopened_by'=>$_smarty_tpl->tpl_vars['sender']->value->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context']->value->getViewUrl(),'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['language']->value),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value)); $_block_repeat=true; echo smarty_block_lang(array('reopened_by'=>$_smarty_tpl->tpl_vars['sender']->value->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context']->value->getViewUrl(),'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['language']->value),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:reopened_by has just reopened "<a href=":url" style=":link_style" target="_blank">:name</a>" :type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('reopened_by'=>$_smarty_tpl->tpl_vars['sender']->value->getDisplayName(),'url'=>$_smarty_tpl->tpl_vars['context']->value->getViewUrl(),'name'=>$_smarty_tpl->tpl_vars['context']->value->getName(),'type'=>$_smarty_tpl->tpl_vars['context']->value->getVerboseType(true,$_smarty_tpl->tpl_vars['language']->value),'link_style'=>$_smarty_tpl->tpl_vars['style']->value['link'],'language'=>$_smarty_tpl->tpl_vars['language']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('notification_wrap_body', array()); $_block_repeat=true; echo smarty_block_notification_wrap_body(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['context']->value->getBody();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrap_body(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_notification_wrapper(array('title'=>':type Reopened','context'=>$_smarty_tpl->tpl_vars['context']->value,'context_view_url'=>$_smarty_tpl->tpl_vars['context_view_url']->value,'recipient'=>$_smarty_tpl->tpl_vars['recipient']->value,'sender'=>$_smarty_tpl->tpl_vars['sender']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>