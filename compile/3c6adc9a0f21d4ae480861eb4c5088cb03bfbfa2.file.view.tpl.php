<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 08:36:12
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/views/default/fw_roles_admin/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:203442924556da9a7c3b89e2-23033527%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c6adc9a0f21d4ae480861eb4c5088cb03bfbfa2' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/views/default/fw_roles_admin/view.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203442924556da9a7c3b89e2-23033527',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'users' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56da9a7c410630_25636474',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56da9a7c410630_25636474')) {function content_56da9a7c410630_25636474($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
?><div id="user_role_users">
<?php if ($_smarty_tpl->tpl_vars['users']->value){?>
  <div id="user_role_users_listing">
    <table class="common" cellspacing="0">
      <tr>
        <th class="icon"></th>
        <th class="name"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
        <th></th>
      </tr>
      <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
      <tr class="<?php if ($_smarty_tpl->tpl_vars['user']->value->getState()==@STATE_ARCHIVED){?>archived_user<?php }else{ ?>visible_user<?php }?>">
        <td class="icon"><img src="<?php echo clean($_smarty_tpl->tpl_vars['user']->value->avatar()->getUrl(@IUserAvatarImplementation::SIZE_SMALL),$_smarty_tpl);?>
"></td>
        <td class="name"><a href="<?php echo clean($_smarty_tpl->tpl_vars['user']->value->getViewUrl(),$_smarty_tpl);?>
" class="quick_view_item"><?php echo clean($_smarty_tpl->tpl_vars['user']->value->getDisplayName(),$_smarty_tpl);?>
</a></td>
        <td class="right">
          <?php if ($_smarty_tpl->tpl_vars['user']->value->getState()==@STATE_ARCHIVED){?>
            <span class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Archived<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
          <?php }?>
        </td>
      </tr>
      <?php } ?>
    </table>
  </div>
  <p style="text-align: center"><?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('id'=>'show_hide_archived_users')); $_block_repeat=true; echo smarty_block_button(array('id'=>'show_hide_archived_users'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hide Archived Users<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('id'=>'show_hide_archived_users'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
<?php }else{ ?>
  <p class="empty_page"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Empty<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
<?php }?>
</div>

<script type="text/javascript">
  $('#user_role_users').each(function() {
    var wrapper = $(this);

    wrapper.find('#show_hide_archived_users').click(function() {
      var button = $(this);
      var user_listing = wrapper.find('#user_role_users_listing');

      if(user_listing.is('.hide_archived_users')) {
        user_listing.removeClass('hide_archived_users');
        button.text(App.lang('Hide Archived Users'));
      } else {
        user_listing.addClass('hide_archived_users');
        button.text(App.lang('Show Archived Users'));
      } // if
    });
  });
</script><?php }} ?>