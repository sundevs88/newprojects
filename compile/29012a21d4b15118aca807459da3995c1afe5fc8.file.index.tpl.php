<?php /* Smarty version Smarty-3.1.12, created on 2016-06-07 14:29:13
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/printer/invoices/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11968419995756da3948be71-32845491%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29012a21d4b15118aca807459da3995c1afe5fc8' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/printer/invoices/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11968419995756da3948be71-32845491',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_title' => 0,
    'invoices' => 0,
    'group_by' => 0,
    'map_name' => 0,
    'object_list' => 0,
    'object' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5756da394f8644_62978884',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5756da394f8644_62978884')) {function content_5756da394f8644_62978884($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean($_smarty_tpl->tpl_vars['page_title']->value,$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php if (is_foreachable($_smarty_tpl->tpl_vars['invoices']->value)){?>
	
	<?php  $_smarty_tpl->tpl_vars['object_list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['object_list']->_loop = false;
 $_smarty_tpl->tpl_vars['map_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['invoices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['object_list']->key => $_smarty_tpl->tpl_vars['object_list']->value){
$_smarty_tpl->tpl_vars['object_list']->_loop = true;
 $_smarty_tpl->tpl_vars['map_name']->value = $_smarty_tpl->tpl_vars['object_list']->key;
?>
		<table class="invoice_table common" cellspacing="0">
      <thead>
        <tr>
          <th colspan="<?php if ($_smarty_tpl->tpl_vars['group_by']->value=='status'||$_smarty_tpl->tpl_vars['group_by']->value=='client_id'){?>3<?php }else{ ?>4<?php }?>"><?php echo clean($_smarty_tpl->tpl_vars['map_name']->value,$_smarty_tpl);?>
</th>
        </tr>
      </thead>
      <tbody>
		  <?php  $_smarty_tpl->tpl_vars['object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['object']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['object_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['object']->key => $_smarty_tpl->tpl_vars['object']->value){
$_smarty_tpl->tpl_vars['object']->_loop = true;
?>
		  <tr>
		    <td class="name"><?php echo clean($_smarty_tpl->tpl_vars['object']->value->getName(),$_smarty_tpl);?>
</td>
        <?php if ($_smarty_tpl->tpl_vars['group_by']->value!='status'){?>
		    <td class="status" style="width: 20%"><?php echo clean($_smarty_tpl->tpl_vars['object']->value->getVerboseStatus(),$_smarty_tpl);?>
</td>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['group_by']->value!='client_id'){?>
        <td class="client" style="width: 20%">
        <?php if ($_smarty_tpl->tpl_vars['object']->value->getCompany() instanceof Company){?>
          <?php echo clean($_smarty_tpl->tpl_vars['object']->value->getCompany()->getName(),$_smarty_tpl);?>

        <?php }?>
        </td>
        <?php }?>
		    <td class="total right" style="width: 20%"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['object']->value->getTotal(),$_smarty_tpl->tpl_vars['object']->value->getCurrency(),null,true,true),$_smarty_tpl);?>
</td>
		  </tr>
		  <?php } ?>
		 </tbody>
	  </table>
	<?php } ?>
<?php }else{ ?>
	<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no Invoices that match this criteria<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>	
<?php }?><?php }} ?>