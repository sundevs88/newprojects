<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:18:32
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13260547065742e708c3b679-15090185%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5bbabc220f534a35fb8119f12e8a95b88921c656' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/edit.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13260547065742e708c3b679-15090185',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_invoice' => 0,
    'invoice_data' => 0,
    'js_invoice_notes' => 0,
    'js_original_note' => 0,
    'js_invoice_item_templates' => 0,
    'tax_rates' => 0,
    'js_company_details_url' => 0,
    'js_company_projects_url' => 0,
    'js_delete_icon_url' => 0,
    'js_move_icon_url' => 0,
    'default_tax_rate' => 0,
    'js_second_tax_is_enabled' => 0,
    'js_second_tax_is_compound' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e708cae537_65130766',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e708cae537_65130766')) {function content_5742e708cae537_65130766($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Update Invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Update<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"invoice_form",'module'=>"invoicing"),$_smarty_tpl);?>


<div id="edit_invoice">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getEditUrl(),'method'=>'post','id'=>'edit_invoice_form','class'=>"big_form")); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getEditUrl(),'method'=>'post','id'=>'edit_invoice_form','class'=>"big_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php echo $_smarty_tpl->getSubTemplate (get_view_path('_invoice_form','invoices','invoicing'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
  

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getEditUrl(),'method'=>'post','id'=>'edit_invoice_form','class'=>"big_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>
<script type="text/javascript">
	$('#edit_invoice_form').invoiceForm({
    'mode'                  : 'edit',
    'items'                   : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['invoice_data']->value['items']);?>
,
	  'notes'                   : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_invoice_notes']->value);?>
,
	  'initial_note'            : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_original_note']->value);?>
,
	  'item_templates'          : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_invoice_item_templates']->value);?>
,
	  'tax_rates'               : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tax_rates']->value);?>
,
	  'company_details_url'     : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_company_details_url']->value);?>
,
    'company_projects_url'    : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_company_projects_url']->value);?>
,
	  'delete_icon_url'         : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_delete_icon_url']->value);?>
,
	  'move_icon_url'           : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_move_icon_url']->value);?>
,
    'default_tax_rate'        : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['default_tax_rate']->value);?>
,
    'currencies'              : <?php echo smarty_modifier_json(Currencies::getIdDetailsMap());?>
,
    'second_tax_is_enabled'   : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_second_tax_is_enabled']->value);?>
,
    'second_tax_is_compound'  : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['js_second_tax_is_compound']->value);?>

	});
</script>
<?php }} ?>