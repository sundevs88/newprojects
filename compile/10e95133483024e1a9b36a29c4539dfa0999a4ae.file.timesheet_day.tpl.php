<?php /* Smarty version Smarty-3.1.12, created on 2016-04-29 08:14:18
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_tracking/timesheet_day.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1245104104572317da436929-01921703%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10e95133483024e1a9b36a29c4539dfa0999a4ae' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_tracking/timesheet_day.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1245104104572317da436929-01921703',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_day' => 0,
    'active_user' => 0,
    'can_add' => 0,
    'active_project' => 0,
    'logged_user' => 0,
    'default_billable_status' => 0,
    'records' => 0,
    'record' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_572317da505e26_23857107',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572317da505e26_23857107')) {function content_572317da505e26_23857107($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_function_user_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/helpers/function.user_link.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_select_job_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_job_type.php';
if (!is_callable('smarty_function_object_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.object_link.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array('lang'=>false)); $_block_repeat=true; echo smarty_block_add_bread_crumb(array('lang'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_day']->value),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array('lang'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="timesheet_day" user_id="<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->getId(),$_smarty_tpl);?>
" day="<?php echo clean($_smarty_tpl->tpl_vars['active_day']->value->toMySQL(),$_smarty_tpl);?>
">
  <div id="timesheet_day_info">
    <div class="user_avatar">
      <img src="<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->avatar()->getUrl(IUserAvatarImplementation::SIZE_BIG),$_smarty_tpl);?>
" alt="" />
    </div>
    <div class="user_day_details">
      <div class="user"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['active_user']->value),$_smarty_tpl);?>
</div>
      <div class="day"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time for<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['active_day']->value,0),$_smarty_tpl);?>
</div>
    </div>
  </div>
  
  <div class="fields_wrapper">
	  <?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
	  <form action="<?php echo clean($_smarty_tpl->tpl_vars['active_project']->value->tracking()->getAddTimeUrl(),$_smarty_tpl);?>
" method="post" id="time_day_log_add_form">
	  <?php }?>
	  
	  <table id="timesheet_day_log" class="common" cellspacing=0>
	    <thead>
	      <tr>
	        <th><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Added On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	        <th class="right"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hours<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	        <th><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Parent<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 / <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Summary<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	        <th><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	        <th></th>
	      </tr>
	    </thead>
	    <tbody>
	    <?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
	      <tr id="time_day_log_add">
	        <td class="created_on"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</td>
	        <td class="value right"><input type="text" name="time_record[value]"> <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
of<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_job_type(array('name'=>'time_record[job_type_id]','user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</td>
	        <td class="summary"><input type="text" name="time_record[summary]"></td>
	        <td class="status">
	          <select name="time_record[billable_status]">
	            <option value="0" <?php if (empty($_smarty_tpl->tpl_vars['default_billable_status']->value)){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Not Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
	            <option value="1" <?php if ($_smarty_tpl->tpl_vars['default_billable_status']->value){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
	          </select>
	        </td>
	        <td class="options"><button type="submit" class="default"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</button></td>
	      </tr>
	    <?php }?>
	    
	  <?php if (is_foreachable($_smarty_tpl->tpl_vars['records']->value)){?>
	    <?php  $_smarty_tpl->tpl_vars['record'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['record']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['records']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['record']->key => $_smarty_tpl->tpl_vars['record']->value){
$_smarty_tpl->tpl_vars['record']->_loop = true;
?>
	      <tr class="timesheet_day_log_entry">
	        <td class="created_on"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['record']->value->getCreatedOn(),0),$_smarty_tpl);?>
</td>
	        <td class="value right"><?php echo clean($_smarty_tpl->tpl_vars['record']->value->getValue(),$_smarty_tpl);?>
</td>
	        <td class="summary">
	        <?php if ($_smarty_tpl->tpl_vars['record']->value->getParent() instanceof ProjectObject){?><?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['record']->value->getParent()),$_smarty_tpl);?>
 - <?php }?><?php echo clean($_smarty_tpl->tpl_vars['record']->value->getSummary(),$_smarty_tpl);?>

	        </td>
	        <td class="status"><?php echo clean($_smarty_tpl->tpl_vars['record']->value->getBillableVerboseStatus(),$_smarty_tpl);?>
</td>
	        <td class="options">
	        <?php if ($_smarty_tpl->tpl_vars['record']->value->state()->canTrash($_smarty_tpl->tpl_vars['logged_user']->value)){?>
	          <a href="<?php echo clean($_smarty_tpl->tpl_vars['record']->value->state()->getTrashUrl(),$_smarty_tpl);?>
" title="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Move to Trash<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
" class="trash"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/move-to-trash.png",'module'=>@SYSTEM_MODULE),$_smarty_tpl);?>
" alt="" /></a>
	        <?php }?>
	        </td>
	      </tr>
	    <?php } ?>
	  <?php }?>
	  
	      <tr id="timesheet_day_log_empty" style="display: none">
	        <td colspan="5"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There is no time logged for this day<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
	      </tr>
	    </tbody>
	    <tfoot>
	      <tr>
	        <td class="value right" colspan="2"></td>
	        <td colspan="3"></td>
	      </tr>
	    </tfoot>
	  </table>
	  
	  <?php if ($_smarty_tpl->tpl_vars['can_add']->value){?>
	  </form>
	  <?php }?>
  </div>
</div><?php }} ?>