<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:23:46
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13903010205742e842614d58-47251238%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '97119ed00dd41167118c0c468b67be51b3f1d8be' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13903010205742e842614d58-47251238',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sample_url' => 0,
    'active_template' => 0,
    'sample_invoice' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e8426743f0_50603243',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e8426743f0_50603243')) {function content_5742e8426743f0_50603243($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice Designer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Modify<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"invoice_designer",'module'=>"invoicing"),$_smarty_tpl);?>


<div id="invoice_designer">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array()); $_block_repeat=true; echo smarty_block_form(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<div class="invoice_designer_paper"> 
		</div>
	  
    <div class="invoice_designer_buttons">
      <ul>
        <li><a href="<?php echo smarty_function_assemble(array('route'=>'admin_invoicing_pdf_paper'),$_smarty_tpl);?>
" class="invoice_paper_size" flyout_width="narrow"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paper Size and Background<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li>
        <li><a href="<?php echo smarty_function_assemble(array('route'=>'admin_invoicing_pdf_header'),$_smarty_tpl);?>
" class="invoice_header"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Header Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li>
        <li><a href="<?php echo smarty_function_assemble(array('route'=>'admin_invoicing_pdf_body'),$_smarty_tpl);?>
" class="invoice_body"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Body Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li>
        <li><a href="<?php echo smarty_function_assemble(array('route'=>'admin_invoicing_pdf_footer'),$_smarty_tpl);?>
" class="invoice_footer"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Footer Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></li>
      </ul>      
    </div>
    
    <div class="disclamer">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('url'=>$_smarty_tpl->tpl_vars['sample_url']->value)); $_block_repeat=true; echo smarty_block_lang(array('url'=>$_smarty_tpl->tpl_vars['sample_url']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Picture on the left is only the draft preview. Font will not be applied to it due to technical limitations.<br /><br /> Download the <a href=":url" target="_blank">sample invoice</a> to see the final invoice design.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('url'=>$_smarty_tpl->tpl_vars['sample_url']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#invoice_designer').invoiceDesigner({
    'template' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_template']->value);?>
,
    'invoice' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['sample_invoice']->value);?>

  });
</script><?php }} ?>