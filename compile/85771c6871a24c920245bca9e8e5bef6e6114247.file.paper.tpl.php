<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:24:06
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/paper.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20171673555742e85692cad6-50581273%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85771c6871a24c920245bca9e8e5bef6e6114247' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/paper.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20171673555742e85692cad6-50581273',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_url' => 0,
    'template_data' => 0,
    'remove_background_image_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e856999063_45109388',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e856999063_45109388')) {function content_5742e856999063_45109388($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_paper_format')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_paper_format.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_file_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.file_field.php';
if (!is_callable('smarty_function_link_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.link_button.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_paper_form",'enctype'=>"multipart/form-data")); $_block_repeat=true; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_paper_form",'enctype'=>"multipart/form-data"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="fields_wrapper">
		<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'paper_format')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'paper_format'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

			<?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'paper_format','required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>'paper_format','required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paper Size:<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'paper_format','required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php echo smarty_function_select_paper_format(array('name'=>"template[paper_size]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['paper_size'],'id'=>"paper_format"),$_smarty_tpl);?>

		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'paper_format'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'background_image')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'background_image'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'background_image')); $_block_repeat=true; echo smarty_block_label(array('for'=>'background_image'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Background Image<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'background_image'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <div class="a4_instructions">
        <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Background image needs to be in exact dimensions of <strong>210mm x 297mm 300dpi</strong> and in <strong>PNG</strong> format. If you upload image which is not in those dimensions, it will be stretched out to fit invoice. Bear in mind that it's desirable to optimize this image as that will affect PDF file size. If possible, avoid using transparency as it will greatly increase rendering time, and memory consumption.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
      </div>
      
      <div class="letter_instructions">
        <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Background image needs to be in exact dimensions of <strong>8½ by 11 inches 300dpi</strong> and in <strong>PNG</strong> format. If you upload image which is not in those dimensions, it will be stretched out to fit invoice. Bear in mind that it's desirable to optimize this image as that will affect PDF file size. If possible, avoid using transparency as it will greatly increase rendering time, and memory consumption.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
      </div>
      
      <div id="upload_image_segment">
        <?php echo smarty_function_file_field(array('name'=>"background_image"),$_smarty_tpl);?>
 <?php echo smarty_function_link_button(array('id'=>"remove_background_image",'label'=>"Remove Background Image",'href'=>$_smarty_tpl->tpl_vars['remove_background_image_url']->value),$_smarty_tpl);?>

      </div>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'background_image'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  </div>
  
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  
  <script type="text/javascript">
  
    var form = $('#invoice_paper_form');
    var paper_size = form.find('#paper_format');
    var remove_image = form.find('#remove_background_image');
    var upload_image_segment = form.find('#upload_image_segment');
    var submit_button = form.find('.button_holder button[type="submit"]');
    
    var a4_instructions = form.find('.a4_instructions');
    var letter_instructions = form.find('.letter_instructions');

    paper_size.change(function () {
      if (paper_size.val() == 'A4') {
        a4_instructions.show();
        letter_instructions.hide(); 
      } else {
        a4_instructions.hide();
        letter_instructions.show(); 
      } // if      
    }).change();

    if (remove_image.attr('href')) {
	    remove_image.click(function () {
	      upload_image_segment.hide();
	      var action_indicator = $('<div class="remove_invoice_background"><img src="' + App.Wireframe.Utils.indicatorUrl() + '" alt="" />Removing Background Image</div>').insertAfter(upload_image_segment);
	      submit_button.attr('disabled', true);

        $.ajax({
          'url' : remove_image.attr('href'),
          'type' : 'post',
          'data' : { 'submitted' : 'submitted' },
          'success' : function (response) {
            submit_button.attr('disabled', false);
            action_indicator.remove();
            upload_image_segment.show();
            App.Wireframe.Flash.success(App.lang('Invoice background image removed successfully'));
            App.Wireframe.Events.trigger('invoice_template_updated');
            remove_image.hide();    
          },
          'error' : function (response) {
            submit_button.attr('disabled', false);
            action_indicator.remove();
            upload_image_segment.show();
            App.Wireframe.Flash.error(App.lang('Failed to remove background image'));
          }
        });
        
	      return false;
	    });
    } else {
      remove_image.hide();
    } // if

  
  </script>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"invoice_paper_form",'enctype'=>"multipart/form-data"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>