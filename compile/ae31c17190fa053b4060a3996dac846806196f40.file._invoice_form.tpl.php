<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:18:32
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/_invoice_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3814106815742e708cb3f01-35689563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ae31c17190fa053b4060a3996dac846806196f40' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/_invoice_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3814106815742e708cb3f01-35689563',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'invoice_data' => 0,
    'logged_user' => 0,
    'active_invoice' => 0,
    'invoice_item_templates' => 0,
    'invoice_item_template' => 0,
    'time_record_id' => 0,
    'allow_payment' => 0,
    'original_note' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e708e32669_34201784',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e708e32669_34201784')) {function content_5742e708e32669_34201784($_smarty_tpl) {?><?php if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_select_company')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_company.php';
if (!is_callable('smarty_block_textarea_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.textarea_field.php';
if (!is_callable('smarty_function_select_currency')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.select_currency.php';
if (!is_callable('smarty_function_select_language')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.select_language.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_project')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project.php';
if (!is_callable('smarty_function_checkbox')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_link_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.link_button.php';
if (!is_callable('smarty_block_link_button_dropdown')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/block.link_button_dropdown.php';
if (!is_callable('smarty_function_select_invoice_second_tax_mode')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_invoice_second_tax_mode.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_payments_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/payments/helpers/function.select_payments_type.php';
if (!is_callable('smarty_block_invoice_comment')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/block.invoice_comment.php';
if (!is_callable('smarty_block_invoice_note')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/block.invoice_note.php';
?><script type="text/javascript"> 
  App.widgets.FlyoutDialog.front().setAutoSize(false);
</script>

<div class="big_form_wrapper two_form_sidebars invoice_form">
  <div class="main_form_column">

      <div class="main_invoice_settings">
        <table>
          <tr>
            <td class="left_cell">
              <div class="invoice_client_address labels_inline">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'company_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'company_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                  <?php echo smarty_function_select_company(array('name'=>"invoice[company_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['company_id'],'class'=>'required','id'=>"companyId",'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'can_create_new'=>true,'required'=>true,'label'=>'Client','success_event'=>'company_created','exclude_owner_company'=>true),$_smarty_tpl);?>

                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'company_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'company_address','class'=>'companyAddressContainer')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'company_address','class'=>'companyAddressContainer'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                  <?php $_smarty_tpl->smarty->_tag_stack[] = array('textarea_field', array('name'=>"invoice[company_address]",'id'=>'companyAddress','class'=>'required long','label'=>'Address','required'=>true)); $_block_repeat=true; echo smarty_block_textarea_field(array('name'=>"invoice[company_address]",'id'=>'companyAddress','class'=>'required long','label'=>'Address','required'=>true), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['invoice_data']->value['company_address'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_textarea_field(array('name'=>"invoice[company_address]",'id'=>'companyAddress','class'=>'required long','label'=>'Address','required'=>true), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'company_address','class'=>'companyAddressContainer'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </div>
            </td>
            <td class="right_cell">
              <div class="invoice_common_settings">
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'currencyId','class'=>'firstHolder')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'currencyId','class'=>'firstHolder'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                  <?php echo smarty_function_select_currency(array('name'=>"invoice[currency_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['currency_id'],'class'=>'short','id'=>'currencyId','label'=>'Currency'),$_smarty_tpl);?>

                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'currencyId','class'=>'firstHolder'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'language','class'=>'secondHolder')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'language','class'=>'secondHolder'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                  <?php echo smarty_function_select_language(array('name'=>"invoice[language_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['language_id'],'label'=>'Language','preselect_default'=>true),$_smarty_tpl);?>

                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'language','class'=>'secondHolder'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'project_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'project_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                  <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->isLoaded()&&$_smarty_tpl->tpl_vars['active_invoice']->value->getBasedOn() instanceof Project){?>
                    <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                    <p><?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getBasedOn()->getName(),$_smarty_tpl);?>
</p>
                    <input type="hidden" name="invoice[project_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['active_invoice']->value->getBasedOn()->getId(),$_smarty_tpl);?>
">
                  <?php }else{ ?>
                    <?php echo smarty_function_select_project(array('name'=>"invoice[project_id]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['project_id'],'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'show_all'=>true,'optional'=>true,'label'=>'Project'),$_smarty_tpl);?>

                    <?php echo smarty_function_checkbox(array('label'=>"Only client's projects",'name'=>"selected_company_projects",'id'=>"selected_company_projects"),$_smarty_tpl);?>

                  <?php }?>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'project_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </div>
            </td>
          </tr>
        </table>
      </div>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'items','class'=>"invoice_items_wrapper")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'items','class'=>"invoice_items_wrapper"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <table class="validate_callback validate_invoice_items" cellspacing="0">
          <thead>
	          <tr class="header">
	            <th class="num">
	              <input type="hidden" name="invoice_sub_total" id="invoice_sub_total" />
	              <input type="hidden" name="invoice_total" id="invoice_total" />
	            </th>
	            <th class="description header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	            <th class="quantity header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Quantity<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	            <th class="unit_cost header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unit Cost<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>
                <th class="tax_rate header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax #1<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
                <th class="tax_rate header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax #2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <?php }else{ ?>
                <th class="tax_rate header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tax<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <?php }?>
	            <th class="subtotal header_cell" style="display: none"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subtotal<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	            <th class="total header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
	            <th class="options"></th>
	          </tr>
          </thead>
          <tbody>
          </tbody>
	        <tfoot>
	          <tr class="invoice_subtotal">
              <td colspan="<?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>5<?php }else{ ?>4<?php }?>"></td>
	            <td class="header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subtotal<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
	            <td class="total field_cell"><input class="subtotal" type="text" disabled="disabled" /></td>
	            <td class="end_cell"></td>
	          </tr>
	          <tr class="invoice_total">
              <td colspan="<?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>5<?php }else{ ?>4<?php }?>"></td>
	            <td class="header_cell"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
	            <td class="total field_cell"><input class="total" type="text" disabled="disabled"/></td>
	            <td class="end_cell"></td>
	          </tr>
          </tfoot>
        </table>
        
        <div class="invoice_item_buttons">
	        <?php echo smarty_function_link_button(array('label'=>"Add New Item",'icon_class'=>'button_add','id'=>"add_new"),$_smarty_tpl);?>

	        <?php if (is_foreachable($_smarty_tpl->tpl_vars['invoice_item_templates']->value)){?>
		        <?php $_smarty_tpl->smarty->_tag_stack[] = array('link_button_dropdown', array('label'=>"Add From Template",'icon_class'=>'button_duplicate','id'=>"add_from_template")); $_block_repeat=true; echo smarty_block_link_button_dropdown(array('label'=>"Add From Template",'icon_class'=>'button_duplicate','id'=>"add_from_template"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		          <ul>
		            <?php  $_smarty_tpl->tpl_vars['invoice_item_template'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['invoice_item_template']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['invoice_item_templates']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['invoice_item_template']->key => $_smarty_tpl->tpl_vars['invoice_item_template']->value){
$_smarty_tpl->tpl_vars['invoice_item_template']->_loop = true;
?>
		              <li><a href="<?php echo clean($_smarty_tpl->tpl_vars['invoice_item_template']->value->getId(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['invoice_item_template']->value->getDescription(),$_smarty_tpl);?>
</a></li>
		            <?php } ?>
		          </ul>
		        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link_button_dropdown(array('label'=>"Add From Template",'icon_class'=>'button_duplicate','id'=>"add_from_template"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	        <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getSecondTaxIsEnabled()){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"second_tax_is_compound")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"second_tax_is_compound"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

              <?php echo smarty_function_select_invoice_second_tax_mode(array('name'=>'invoice[second_tax_is_compound]','value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['second_tax_is_compound'],'id'=>'second_tax_is_compound_toggler'),$_smarty_tpl);?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"second_tax_is_compound"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php }?>
        </div>
      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'items','class'=>"invoice_items_wrapper"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    
    <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->isNew()&&is_foreachable($_smarty_tpl->tpl_vars['invoice_data']->value['time_record_ids'])){?>
      <?php  $_smarty_tpl->tpl_vars['time_record_id'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['time_record_id']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['invoice_data']->value['time_record_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['time_record_id']->key => $_smarty_tpl->tpl_vars['time_record_id']->value){
$_smarty_tpl->tpl_vars['time_record_id']->_loop = true;
?>
      <input type="hidden" name="invoice[time_record_ids][]" value="<?php echo clean($_smarty_tpl->tpl_vars['time_record_id']->value,$_smarty_tpl);?>
" />
      <?php } ?>
    <?php }?>    
  </div>
  
  <div class="form_sidebar form_second_sidebar">
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'number','id'=>'invoiceNumberGenerator')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'number','id'=>'invoiceNumberGenerator'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'invoiceNumber')); $_block_repeat=true; echo smarty_block_label(array('for'=>'invoiceNumber'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice ID<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'invoiceNumber'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getStatus()==@INVOICE_STATUS_ISSUED){?>
        <?php echo smarty_function_text_field(array('name'=>"invoice[number]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['number'],'class'=>'','id'=>'invoiceNumber','disabled'=>'disabled'),$_smarty_tpl);?>

      <?php }else{ ?>
        <div id="autogenerateID" style="<?php if ($_smarty_tpl->tpl_vars['invoice_data']->value['number']){?>display:none<?php }?>">
          <div class="field_wrapper"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Auto-generate on issue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<a href="#">(<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Specify<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
)</a></div>
        </div>
        <div id="manuallyID" style="<?php if (!$_smarty_tpl->tpl_vars['invoice_data']->value['number']){?>display:none<?php }?>">
          <div class="field_wrapper"><?php echo smarty_function_text_field(array('name'=>"invoice[number]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['number'],'class'=>'','id'=>'invoiceNumber'),$_smarty_tpl);?>
<a href="#">(<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Generate<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
)</a></div>
        </div>        
      <?php }?>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'number','id'=>'invoiceNumberGenerator'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    
    <?php if ($_smarty_tpl->tpl_vars['active_invoice']->value->getStatus()==@INVOICE_STATUS_ISSUED){?>
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'issued_on','class'=>'firstHolder')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'issued_on','class'=>'firstHolder'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php echo smarty_function_select_date(array('name'=>"invoice[issued_on]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['issued_on'],'label'=>"Issued On",'required'=>true),$_smarty_tpl);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'issued_on','class'=>'firstHolder'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'issued_on','class'=>'secondHolder')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'issued_on','class'=>'secondHolder'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php echo smarty_function_select_date(array('name'=>"invoice[due_on]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['due_on'],'label'=>'Payment Due On','required'=>true),$_smarty_tpl);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'issued_on','class'=>'secondHolder'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php }?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'purchase_order_number')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'purchase_order_number'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Purchase Order Number<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php echo smarty_function_text_field(array('name'=>"invoice[purchase_order_number]",'value'=>$_smarty_tpl->tpl_vars['invoice_data']->value['purchase_order_number'],'id'=>"purchase_order_number"),$_smarty_tpl);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'purchase_order_number'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <?php if ($_smarty_tpl->tpl_vars['allow_payment']->value){?>
      <div class="invoice_client_address">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'allow_partial')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'allow_partial'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_select_payments_type(array('name'=>"invoice[allow_payments]",'selected'=>$_smarty_tpl->tpl_vars['invoice_data']->value['payment_type'],'label'=>'Payments','required'=>true),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'allow_partial'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    <?php }?>

    <div class="invoice_comment_wrapper">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'comment')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'comment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('invoice_comment', array('name'=>"invoice[private_note]",'label'=>'Our Private Comment')); $_block_repeat=true; echo smarty_block_invoice_comment(array('name'=>"invoice[private_note]",'label'=>'Our Private Comment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['invoice_data']->value['private_note'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_invoice_comment(array('name'=>"invoice[private_note]",'label'=>'Our Private Comment'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
This comment is never displayed to the client or included in the final invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'comment'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
    
  </div>
  
  <div class="invoice_details_wrapper"> 
    <div class="invoice_note_wrapper">
    	<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'note')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'note'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      	<?php $_smarty_tpl->smarty->_tag_stack[] = array('invoice_note', array('name'=>'invoice[note]','original_note'=>$_smarty_tpl->tpl_vars['original_note']->value,'label'=>'Public Invoice Note','select_default'=>$_smarty_tpl->tpl_vars['active_invoice']->value->isNew())); $_block_repeat=true; echo smarty_block_invoice_note(array('name'=>'invoice[note]','original_note'=>$_smarty_tpl->tpl_vars['original_note']->value,'label'=>'Public Invoice Note','select_default'=>$_smarty_tpl->tpl_vars['active_invoice']->value->isNew()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['invoice_data']->value['note'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_invoice_note(array('name'=>'invoice[note]','original_note'=>$_smarty_tpl->tpl_vars['original_note']->value,'label'=>'Public Invoice Note','select_default'=>$_smarty_tpl->tpl_vars['active_invoice']->value->isNew()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      	<p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice note is included in the final invoice and visible to the client<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
     	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'note'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
  </div>  
</div>

<input type="hidden" name="invoice[quote_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['invoice_data']->value['quote_id'],$_smarty_tpl);?>
" /><?php }} ?>