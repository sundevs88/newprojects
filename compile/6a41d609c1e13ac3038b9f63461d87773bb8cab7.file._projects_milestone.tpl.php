<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:19:16
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/printer/project/_projects_milestone.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15701313885742e734d69483-21731192%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a41d609c1e13ac3038b9f63461d87773bb8cab7' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/printer/project/_projects_milestone.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15701313885742e734d69483-21731192',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_upcoming_objects' => 0,
    '_headline' => 0,
    'object' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e734daa1a9_02495287',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e734daa1a9_02495287')) {function content_5742e734daa1a9_02495287($_smarty_tpl) {?><?php if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_user_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/helpers/function.user_link.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_function_due')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/complete/helpers/function.due.php';
?><div class="project_milestone">
<?php if (is_foreachable($_smarty_tpl->tpl_vars['_upcoming_objects']->value)){?>
    <div class="project_overview_box">
      <div class="project_overview_box_title">
        <h2><?php echo clean($_smarty_tpl->tpl_vars['_headline']->value,$_smarty_tpl);?>
</h2>
      </div>
      <div class="project_overview_box_content"><div class="project_overview_box_content_inner">
        <table class="common" cellspacing="0">
          <thead>
            <tr>
              <th class="milestone"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Milestone<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <th class="responsible"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Responsible Person<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <th class="start"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Start On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <th class="due"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Due On<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            </tr>
          </thead>
          <tbody>
          <?php  $_smarty_tpl->tpl_vars['object'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['object']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_upcoming_objects']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['object']->key => $_smarty_tpl->tpl_vars['object']->value){
$_smarty_tpl->tpl_vars['object']->_loop = true;
?>
            <tr class="<?php if ($_smarty_tpl->tpl_vars['object']->value->isLate()){?>late<?php }elseif($_smarty_tpl->tpl_vars['object']->value->isUpcoming()){?>upcoming<?php }else{ ?>today<?php }?>">
              <td class="milestone"><a href="<?php echo clean($_smarty_tpl->tpl_vars['object']->value->getViewUrl(),$_smarty_tpl);?>
"><?php echo clean($_smarty_tpl->tpl_vars['object']->value->getName(),$_smarty_tpl);?>
</a></td>
              <td class="responsible">
                <?php if ($_smarty_tpl->tpl_vars['object']->value->assignees()->hasAssignee()){?>
                  <span class="details block"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['object']->value->assignees()->getAssignee()),$_smarty_tpl);?>
</span>
                <?php }else{ ?>
                  ---
                <?php }?>
              </td>
              <td class="due"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['object']->value->getStartOn(),0),$_smarty_tpl);?>
</td>
              <td class="due"><?php echo smarty_function_due(array('object'=>$_smarty_tpl->tpl_vars['object']->value),$_smarty_tpl);?>
</td>
            </tr>
          <?php } ?>
          </tbody>
        </table>
      </div></div>
    </div>
  <?php }?>  
</div><?php }} ?>