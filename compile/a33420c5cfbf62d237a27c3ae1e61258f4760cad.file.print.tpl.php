<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 14:14:04
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/layouts/print.tpl" */ ?>
<?php /*%%SmartyHeaderCode:60691194756dae9ac458a34-14522349%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a33420c5cfbf62d237a27c3ae1e61258f4760cad' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/layouts/print.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '60691194756dae9ac458a34-14522349',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'locale_code' => 0,
    'wireframe' => 0,
    'owner_company' => 0,
    'content_for_layout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56dae9ac47c178_37603161',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56dae9ac47c178_37603161')) {function content_56dae9ac47c178_37603161($_smarty_tpl) {?><?php if (!is_callable('smarty_function_page_head_tags')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.page_head_tags.php';
if (!is_callable('smarty_function_page_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.page_title.php';
if (!is_callable('smarty_function_template_vars_to_js')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.template_vars_to_js.php';
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo clean($_smarty_tpl->tpl_vars['locale_code']->value,$_smarty_tpl);?>
" lang="<?php echo clean($_smarty_tpl->tpl_vars['locale_code']->value,$_smarty_tpl);?>
">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedPrintStylesheetsUrl(),$_smarty_tpl);?>
" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedPrintStylesheetsUrl(),$_smarty_tpl);?>
" type="text/css" media="print" />
    <script type="text/javascript" src="<?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getCollectedPrintJavaScriptUrl(),$_smarty_tpl);?>
"></script>
    <?php echo smarty_function_page_head_tags(array(),$_smarty_tpl);?>

    <title><?php echo smarty_function_page_title(array('default'=>"Projects"),$_smarty_tpl);?>
 / <?php echo clean($_smarty_tpl->tpl_vars['owner_company']->value->getName(),$_smarty_tpl);?>
</title>
    <?php echo smarty_function_template_vars_to_js(array('wireframe'=>$_smarty_tpl->tpl_vars['wireframe']->value),$_smarty_tpl);?>

    <style>
      .opera_print {
        text-align: right;
        margin: 0;
        padding: 5px;
        border-bottom: 1px solid #000;
        background:#ccc;
      }

      @media print {
        .opera_print {
          display: none;
        }
      }
    </style>
  </head>
  <body>
    <div class="page_header" id="page_header"></div>
    <h1 class="page_title" id="page_title"><?php echo clean($_smarty_tpl->tpl_vars['wireframe']->value->getPageTitle(),$_smarty_tpl);?>
</h1>
    <div class="page_content" id="page_content"><?php echo $_smarty_tpl->tpl_vars['content_for_layout']->value;?>
</div>
    <div class="page_footer" id="page_footer"></div>
  </body>
  <script type="text/javascript">
    if (!$.browser.opera) {
      setTimeout(function () {
        window.focus();
        window.print();
        hidePrintOverlay();
      }, 1000);
    } else {
      $('body').prepend('<p class="opera_print"><input type="button" onclick="window.print();window.close()" value="' + App.lang('Print') + '"/></p>');
      var clone = document.documentElement.cloneNode(true)
      var win = window.open('about:blank');
      win.document.replaceChild(clone, win.document.documentElement);
      hidePrintOverlay();
    } // if

    /**
     * Hide "preparing for printing overlay"
     */
    function hidePrintOverlay() {
      if (window.parent && window.parent.App && window.parent.App.Wireframe && window.parent.App.Wireframe.Print) {
        window.parent.App.Wireframe.Print.close();
      } // if
    } // hidePrintOverlay
  </script>
</html><?php }} ?>