<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 08:43:16
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/tasks_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:158740000056da9c247f84f1-37179954%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a63edd67be7ac8afdc3891b2091cf5967ce08d3e' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/default/tasks_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '158740000056da9c247f84f1-37179954',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'owner_company' => 0,
    'tasks_auto_reopen' => 0,
    'tasks_auto_reopen_clients_only' => 0,
    'tasks_public_submit_enabled' => 0,
    'tasks_use_captcha' => 0,
    'task_custom_fields' => 0,
    'forms' => 0,
    'forms_per_page' => 0,
    'total_forms' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56da9c24877aa2_61027576',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56da9c24877aa2_61027576')) {function content_56da9c24877aa2_61027576($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.link.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="tasks_admin" class="wireframe_content_wrapper settings_panel">
  <div class="settings_panel_header">
    <table class="settings_panel_header_cell_wrapper">
      <tr>
        <td class="settings_panel_header_cell">
          <h2><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h2>
		      <div class="properties">
		        <div class="property" id="tasks_setting_auto_reopen">
		          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Auto-Reopen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
		          <div class="data"></div>
		        </div>
		        
		        <div class="property" id="tasks_setting_public_forms_enabled">
		          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Public Forms<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
		          <div class="data"></div>
		        </div>
		        
		        <div class="property" id="tasks_setting_use_captcha">
		          <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
CAPTCHA<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
		          <div class="data"></div>
		        </div>

            <div class="property" id="tasks_setting_custom_fields">
              <div class="label"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Custom Fields<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
              <div class="data"></div>
            </div>
		      </div>
          
          <ul class="settings_panel_header_cell_actions">
            <li><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('tasks_admin_settings'),'mode'=>'flyout_form','title'=>"Change Settings",'success_event'=>'tasks_settings_updated','class'=>"link_button_alternative")); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('tasks_admin_settings'),'mode'=>'flyout_form','title'=>"Change Settings",'success_event'=>'tasks_settings_updated','class'=>"link_button_alternative"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('tasks_admin_settings'),'mode'=>'flyout_form','title'=>"Change Settings",'success_event'=>'tasks_settings_updated','class'=>"link_button_alternative"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
          </ul>
        </td>
      </tr>
    </table>
  </div>
  
  <div class="settings_panel_body">
    <div id="public_task_forms"></div>
  </div>
</div>

<script type="text/javascript">
  $('#tasks_admin').each(function() {
    var wrapper = $(this);

    /**
     * Update messages in settings block
     *
     * @param Boolean auto_reopen
     * @param Boolean auto_reopen_clients_only
     * @param Boolean public_submit_enabled
     * @param Boolean use_captcha
     * @param Array custom_fields
     */
    var update_settings_display = function(auto_reopen, auto_reopen_clients_only, public_submit_enabled, use_captcha, custom_fields) {
      if(auto_reopen) {
        if(auto_reopen_clients_only) {
          $('#tasks_setting_auto_reopen div.data').text(App.lang('Enabled. Completed tasks will be reopened when user who is not a member of :owner company posts a comment', {
            'owner' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['owner_company']->value->getName());?>

          }));
        } else {
          $('#tasks_setting_auto_reopen div.data').text(App.lang('Enabled. Completed tasks will be reopened when new comment is posted'));
        } // if
      } else {
        $('#tasks_setting_auto_reopen div.data').text(App.lang('Disabled. Completed tasks will not be reopened on new comments'));
      } // if

      if(public_submit_enabled) {
        $('#tasks_setting_public_forms_enabled div.data').text(App.lang('Enabled. Visitors will be able to create new tasks through public forms'));
        $('#tasks_setting_use_captcha').show().find('div.data').text(use_captcha ?
          App.lang('Public tasks forms are protected with CAPTCHA images') :
          App.lang('Public tasks forms are not protected with CAPTCHA images')
        );
      } else {
        $('#tasks_setting_public_forms_enabled div.data').text(App.lang('Disabled. Users will not be able to post new tasks without logging in'));
        $('#tasks_setting_use_captcha').hide();
      } // if

      if(jQuery.isArray(custom_fields) && custom_fields.length) {
        var cleared_values = [];

        App.each(custom_fields, function(k, v) {
          cleared_values.push(App.clean(v));
        });

        wrapper.find('#tasks_setting_custom_fields div.data').empty().append(cleared_values.join(', '));
      } else {
        wrapper.find('#tasks_setting_custom_fields div.data').empty().append(App.lang('Custom Fields are not Configured'));
      } // if
    }; // update_settings_display

    // Initial value
    update_settings_display(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tasks_auto_reopen']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tasks_auto_reopen_clients_only']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tasks_public_submit_enabled']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['tasks_use_captcha']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_custom_fields']->value);?>
);

    // On update
    App.Wireframe.Events.bind('tasks_settings_updated.content', function(e, response) {
      if(typeof(response) == 'object') {
        var auto_reopen = response['tasks_auto_reopen'];
        var auto_reopen_clients_only = auto_reopen && response['tasks_auto_reopen_clients_only'];
        var public_submit_enabled = response['tasks_public_submit_enabled'];
        var use_captcha = public_submit_enabled && response['tasks_use_captcha'];
        var custom_fields = typeof(response['task_custom_fields']) != 'undefined' && jQuery.isArray(response['task_custom_fields']) ? response['task_custom_fields'] : [];
      } else {
        var auto_reopen = false, auto_reopen_clients_only = false, public_submit_enabled = false, use_captcha = false; custom_fields = false;
      } // if

      update_settings_display(auto_reopen, auto_reopen_clients_only, public_submit_enabled, use_captcha, custom_fields);
    });

    // Forms
    wrapper.find('#public_task_forms').pagedObjectsList({
      'load_more_url' : '<?php echo smarty_function_assemble(array('route'=>'tasks_admin'),$_smarty_tpl);?>
',
      'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['forms']->value);?>
,
      'items_per_load' : <?php echo clean($_smarty_tpl->tpl_vars['forms_per_page']->value,$_smarty_tpl);?>
,
      'total_items' : <?php echo clean($_smarty_tpl->tpl_vars['total_forms']->value,$_smarty_tpl);?>
,
      'list_items_are' : 'tr',
      'list_item_attributes' : { 'class' : 'task_form' },
      'columns' : {
        'is_enabled' : '',
        'name' : App.lang('Form'),
        'project' : App.lang('Project'),
        'options' : ''
      },
      'sort_by' : function() {
        return $(this).find('td.name span.form_name').text();
      },
      'empty_message' : App.lang('There are no public task forms defined'),
      'listen' : 'public_task_form',
      'on_add_item' : function(item) {
        var form = $(this);

        form.append(
          '<td class="is_enabled"></td>' +
            '<td class="name"></td>' +
            '<td class="project"></td>' +
            '<td class="options"></td>'
        );

        form.attr('id', item['id']);

        var checkbox = $('<input type="checkbox" />').attr({
          'on_url' : item['urls']['enable'],
          'off_url' : item['urls']['disable']
        }).asyncCheckbox({
            'success_event' : 'public_task_form_updated',
            'success_message' : [ App.lang('Form has been disabled'), App.lang('Form has been enabled') ]
          }).appendTo(form.find('td.is_enabled'));

        if(item['is_enabled']) {
          checkbox[0].checked = true;
        } // if

        form.find('td.name').html('<span class="form_name">' + App.clean(item['name']) + '</span>' + '<a href="' + App.clean(item['urls']['public']) + '" class="form_url" target="_blank">' + App.clean(item['urls']['public']) + '</a>');
        $('<a></a>').attr('href', item['project']['url']).text(item['project']['name']).appendTo(form.find('td.project'));

        form.find('td.options')
          .append('<a href="' + item['urls']['edit'] + '" class="edit_form" title="' + App.lang('Change Settings') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Edit') + '" /></a>')
          .append('<a href="' + item['urls']['delete'] + '" class="delete_form" title="' + App.lang('Remove Form') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Delete') + '" /></a>')
        ;

        form.find('td.options a.edit_form').flyoutForm({
          'success_event' : 'public_task_form_updated'
        });
        form.find('td.options a.delete_form').asyncLink({
          'confirmation' : App.lang('Are you sure that you want to permanently delete this public task form?'),
          'success_event' : 'public_task_form_deleted',
          'success_message' : App.lang('Public task form has been deleted successfully')
        });
      }
    });
  });
</script><?php }} ?>