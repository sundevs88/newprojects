<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 18:08:18
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/comments/views/phone/_render_comment_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:44220068256d9cf12811f46-20438480%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c8d4a268b9e10b2296ae0a7ba364dbda8c4b2fda' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/comments/views/phone/_render_comment_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44220068256d9cf12811f46-20438480',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'post_comment_url' => 0,
    'comment_data' => 0,
    'id' => 0,
    'object' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d9cf12852a57_71521253',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d9cf12852a57_71521253')) {function content_56d9cf12852a57_71521253($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap_editor')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_editor.php';
if (!is_callable('smarty_block_editor_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/visual_editor/helpers/block.editor_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php if ($_smarty_tpl->tpl_vars['post_comment_url']->value){?>
  <div id="post_comment">
	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['post_comment_url']->value)); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['post_comment_url']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_editor', array('field'=>'body')); $_block_repeat=true; echo smarty_block_wrap_editor(array('field'=>'body'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

			  <?php $_smarty_tpl->smarty->_tag_stack[] = array('editor_field', array('name'=>'comment[body]','label'=>'Leave a Comment','id'=>'comment_body')); $_block_repeat=true; echo smarty_block_editor_field(array('name'=>'comment[body]','label'=>'Leave a Comment','id'=>'comment_body'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['comment_data']->value['body'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_editor_field(array('name'=>'comment[body]','label'=>'Leave a Comment','id'=>'comment_body'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

			<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_editor(array('field'=>'body'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Comment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['post_comment_url']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	</div>
	
	<script type="text/javascript">
		$(document).ready(function() {
			var comment_block = $('#post_comment');
	    
	    comment_block.find('textarea').each(function() {
	      var comment = $(this).val();
	      
	      comment_block.find('form').submit(function() {
				  var comment_value = jQuery.trim(comment.val());
		      if(comment_value) {
		        $('#<?php echo clean($_smarty_tpl->tpl_vars['id']->value,$_smarty_tpl);?>
').listview('refresh');
		        return true;
		      } // if
		      
				  return false;
				});
	    });
	  });
	</script>
<?php }else{ ?>
  <div class="object_comments_locked">
    <img src="<?php echo smarty_function_image_url(array('name'=>'icons/32x32/comments-locked.png','module'=>@COMMENTS_FRAMEWORK,'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
" alt=""/>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('object_type'=>mb_strtolower($_smarty_tpl->tpl_vars['object']->value->getVerboseType(), 'UTF-8'))); $_block_repeat=true; echo smarty_block_lang(array('object_type'=>mb_strtolower($_smarty_tpl->tpl_vars['object']->value->getVerboseType(), 'UTF-8')), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Comments for this :object_type are locked<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('object_type'=>mb_strtolower($_smarty_tpl->tpl_vars['object']->value->getVerboseType(), 'UTF-8')), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  </div>
<?php }?><?php }} ?>