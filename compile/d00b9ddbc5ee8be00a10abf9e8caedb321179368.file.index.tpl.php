<?php /* Smarty version Smarty-3.1.12, created on 2016-03-07 03:59:05
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoicing_settings_admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11302549356dcfc89ef6946-73947314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd00b9ddbc5ee8be00a10abf9e8caedb321179368' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoicing_settings_admin/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11302549356dcfc89ef6946-73947314',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings_data' => 0,
    'description_formats' => 0,
    'description_format_config_option' => 0,
    'description_format' => 0,
    'counters' => 0,
    'change_counter_value_url' => 0,
    'pattern_variables' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56dcfc8a191c56_71938596',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56dcfc8a191c56_71938596')) {function content_56dcfc8a191c56_71938596($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_when_invoice_is_based_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.when_invoice_is_based_on.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_list_financial_managers')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.list_financial_managers.php';
if (!is_callable('smarty_function_select_invoice_due_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_invoice_due_on.php';
if (!is_callable('smarty_block_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.link.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_function_select_counter_padding')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_counter_padding.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php echo smarty_function_use_widget(array('name'=>'invoicing_settings_dialog','module'=>@INVOICING_MODULE),$_smarty_tpl);?>


<div id="invoicing_settings">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>Router::assemble('invoicing_settings'))); $_block_repeat=true; echo smarty_block_form(array('action'=>Router::assemble('invoicing_settings')), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="content_stack_wrapper">
      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Show Invoice As<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'print_invoices_as')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'print_invoices_as'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_text_field(array('name'=>'settings[print_invoices_as]','value'=>$_smarty_tpl->tpl_vars['settings_data']->value['print_invoices_as'],'label'=>"Issued Invoices"),$_smarty_tpl);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'print_invoices_as'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'print_proforma_invoices_as')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'print_proforma_invoices_as'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_text_field(array('name'=>'settings[print_proforma_invoices_as]','value'=>$_smarty_tpl->tpl_vars['settings_data']->value['print_proforma_invoices_as'],'label'=>"Draft (Proforma) Invoices"),$_smarty_tpl);?>

            <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Specify how invoices will be called in generated PDF files ("Tax Invoice" for example). To use default value, <u>leave these fields blank</u> (activeCollab will use "Invoice" and "Proforma Invoice", optionally translated using localization feature)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'print_proforma_invoices_as'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice from Tracked Records<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'when_invoice_is_based_on')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'when_invoice_is_based_on'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_when_invoice_is_based_on(array('name'=>"settings[on_invoice_based_on]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['on_invoice_based_on'],'label'=>'By Default, Group Records By','mode'=>'select','required'=>true),$_smarty_tpl);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'when_invoice_is_based_on'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'description_formats','id'=>'invoicing_settings_description_formats')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'description_formats','id'=>'invoicing_settings_description_formats'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Description Formats<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <table class="common" cellspacing="0">
              <?php  $_smarty_tpl->tpl_vars['description_format'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['description_format']->_loop = false;
 $_smarty_tpl->tpl_vars['description_format_config_option'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['description_formats']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['description_format']->key => $_smarty_tpl->tpl_vars['description_format']->value){
$_smarty_tpl->tpl_vars['description_format']->_loop = true;
 $_smarty_tpl->tpl_vars['description_format_config_option']->value = $_smarty_tpl->tpl_vars['description_format']->key;
?>
                <tr data-format-config-option="<?php echo clean($_smarty_tpl->tpl_vars['description_format_config_option']->value,$_smarty_tpl);?>
">
                  <td><?php echo clean($_smarty_tpl->tpl_vars['description_format']->value['text'],$_smarty_tpl);?>
</td>
                  <td class="format"><?php echo clean($_smarty_tpl->tpl_vars['description_format']->value['format'],$_smarty_tpl);?>
</td>
                </tr>
              <?php } ?>
            </table>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>Router::assemble('invoicing_settings_change_description_formats'),'mode'=>'flyout_form','flyout_title'=>'Change Formats','flyout_width'=>700,'success_event'=>'description_formats_updated','success_message'=>'Settings updated')); $_block_repeat=true; echo smarty_block_button(array('href'=>Router::assemble('invoicing_settings_change_description_formats'),'mode'=>'flyout_form','flyout_title'=>'Change Formats','flyout_width'=>700,'success_event'=>'description_formats_updated','success_message'=>'Settings updated'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change Formats<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>Router::assemble('invoicing_settings_change_description_formats'),'mode'=>'flyout_form','flyout_title'=>'Change Formats','flyout_width'=>700,'success_event'=>'description_formats_updated','success_message'=>'Settings updated'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'description_formats','id'=>'invoicing_settings_description_formats'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Taxes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_tax_options')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_tax_options'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <div><?php echo smarty_function_checkbox_field(array('name'=>"settings[invoice_second_tax_is_enabled]",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_second_tax_is_enabled'],'label'=>"Enable Second Tax for Invoices"),$_smarty_tpl);?>
</div>
            <div><?php echo smarty_function_checkbox_field(array('name'=>"settings[invoice_second_tax_is_compound]",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_second_tax_is_compound'],'label'=>"Second Tax is Compound Tax"),$_smarty_tpl);?>
</div>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_tax_options'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Notifications<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_notify_on_payment')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_notify_on_payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <label class="main_label">Please select which financial managers will be notified when new payment is received:</label>
            <div>
              <?php echo smarty_function_radio_field(array('name'=>"settings[invoice_notify_financial_managers]",'class'=>"notify_managers_radio",'pre_selected_value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_financial_managers'],'value'=>Invoice::INVOICE_NOTIFY_FINANCIAL_MANAGERS_NONE,'label'=>"Don't Notify Financial Managers"),$_smarty_tpl);?>

            </div>
            <div>
              <?php echo smarty_function_radio_field(array('name'=>"settings[invoice_notify_financial_managers]",'class'=>"notify_managers_radio",'pre_selected_value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_financial_managers'],'value'=>Invoice::INVOICE_NOTIFY_FINANCIAL_MANAGERS_ALL,'label'=>'Notify All Financial Managers'),$_smarty_tpl);?>

            </div>
            <div>
              <?php echo smarty_function_radio_field(array('name'=>"settings[invoice_notify_financial_managers]",'class'=>"notify_managers_radio",'pre_selected_value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_financial_managers'],'value'=>Invoice::INVOICE_NOTIFY_FINANCIAL_MANAGERS_SELECTED,'label'=>'Notify Selected Financial Managers'),$_smarty_tpl);?>

              <?php echo smarty_function_list_financial_managers(array('id'=>"list_financial_managers",'name'=>"settings[invoice_notify_financial_manager_ids]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_financial_manager_ids'],'label'=>"Financial Managers"),$_smarty_tpl);?>

            </div>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_notify_on_payment'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_payment_notification_option')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_payment_notification_option'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <div>
              <?php echo smarty_function_checkbox_field(array('name'=>"settings[invoice_notify_on_payment]",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_on_payment'],'label'=>"Notify Client when Invoice is Fully Paid"),$_smarty_tpl);?>

            </div>
            <div>
              <?php echo smarty_function_checkbox_field(array('name'=>"settings[invoice_notify_on_cancel]",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoice_notify_on_cancel'],'label'=>"Notify Client when Invoice is Canceled"),$_smarty_tpl);?>

            </div>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_payment_notification_option'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

      <div class="content_stack_element">
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Due After Issue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoicing_default_due')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoicing_default_due'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_select_invoice_due_on(array('name'=>"settings[invoicing_default_due]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoicing_default_due'],'label'=>'Default Due Date','allow_selected'=>false,'required'=>true),$_smarty_tpl);?>

            <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Users are able to change due date when they issue an invoice. This is just the default, pre-selected value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoicing_default_due'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>

      <div class="content_stack_element" >
        <div class="content_stack_element_info">
          <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Number Generator<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
        </div>
        <div class="content_stack_element_body">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_generator_pattern_input')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_generator_pattern_input'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_text_field(array('name'=>"settings[invoicing_number_pattern]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoicing_number_pattern'],'class'=>"invoice_generator_pattern_input",'label'=>"Generator Pattern"),$_smarty_tpl);?>
 <span class="invoice_generator_pattern_preview"></span>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_generator_pattern_input'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


          <div class="generator_patterns_and_counters">
            <ul class="invoice_generator_variables">
              <li>
                <strong>Counters</strong>
              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_NUMBER_COUNTER_TOTAL,$_smarty_tpl);?>
</a>

                <span class="change_counter_wrapper" counter_type="total_counter">
                  &mdash; <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Next Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <strong><?php echo clean($_smarty_tpl->tpl_vars['counters']->value['total_counter'],$_smarty_tpl);?>
</strong> <?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change")); $_block_repeat=true; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<img src="<?php echo smarty_function_image_url(array('name'=>'icons/12x12/edit.png','module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
                </span>

                <br>

                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice number in total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_NUMBER_COUNTER_YEAR,$_smarty_tpl);?>
</a>

                <span class="change_counter_wrapper" counter_type="year_counter">
                  &mdash; <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Next Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <strong><?php echo clean($_smarty_tpl->tpl_vars['counters']->value['year_counter'],$_smarty_tpl);?>
</strong> <?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change")); $_block_repeat=true; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<img src="<?php echo smarty_function_image_url(array('name'=>'icons/12x12/edit.png','module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
                </span>

                <br>

                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice number in current year<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_NUMBER_COUNTER_MONTH,$_smarty_tpl);?>
</a>

                <span class="change_counter_wrapper" counter_type="month_counter">
                  &mdash; <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Next Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <strong><?php echo clean($_smarty_tpl->tpl_vars['counters']->value['month_counter'],$_smarty_tpl);?>
</strong> <?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change")); $_block_repeat=true; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<img src="<?php echo smarty_function_image_url(array('name'=>'icons/12x12/edit.png','module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Change<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['change_counter_value_url']->value,'title'=>"Click to Change"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>
                </span>

                <br>

                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Invoice number in current month<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
            </ul>
            <ul class="invoice_generator_variables">
              <li>
                <strong>Variables</strong>
              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_VARIABLE_CURRENT_YEAR,$_smarty_tpl);?>
</a><br>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Current year in number format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_VARIABLE_CURRENT_MONTH,$_smarty_tpl);?>
</a><br>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Current month in number format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_VARIABLE_CURRENT_MONTH_SHORT,$_smarty_tpl);?>
</a><br>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Current month in short text format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
              <li>
                <a href="#" class="number_pattern_variable"><?php echo clean(@INVOICE_VARIABLE_CURRENT_MONTH_LONG,$_smarty_tpl);?>
</a><br>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Current month in long text format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

              </li>
            </ul>
          </div>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoicing_number_counter_padding')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoicing_number_counter_padding'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php echo smarty_function_select_counter_padding(array('name'=>"settings[invoicing_number_counter_padding]",'value'=>$_smarty_tpl->tpl_vars['settings_data']->value['invoicing_number_counter_padding'],'class'=>'invoice_generator_padding_select','label'=>"Fix Counter Length"),$_smarty_tpl);?>

            <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
When counter value length is fixed, system will prefix current counter value with appropriate number of zeros<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
          <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoicing_number_counter_padding'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        </div>
      </div>
    </div>
  
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    	<?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>Router::assemble('invoicing_settings')), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#invoicing_settings').each(function() {
    var wrapper = $(this);

    App.Wireframe.Events.bind('description_formats_updated.' + wrapper.parents('.flyout_dialog:first').attr('id'), function (event, response) {
      if(App.each(response, function(k, v) {
        var row = wrapper.find('#invoicing_settings_description_formats tr[data-format-config-option=' + k + ']');

        if(row.length) {
          var format = row.find('td.format');

          if(format.text() != v) {
            row.find('td.format').text(v);
            row.find('td').highlightFade();
          } // if
        } // if
      }));
    });

    var pattern_input = wrapper.find('input.invoice_generator_pattern_input');
    var pattern_preview = wrapper.find('span.invoice_generator_pattern_preview');
    var padding_select = wrapper.find('select.invoice_generator_padding_select');
    var pattern_variables = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['pattern_variables']->value);?>
;

    var notify_managers_radio = wrapper.find('input.notify_managers_radio');
    var list_financial_managers = wrapper.find('div#list_financial_managers');
		var  notify_managers_radio_checked = wrapper.find("input.notify_managers_radio:checked");

    // Hide financial manager list if "all" is selected
		if(wrapper.find("input.notify_managers_radio:checked").val() != "<?php echo clean(Invoice::INVOICE_NOTIFY_FINANCIAL_MANAGERS_SELECTED,$_smarty_tpl);?>
") {
			list_financial_managers.hide();
		} // if
    
    notify_managers_radio.change(function(){
			var obj = $(this);
			if(obj.val() === "<?php echo clean(Invoice::INVOICE_NOTIFY_FINANCIAL_MANAGERS_SELECTED,$_smarty_tpl);?>
") {
				list_financial_managers.show('slow');
			} else {
				list_financial_managers.hide('slow');
			} // if
    });
    
    /**
     * Prepare pattern preview value
     *
     * @param String pattern
     */
    var prepare_pattern_preview = function(pattern) {
      var pattern = pattern_input.val();
      var padding = padding_select.val() == '' ? 0 : parseInt(padding_select.val());
      
      for(var key in pattern_variables) {
        var regexp = new RegExp(key, "g");
        var replace_with = pattern_variables[key];

        if(padding && (key == ':invoice_in_total' || key == ':invoice_in_year' || key == ':invoice_in_month')) {
          replace_with = App.strPad(replace_with, padding, '0', 'STR_PAD_LEFT');
        } // if

        pattern = pattern.replace(regexp, replace_with);
      } // for
      
      pattern_preview.text(App.lang('Preview: :generated_preview', { 'generated_preview' : pattern }));
    }; // prepare_pattern_preview

    // Bind
    pattern_input.change(prepare_pattern_preview).keyup(prepare_pattern_preview);
    padding_select.change(prepare_pattern_preview);

    // Initial value
    pattern_input.change();

    wrapper.find('a.number_pattern_variable').click(function() {
      pattern_input.insertAtCursor($(this).text()).change();
      
      return false;
    });

    wrapper.find('span.change_counter_wrapper').each(function() {
      var change_counter_wrapper = $(this);

      change_counter_wrapper.find('a').click(function() {
        var link = $(this);

        if(link[0].block_async_clicks) {
          return false;
        } // if

        var counter_value_wrapper = change_counter_wrapper.find('strong');
        
        var counter_value = parseInt(counter_value_wrapper.text());
        var counter_type = change_counter_wrapper.attr('counter_type');
        
        switch(counter_type) {
        	case 'total_counter':
          	var input = prompt(App.lang('Change total invoices counter value'), counter_value); break;
        	case 'year_counter':
        	  var input = prompt(App.lang('Change counter value for the current year'), counter_value); break;
        	case 'month_counter':
        	  var input = prompt(App.lang('Change counter value for the current month'), counter_value); break;
         	default:
           	App.Wireframe.Flash.error('Unknown counter');
         		return false;
        } // switch

        if(input === null || input === '') {
          return false;
        } else {
          var img = link.find('img');

          var old_image_url = img.attr('src');
          img.attr('src', App.Wireframe.Utils.indicatorUrl('small'));

          link[0].block_async_clicks = true;
          
          $.ajax({
            'url' : link.attr('href'),
            'type' : 'post', 
            'data' : {
              'counter_type' : counter_type, 
              'counter_value' : input, 
              'submitted' : 'submitted'
            }, 
            'success' : function(response) {
              img.attr('src', old_image_url);
              link[0].block_async_clicks = false;

              switch(counter_type) {
              	case 'total_counter':
              	  pattern_variables[':invoice_in_total'] = response; break;
              	case 'year_counter':
              	  pattern_variables[':invoice_in_year'] = response; break;
              	case 'month_counter':
              	  pattern_variables[':invoice_in_month'] = response; break;
              } // switch

              counter_value_wrapper.text(response); // Update displayed value
              pattern_input.change(); // Update preview
            }, 
            'error' : function() {
              img.attr('src', old_image_url);
              link[0].block_async_clicks = false;
              
              App.Wireframe.Flash.error('Failed to update counter value. Please try again later');
            }
          });
        } // if

        return false;
      });
    });
  });
</script><?php }} ?>