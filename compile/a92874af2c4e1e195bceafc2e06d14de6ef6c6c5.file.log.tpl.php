<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 14:50:50
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/printer/project_tracking/log.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3552927625728bacac3ca68-75562031%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a92874af2c4e1e195bceafc2e06d14de6ef6c6c5' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/printer/project_tracking/log.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3552927625728bacac3ca68-75562031',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'items' => 0,
    'date' => 0,
    'records' => 0,
    'record' => 0,
    'project_currency' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728bacacf42c2_99815655',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728bacacf42c2_99815655')) {function content_5728bacacf42c2_99815655($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_hours')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.hours.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array('lang'=>'lang')); $_block_repeat=true; echo smarty_block_title(array('lang'=>'lang'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean($_smarty_tpl->tpl_vars['active_project']->value->getName(),$_smarty_tpl);?>
: <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array('lang'=>'lang'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php if (is_foreachable($_smarty_tpl->tpl_vars['items']->value)){?>
	<?php  $_smarty_tpl->tpl_vars['records'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['records']->_loop = false;
 $_smarty_tpl->tpl_vars['date'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['records']->key => $_smarty_tpl->tpl_vars['records']->value){
$_smarty_tpl->tpl_vars['records']->_loop = true;
 $_smarty_tpl->tpl_vars['date']->value = $_smarty_tpl->tpl_vars['records']->key;
?>
		<table class="tiemexpenses_table common" cellspacing="0">
      <thead>
        <tr>
          <th colspan="5"><?php echo clean($_smarty_tpl->tpl_vars['date']->value,$_smarty_tpl);?>
</th>
        </tr>
      </thead>
      <tbody>
		  <?php  $_smarty_tpl->tpl_vars['record'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['record']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['records']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['record']->key => $_smarty_tpl->tpl_vars['record']->value){
$_smarty_tpl->tpl_vars['record']->_loop = true;
?>
		  <tr>
		  	<td class="value" align="left">
        <?php if ($_smarty_tpl->tpl_vars['record']->value['type']=='TimeRecord'){?>
          <?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['value']),$_smarty_tpl);?>
h
        <?php }else{ ?>
          <?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['record']->value['value'],$_smarty_tpl->tpl_vars['project_currency']->value),$_smarty_tpl);?>

        <?php }?>
		  	</td>
        <td class="user"><?php echo clean($_smarty_tpl->tpl_vars['record']->value['user_display_name'],$_smarty_tpl);?>
</td>
        <td class="summary details" align="left">
        <?php if (($_smarty_tpl->tpl_vars['record']->value['parent_type']=='Task'&&$_smarty_tpl->tpl_vars['record']->value['parent_name'])&&$_smarty_tpl->tpl_vars['record']->value['summary']){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name'])); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task: :name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 (<?php echo clean($_smarty_tpl->tpl_vars['record']->value['summary'],$_smarty_tpl);?>
)
        <?php }elseif($_smarty_tpl->tpl_vars['record']->value['parent_type']=='Task'&&$_smarty_tpl->tpl_vars['record']->value['parent_name']){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name'])); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task: :name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }elseif($_smarty_tpl->tpl_vars['record']->value['summary']){?>
          <?php echo clean($_smarty_tpl->tpl_vars['record']->value['summary'],$_smarty_tpl);?>

        <?php }?>
        </td>
        <td class="status details" align="left">
        <?php if ($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_NOT_BILLABLE){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Not Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }elseif($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_BILLABLE){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }elseif($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_PENDING_PAYMENT){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Pending Payment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }elseif($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_PAID){?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paid<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }else{ ?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unknown<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
        </td>
		  </tr>    
		  <?php } ?>
		 </tbody>
	  </table>
	<?php } ?>
<?php }else{ ?>
	<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No Time or Expenses logged with this criteria<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>	
<?php }?><?php }} ?>