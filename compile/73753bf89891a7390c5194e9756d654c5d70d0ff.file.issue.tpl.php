<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:22:06
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/issue.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1504070255742e7de074881-99172734%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '73753bf89891a7390c5194e9756d654c5d70d0ff' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/issue.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1504070255742e7de074881-99172734',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_invoice' => 0,
    'issue_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e7de178b10_95367288',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e7de178b10_95367288')) {function content_5742e7de178b10_95367288($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap_fields')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_fields.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_invoice_due_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_invoice_due_on.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_company_financial_manager')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_company_financial_manager.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Issue Invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Issue<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="issue_invoice">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getIssueUrl(),'method'=>'post')); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getIssueUrl(),'method'=>'post'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_fields', array()); $_block_repeat=true; echo smarty_block_wrap_fields(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <div id="issue_invoice_dates">
  	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'issued_on')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'issued_on'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  	        <?php echo smarty_function_select_date(array('name'=>'issue[issued_on]','value'=>$_smarty_tpl->tpl_vars['issue_data']->value['issued_on'],'required'=>true,'label'=>'Issued On'),$_smarty_tpl);?>

  	      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'issued_on'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


  	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'due_in_days')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'due_in_days'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  	        <?php echo smarty_function_select_invoice_due_on(array('name'=>'issue[due_in_days]','value'=>$_smarty_tpl->tpl_vars['issue_data']->value['due_in_days'],'required'=>true,'label'=>'Payment Due On'),$_smarty_tpl);?>

  	      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'due_in_days'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    </div>

      <div id="issue_invoice_send_email">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Notify Client<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


        <p><input type="radio" name="issue[send_emails]" value="1" <?php if ($_smarty_tpl->tpl_vars['issue_data']->value['send_emails']){?>checked="checked"<?php }?> class="send_mails_radio inline input_radio" id="issueFormSendEmailsYes"> <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"issueFormSendEmailsYes",'main_label'=>false,'after_text'=>'')); $_block_repeat=true; echo smarty_block_label(array('for'=>"issueFormSendEmailsYes",'main_label'=>false,'after_text'=>''), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Send email to client<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"issueFormSendEmailsYes",'main_label'=>false,'after_text'=>''), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
        <div id="select_invoice_recipients" style="display: none">
          <?php echo smarty_function_select_company_financial_manager(array('name'=>'issue[user_id]','value'=>$_smarty_tpl->tpl_vars['issue_data']->value['user_id'],'company'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getCompany()),$_smarty_tpl);?>

          <p id="issue_invoice_send_email_pdf"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PDF version of the invoice will be attached to the email<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
        </div>
        <p><input type="radio" name="issue[send_emails]" value="0" <?php if (!$_smarty_tpl->tpl_vars['issue_data']->value['send_emails']){?>checked="checked"<?php }?> class="send_mails_radio inline input_radio" id="issueFormSendEmailsNo"> <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"issueFormSendEmailsNo",'main_label'=>false,'after_text'=>'')); $_block_repeat=true; echo smarty_block_label(array('for'=>"issueFormSendEmailsNo",'main_label'=>false,'after_text'=>''), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Don't send emails, but mark invoice as issued<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"issueFormSendEmailsNo",'main_label'=>false,'after_text'=>''), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>

        <script type="text/javascript">
          $("#issue_invoice .send_mails_radio").click(function(){
            if($(this).val() == '1') {
              $("#select_invoice_recipients").slideDown();
            } else {
              $("#select_invoice_recipients").slideUp();
            }//if
          });
        </script>
      </div>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_fields(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Issue Invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getIssueUrl(),'method'=>'post'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div><?php }} ?>