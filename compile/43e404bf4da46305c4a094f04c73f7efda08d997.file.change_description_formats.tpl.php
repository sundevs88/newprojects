<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:38:08
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoicing_settings_admin/change_description_formats.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2290836935742eba0da3ed4-51543845%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '43e404bf4da46305c4a094f04c73f7efda08d997' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoicing_settings_admin/change_description_formats.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2290836935742eba0da3ed4-51543845',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'formats_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742eba0f118c3_95161233',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742eba0f118c3_95161233')) {function content_5742eba0f118c3_95161233($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap_fields')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_fields.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_pattern_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.pattern_field.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>Router::assemble('invoicing_settings_change_description_formats'))); $_block_repeat=true; echo smarty_block_form(array('action'=>Router::assemble('invoicing_settings_change_description_formats')), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_fields', array()); $_block_repeat=true; echo smarty_block_wrap_fields(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_item_formats')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php echo smarty_function_pattern_field(array('name'=>"formats[description_format_grouped_by_task]",'value'=>$_smarty_tpl->tpl_vars['formats_data']->value['description_format_grouped_by_task'],'variables'=>'job_type,task_id,task_summary,project_name','default_format'=>@Invoices::DEFAULT_TASK_DESCRIPTION_FORMAT,'label'=>'When Records are Grouped by Task'),$_smarty_tpl);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_item_formats')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php echo smarty_function_pattern_field(array('name'=>"formats[description_format_grouped_by_project]",'value'=>$_smarty_tpl->tpl_vars['formats_data']->value['description_format_grouped_by_project'],'variables'=>'name,client,category','default_format'=>@Invoices::DEFAULT_PROJECT_DESCRIPTION_FORMAT,'label'=>'When Records are Grouped by Project'),$_smarty_tpl);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_item_formats')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php echo smarty_function_pattern_field(array('name'=>"formats[description_format_grouped_by_job_type]",'value'=>$_smarty_tpl->tpl_vars['formats_data']->value['description_format_grouped_by_job_type'],'variables'=>'job_type','default_format'=>@Invoices::DEFAULT_JOB_TYPE_DESCRIPTION_FORMAT,'label'=>'When Records are Grouped by Job Type'),$_smarty_tpl);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'invoice_item_formats')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php echo smarty_function_pattern_field(array('name'=>"formats[description_format_separate_items]",'value'=>$_smarty_tpl->tpl_vars['formats_data']->value['description_format_separate_items'],'variables'=>'job_type_or_category,record_summary,record_date,parent_task_or_project,project_name','default_format'=>@Invoices::DEFAULT_INDIVIDUAL_DESCRIPTION_FORMAT,'label'=>'When Records are Displayed as Individual Invoice Items'),$_smarty_tpl);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'invoice_item_formats'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'record_summary_transoformations')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'record_summary_transoformations'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Transform Non-Empty Record Summary<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <select name="formats[first_record_summary_transformation]">
        <option value=""><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Don't Change<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PUT_IN_PARENTHESES,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['first_record_summary_transformation']==@Invoices::SUMMARY_PUT_IN_PARENTHESES){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Put in between '(' and ')'<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PREFIX_WITH_DASH,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['first_record_summary_transformation']==@Invoices::SUMMARY_PREFIX_WITH_DASH){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prefix with ' - '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_SUFIX_WITH_DASH,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['first_record_summary_transformation']==@Invoices::SUMMARY_SUFIX_WITH_DASH){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sufix with ' - '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PREFIX_WITH_COLON,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['first_record_summary_transformation']==@Invoices::SUMMARY_PREFIX_WITH_COLON){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prefix with ' :'<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_SUFIX_WITH_COLON,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['first_record_summary_transformation']==@Invoices::SUMMARY_SUFIX_WITH_COLON){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sufix with ': '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
      </select>
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
and then<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <select name="formats[second_record_summary_transformation]">
        <option><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Don't Change<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PUT_IN_PARENTHESES,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['second_record_summary_transformation']==@Invoices::SUMMARY_PUT_IN_PARENTHESES){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Put in between '(' and ')'<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PREFIX_WITH_DASH,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['second_record_summary_transformation']==@Invoices::SUMMARY_PREFIX_WITH_DASH){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prefix with ' - '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_SUFIX_WITH_DASH,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['second_record_summary_transformation']==@Invoices::SUMMARY_SUFIX_WITH_DASH){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sufix with ' - '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_PREFIX_WITH_COLON,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['second_record_summary_transformation']==@Invoices::SUMMARY_PREFIX_WITH_COLON){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prefix with ' :'<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
        <option value="<?php echo clean(@Invoices::SUMMARY_SUFIX_WITH_COLON,$_smarty_tpl);?>
" <?php if ($_smarty_tpl->tpl_vars['formats_data']->value['second_record_summary_transformation']==@Invoices::SUMMARY_SUFIX_WITH_COLON){?>selected<?php }?>><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sufix with ': '<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</option>
      </select>
      <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Record summary can be empty, so you should prepare format that works for both cases: when summary is available and when it is not present<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'record_summary_transoformations'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_fields(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>Router::assemble('invoicing_settings_change_description_formats')), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>