<?php /* Smarty version Smarty-3.1.12, created on 2016-04-29 08:14:01
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_expenses/add_expense.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1770600469572317c91a3934-12601644%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a61532c931c213a99c56c61a8a19e8b8357fae8' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_expenses/add_expense.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1770600469572317c91a3934-12601644',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_tracking_object' => 0,
    'expense_data' => 0,
    'can_track_for_others' => 0,
    'active_project' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_572317c9276c82_91707242',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572317c9276c82_91707242')) {function content_572317c9276c82_91707242($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_money_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/function.money_field.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_select_expense_category')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_expense_category.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_project_user')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project_user.php';
if (!is_callable('smarty_function_select_billable_status')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_billable_status.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log Expense<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="add_expense">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddExpenseUrl(),'method'=>'post')); $_block_repeat=true; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddExpenseUrl(),'method'=>'post'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="fields_wrapper">
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'value')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'value'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	    	<?php echo smarty_function_money_field(array('name'=>'expense[value]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['value'],'label'=>'Amount','required'=>true),$_smarty_tpl);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
in<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_expense_category(array('name'=>'expense[category_id]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['category_id']),$_smarty_tpl);?>

	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'value'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'summary')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'summary'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	      <?php echo smarty_function_text_field(array('name'=>'expense[summary]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['summary'],'label'=>'Summary'),$_smarty_tpl);?>

	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'summary'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	  
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'record_date')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'record_date'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	    	<?php echo smarty_function_select_date(array('name'=>'expense[record_date]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['record_date'],'label'=>'Date','required'=>true),$_smarty_tpl);?>

	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'record_date'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


      <?php if ($_smarty_tpl->tpl_vars['can_track_for_others']->value){?>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'user_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'user_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_select_project_user(array('name'=>'expense[user_id]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['user_id'],'project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'optional'=>false,'required'=>true,'label'=>'User'),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'user_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php }else{ ?>
        <input type="hidden" name="expense[user_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['expense_data']->value['user_id'],$_smarty_tpl);?>
"/>
      <?php }?>
	    
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'billable_status')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'billable_status'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	      <?php echo smarty_function_select_billable_status(array('name'=>'expense[billable_status]','value'=>$_smarty_tpl->tpl_vars['expense_data']->value['billable_status'],'label'=>'Is Billable?'),$_smarty_tpl);?>

	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'billable_status'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
    
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    	<?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log Expense<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>$_smarty_tpl->tpl_vars['active_tracking_object']->value->tracking()->getAddExpenseUrl(),'method'=>'post'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div><?php }} ?>