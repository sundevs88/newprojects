<?php /* Smarty version Smarty-3.1.12, created on 2016-05-09 00:54:31
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_time_records/_time_record_form_row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1253222062572fdfc775f9f8-73099776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f0b30c004a82a6744c18a2aaea4f5718069c0b7' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/object_tracking_time_records/_time_record_form_row.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1253222062572fdfc775f9f8-73099776',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_project_time_form_row_record' => 0,
    '_project_time_form_id' => 0,
    'active_project' => 0,
    'can_track_for_others' => 0,
    'time_record_data' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_572fdfc78958b7_22742081',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572fdfc78958b7_22742081')) {function content_572fdfc78958b7_22742081($_smarty_tpl) {?><?php if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_select_project_user')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_project_user.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_select_job_type')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_job_type.php';
if (!is_callable('smarty_function_select_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_date.php';
if (!is_callable('smarty_function_select_billable_status')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/helpers/function.select_billable_status.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php if ($_smarty_tpl->tpl_vars['_project_time_form_row_record']->value instanceof TimeRecord&&$_smarty_tpl->tpl_vars['_project_time_form_row_record']->value->isLoaded()){?>
<tr class="item_form time_record_form edit_time_record" id="<?php echo clean($_smarty_tpl->tpl_vars['_project_time_form_id']->value,$_smarty_tpl);?>
">
<?php }else{ ?>
<tr class="item_form time_record_form new_time_record" id="<?php echo clean($_smarty_tpl->tpl_vars['_project_time_form_id']->value,$_smarty_tpl);?>
">
<?php }?>
  <td colspan="5">
    <?php if ($_smarty_tpl->tpl_vars['_project_time_form_row_record']->value instanceof TimeRecord&&$_smarty_tpl->tpl_vars['_project_time_form_row_record']->value->isLoaded()){?>
    <form action="<?php echo clean($_smarty_tpl->tpl_vars['_project_time_form_row_record']->value->getEditUrl(),$_smarty_tpl);?>
" method="post" class="time_record_form">
    <?php }else{ ?>
    <form action="<?php echo clean($_smarty_tpl->tpl_vars['active_project']->value->tracking()->getAddTimeUrl(),$_smarty_tpl);?>
" method="post" class="time_record_form">
    <?php }?>
    
      <div class="item_attributes">

        <?php if ($_smarty_tpl->tpl_vars['can_track_for_others']->value){?>
          <div class="item_attribute time_record_user">
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_user",'required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_user",'required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_user",'required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_project_user(array('name'=>'time[user_id]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['user_id'],'project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'optional'=>false,'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_user"),$_smarty_tpl);?>

          </div>
        <?php }else{ ?>
          <input type="hidden" name="time[user_id]" value="<?php echo clean($_smarty_tpl->tpl_vars['time_record_data']->value['user_id'],$_smarty_tpl);?>
"/>
        <?php }?>
      
        <div class="item_attribute item_value_wrapper time_record_value">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_value",'required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_value",'required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hours<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_value",'required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_text_field(array('name'=>'time[value]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['value'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_value"),$_smarty_tpl);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
of<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_job_type(array('name'=>'time[job_type_id]','id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_job_type_id",'value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['job_type_id'],'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'required'=>true),$_smarty_tpl);?>

        </div>
        
        <div class="item_attribute time_record_date">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_date",'required'=>'yes')); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_date",'required'=>'yes'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_date",'required'=>'yes'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_date(array('name'=>'time[record_date]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['record_date'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_date"),$_smarty_tpl);?>

        </div>
        
        <div class="item_attribute item_summary_wrapper item_summary time_record_summary">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_summary")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_summary"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Summary<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_summary"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_text_field(array('name'=>'time[summary]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['summary'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_summary"),$_smarty_tpl);?>

        </div>
        
        <div class="item_attribute time_record_billable">
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_billable")); $_block_repeat=true; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_billable"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"(".((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value).")_billable"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php echo smarty_function_select_billable_status(array('name'=>'time[billable_status]','value'=>$_smarty_tpl->tpl_vars['time_record_data']->value['billable_status'],'id'=>((string)$_smarty_tpl->tpl_vars['_project_time_form_id']->value)."_billable"),$_smarty_tpl);?>

        </div>
      </div>
      
      <p class="details"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Value can be inserted in decimal (5.25) and HH:MM (5:15) format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
      
      <div class="item_form_buttons">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
or<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 <a href="#" class="item_form_cancel"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a>
      </div>
    </form>
  </td>
</tr><?php }} ?>