<?php /* Smarty version Smarty-3.1.12, created on 2016-04-29 08:12:16
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_hourly_rates/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9887108135723176025af62-88566976%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a44370854640cbd485b25e191a072adb2f4502d8' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/project_hourly_rates/index.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9887108135723176025af62-88566976',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'job_types' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5723176031aaf4_95252715',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5723176031aaf4_95252715')) {function content_5723176031aaf4_95252715($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hourly Rates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hourly Rates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"paged_objects_list",'module'=>"environment"),$_smarty_tpl);?>


<div id="project_hourly_rates"></div>

<script type="text/javascript">
  $('#project_hourly_rates').each(function() {
    var wrapper = $(this);

    var project_currency = <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getCurrency());?>
;

    wrapper.pagedObjectsList({
      'paged' : false,
      'items' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['job_types']->value);?>
,
      'list_items_are' : 'tr',
      'list_item_attributes' : { 'class' : 'job_type' },
      'columns' : {
        'name' : App.lang('Job Type'),
        'hourly_rate' : App.lang('Hourly Rate'),
        'options' : ''
      },
      'empty_message' : App.lang('There are no job types defined'),
      'listen' : 'job_type',
      'listen_constraint' : function(event, job_type) {
        if(event.type == 'job_type_updated') {
          return wrapper.find('tr[list_item_id=' + job_type.id + ']').length ? true : false;
        } // if
      },
      'on_add_item' : function(item) {
        var job_type = $(this);

        if(!item['is_active']) {
          job_type.addClass('archived');
        } // if

        job_type.append(
          '<td class="name"></td>' +
          '<td class="hourly_rate"></td>' +
          '<td class="options"></td>'
        );

        job_type.find('td.name').html(App.clean(item['name']));

        if(!item['custom_hourly_rate']) {
          job_type.find('td.hourly_rate').html(App.moneyFormat(item['default_hourly_rate'], project_currency, null, true));
        } else {
          job_type.addClass('custom_hourly_rate');
          job_type.find('td.hourly_rate').html(App.moneyFormat(item['custom_hourly_rate'], project_currency, null, true));
        } // if

        var options_cell = job_type.find('td.options');

        options_cell.append('<a href="' + item['urls']['edit'] + '" class="edit_project_hourly_rate" title="' + App.lang('Edit Project Hourly Rate') + '"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="' + App.lang('Edit') + '" /></a>').find('a.edit_project_hourly_rate').flyoutForm({
          'title' : App.lang('Hourly Rate'),
          'success_event' : 'job_type_updated'
        });
      }
    });

    // Job type archived
    App.Wireframe.Events.bind('job_type_archived', function(event, job_type) {
      var current_item = wrapper.find('tr[list_item_id=' + job_type.id + ']');

      if(current_item.length) {
        current_item.addClass('archived');
      } else {
        App.Wireframe.Events.trigger('job_type_deleted.content', [job_type]);
      } // if
    });

    // Job type unarchived
    App.Wireframe.Events.bind('job_type_unarchived', function(event, job_type) {
      var current_item = wrapper.find('tr[list_item_id=' + job_type.id + ']');

      if(current_item.length) {
        current_item.removeClass('archived');
      } else {
        App.Wireframe.Events.trigger('job_type_created.content', [job_type]);
      } // if
    });
  });
</script><?php }} ?>