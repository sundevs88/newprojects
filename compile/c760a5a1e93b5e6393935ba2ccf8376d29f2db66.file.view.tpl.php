<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 04:41:26
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/users/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:199288395856d7c0760421a5-72618769%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c760a5a1e93b5e6393935ba2ccf8376d29f2db66' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/users/view.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '199288395856d7c0760421a5-72618769',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_user' => 0,
    'logged_user' => 0,
    'request' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7c076086e79_94160631',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7c076086e79_94160631')) {function content_56d7c076086e79_94160631($_smarty_tpl) {?><?php if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_object')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.object.php';
if (!is_callable('smarty_function_inline_tabs')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.inline_tabs.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Profile<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('object', array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value)); $_block_repeat=true; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="wireframe_content_wrapper">
    <?php echo smarty_function_inline_tabs(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value),$_smarty_tpl);?>

  </div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_user']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
  App.Wireframe.Events.bind('user_updated.<?php echo clean($_smarty_tpl->tpl_vars['request']->value->getEventScope(),$_smarty_tpl);?>
', function (event, user) {
    if(user['class'] == 'User' && user.id == '<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->getId(),$_smarty_tpl);?>
') {
    	// update avatar on the profile page
      $('#user_page_' + user.id + ' #select_user_icon .properties_icon').attr('src', user.avatar.photo);

      // if user is changing their own avatar, update the image at the bottom left corner
      if ('<?php echo clean($_smarty_tpl->tpl_vars['active_user']->value->getId(),$_smarty_tpl);?>
' == '<?php echo clean($_smarty_tpl->tpl_vars['logged_user']->value->getId(),$_smarty_tpl);?>
') {
        $('#menu_item_profile img').attr('src', user.avatar.large);
        $('#menu_item_profile span.label').html(user.display_name);
      } // if
    } // if
  });
</script><?php }} ?>