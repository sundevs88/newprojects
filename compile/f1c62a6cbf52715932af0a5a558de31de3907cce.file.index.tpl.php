<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:19:16
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/printer/project/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1613656605742e734ceb947-24283199%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f1c62a6cbf52715932af0a5a558de31de3907cce' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/printer/project/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1613656605742e734ceb947-24283199',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_project' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742e734d53cf4_82871873',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742e734d53cf4_82871873')) {function content_5742e734d53cf4_82871873($_smarty_tpl) {?><?php if (!is_callable('smarty_block_object')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.object.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_people_on_project')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.people_on_project.php';
if (!is_callable('smarty_function_project_milestone')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.project_milestone.php';
if (!is_callable('smarty_function_activity_logs_in')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/activity_logs/helpers/function.activity_logs_in.php';
?><div id="print_container">
<?php $_smarty_tpl->smarty->_tag_stack[] = array('object', array('object'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value)); $_block_repeat=true; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="wireframe_content_wrapper">
    <div class="object_body with_shadow">
      <div class="object_content_wrapper"><div class="object_body_content formatted_content">
        <?php if ($_smarty_tpl->tpl_vars['active_project']->value->inspector()->hasBody()){?>
          <?php echo $_smarty_tpl->tpl_vars['active_project']->value->inspector()->getBody();?>

        <?php }else{ ?>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
No description provided<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php }?>
      </div>
      </div>
      
    </div>
  </div>


  <div class="wireframe_content_wrapper"><?php echo smarty_function_people_on_project(array('project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</div>
    <div class="wireframe_content_wrapper"><?php echo smarty_function_project_milestone(array('project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</div>
    <div class="wireframe_content_wrapper"><?php echo smarty_function_activity_logs_in(array('in'=>$_smarty_tpl->tpl_vars['active_project']->value,'id'=>'recent_activities','user'=>$_smarty_tpl->tpl_vars['logged_user']->value),$_smarty_tpl);?>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_object(array('object'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>
<?php }} ?>