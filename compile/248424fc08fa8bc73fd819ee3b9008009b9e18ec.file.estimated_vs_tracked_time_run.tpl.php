<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 15:09:25
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/estimated_vs_tracked_time/estimated_vs_tracked_time_run.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3839800315728bf25409860-23014750%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '248424fc08fa8bc73fd819ee3b9008009b9e18ec' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/default/estimated_vs_tracked_time/estimated_vs_tracked_time_run.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3839800315728bf25409860-23014750',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'assignments' => 0,
    'labels' => 0,
    'project_slugs' => 0,
    'task_url' => 0,
    'job_types' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728bf25479a04_79250027',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728bf25479a04_79250027')) {function content_5728bf25479a04_79250027($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_modifier_map')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.map.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Estimated vs Tracked Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Estimated vs Tracked Time Report<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"assignments_list",'module'=>"system"),$_smarty_tpl);?>


<div id="estimated_vs_tracked_time_report_result"></div>

<script type="text/javascript">
  $('#estimated_vs_tracked_time_report_result').assignmentsList({
    'assignments' : <?php echo smarty_modifier_map($_smarty_tpl->tpl_vars['assignments']->value);?>
,
    'labels' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['labels']->value);?>
,
    'project_slugs' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['project_slugs']->value);?>
,
    'task_url' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['task_url']->value);?>
,
    'job_types' : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['job_types']->value);?>
,
    'additional_column_1' : <?php echo smarty_modifier_json(@AssignmentFilter::ADDITIONAL_COLUMN_ESTIMATED_TIME);?>
,
    'additional_column_2' : <?php echo smarty_modifier_json(@AssignmentFilter::ADDITIONAL_COLUMN_TRACKED_TIME);?>
,
    'show_additional_column_label_in_header' : true,
    'show_no_assignments_message' : true,
    'no_assignments_message' : App.lang("There are no tasks in selected project"),
    'on_group_ready' : function(all_settings) {
      var records_table = $(this).find('table.assignments');

      if(records_table.length) {
        var estimated_time_total = 0;
        var tracked_time_total = 0;

        records_table.find('tr.assignment.task').each(function() {
          var row = $(this);

          var estimated_time = App.parseNumeric(row.find('td.additional_column_1').attr('estimated_time'));
          var tracked_time = App.parseNumeric(row.find('td.additional_column_2').attr('tracked_time'));

          if(!isNaN(estimated_time)) {
            estimated_time_total += estimated_time;
          } // if

          if(!isNaN(tracked_time)) {
            tracked_time_total += tracked_time;
          } // if
        });

        records_table.append('<tfoot>' +
          '<tr>' +
            '<td class="right" colspan="2">' + App.lang('Total') + ':</td>' +
            '<td class="center">' + App.hoursFormat(estimated_time_total) + '</td>' +
            '<td class="center">' + App.hoursFormat(tracked_time_total) + '</td>' +
          '</tr>' +
        '</tfoot>');
      } // if
    }
  });
</script><?php }} ?>