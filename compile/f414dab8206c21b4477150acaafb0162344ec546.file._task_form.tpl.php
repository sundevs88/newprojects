<?php /* Smarty version Smarty-3.1.12, created on 2016-05-29 01:49:16
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/phone/tasks/_task_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:723425634574a4a9c9b40f9-99104565%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f414dab8206c21b4477150acaafb0162344ec546' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/views/phone/tasks/_task_form.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '723425634574a4a9c9b40f9-99104565',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'task_data' => 0,
    'active_project' => 0,
    'logged_user' => 0,
    'active_task' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_574a4a9ca3f709_74639657',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574a4a9ca3f709_74639657')) {function content_574a4a9ca3f709_74639657($_smarty_tpl) {?><?php if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_function_text_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.text_field.php';
if (!is_callable('smarty_block_editor_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/visual_editor/helpers/block.editor_field.php';
if (!is_callable('smarty_function_select_task_category')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tasks/helpers/function.select_task_category.php';
if (!is_callable('smarty_function_select_milestone')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/helpers/function.select_milestone.php';
if (!is_callable('smarty_function_select_priority')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/complete/helpers/function.select_priority.php';
if (!is_callable('smarty_function_select_visibility')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_visibility.php';
if (!is_callable('smarty_function_select_due_on')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.select_due_on.php';
if (!is_callable('smarty_function_select_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/labels/helpers/function.select_label.php';
if (!is_callable('smarty_function_select_assignees')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/assignees/helpers/function.select_assignees.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'name')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'name'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php echo smarty_function_text_field(array('name'=>"task[name]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['name'],'id'=>'taskSummary','required'=>true,'label'=>'Summary'),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'name'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'body')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'body'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php $_smarty_tpl->smarty->_tag_stack[] = array('editor_field', array('name'=>"task[body]",'label'=>'Description')); $_block_repeat=true; echo smarty_block_editor_field(array('name'=>"task[body]",'label'=>'Description'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['task_data']->value['body'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_editor_field(array('name'=>"task[body]",'label'=>'Description'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'body'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'category_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'category_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php echo smarty_function_select_task_category(array('name'=>"task[category_id]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['category_id'],'parent'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'label'=>'Category'),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'category_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php if ($_smarty_tpl->tpl_vars['logged_user']->value->canSeeMilestones($_smarty_tpl->tpl_vars['active_project']->value)){?>
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'milestone_id')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'milestone_id'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php echo smarty_function_select_milestone(array('name'=>"task[milestone_id]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['milestone_id'],'project'=>$_smarty_tpl->tpl_vars['active_project']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'label'=>'Milestone'),$_smarty_tpl);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'milestone_id'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php }?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'priority')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'priority'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php echo smarty_function_select_priority(array('name'=>"task[priority]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['priority'],'label'=>'Priority'),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'priority'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php if ($_smarty_tpl->tpl_vars['logged_user']->value->canSeePrivate()){?>
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'visibility')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'visibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php echo smarty_function_select_visibility(array('name'=>"task[visibility]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['visibility'],'label'=>'Visibility','object'=>$_smarty_tpl->tpl_vars['active_task']->value),$_smarty_tpl);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'visibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php }else{ ?>
  <input type="hidden" name="task[visibility]" value="1" />
<?php }?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'due_on')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'due_on'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php echo smarty_function_select_due_on(array('name'=>"task[due_on]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['due_on'],'label'=>'Due On'),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'due_on'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'label')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'label'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php echo smarty_function_select_label(array('name'=>"task[label_id]",'value'=>$_smarty_tpl->tpl_vars['task_data']->value['label_id'],'id'=>"taskLabel",'type'=>'AssignmentLabel','user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'label'=>'Label'),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'label'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'assignees')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'assignees'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	<?php echo smarty_function_select_assignees(array('name'=>'task','value'=>$_smarty_tpl->tpl_vars['task_data']->value['assignee_id'],'exclude'=>$_smarty_tpl->tpl_vars['task_data']->value['exclude_ids'],'other_assignees'=>$_smarty_tpl->tpl_vars['task_data']->value['other_assignees'],'object'=>$_smarty_tpl->tpl_vars['active_task']->value,'user'=>$_smarty_tpl->tpl_vars['logged_user']->value,'choose_responsible'=>true,'choose_subscribers'=>$_smarty_tpl->tpl_vars['active_task']->value->isNew(),'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'assignees'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<script type="text/javascript">
	$(document).ready(function() {
		App.Wireframe.SelectBox.init();
		App.Wireframe.DateBox.init();
	});
</script><?php }} ?>