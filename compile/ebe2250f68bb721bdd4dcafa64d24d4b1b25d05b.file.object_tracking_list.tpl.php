<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 18:09:24
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/phone/object_tracking/object_tracking_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:142307122356d9cf5467e8f4-44900014%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebe2250f68bb721bdd4dcafa64d24d4b1b25d05b' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/phone/object_tracking/object_tracking_list.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '142307122356d9cf5467e8f4-44900014',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'formatted_items' => 0,
    'items' => 0,
    'record_date' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d9cf546e8956_01544668',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d9cf546e8956_01544668')) {function content_56d9cf546e8956_01544668($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="project_time_expenses">
	<?php if ($_smarty_tpl->tpl_vars['formatted_items']->value){?>
	  <?php  $_smarty_tpl->tpl_vars['items'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['items']->_loop = false;
 $_smarty_tpl->tpl_vars['record_date'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['formatted_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['items']->key => $_smarty_tpl->tpl_vars['items']->value){
$_smarty_tpl->tpl_vars['items']->_loop = true;
 $_smarty_tpl->tpl_vars['record_date']->value = $_smarty_tpl->tpl_vars['items']->key;
?>
	  	<?php if (is_foreachable($_smarty_tpl->tpl_vars['items']->value)){?>
	  		<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
	  		<li data-role="list-divider"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['record_date']->value),$_smarty_tpl);?>
</li>
		  	<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			    <?php if ($_smarty_tpl->tpl_vars['item']->value['class']=='TimeRecord'){?>
			    	<li>
			    		<a href="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['urls']['view'],$_smarty_tpl);?>
"><img class="ui-li-icon" src="<?php echo smarty_function_image_url(array('name'=>'icons/32x32/time-entry.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" alt="Time Record"><h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('time_record_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name'])); $_block_repeat=true; echo smarty_block_lang(array('time_record_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:time_record_value h, :user_name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('time_record_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3><p><?php echo clean($_smarty_tpl->tpl_vars['item']->value['summary'],$_smarty_tpl);?>
</p></a>
			    	</li>
			    <?php }else{ ?>
			    	<li>
			    		<a href="<?php echo clean($_smarty_tpl->tpl_vars['item']->value['urls']['view'],$_smarty_tpl);?>
"><img class="ui-li-icon" src="<?php echo smarty_function_image_url(array('name'=>'icons/32x32/expense.png','module'=>@TRACKING_MODULE),$_smarty_tpl);?>
" alt="Expense"><h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('expense_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name'])); $_block_repeat=true; echo smarty_block_lang(array('expense_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
$ :expense_value, :user_name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('expense_value'=>$_smarty_tpl->tpl_vars['item']->value['value'],'user_name'=>$_smarty_tpl->tpl_vars['item']->value['user_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3><p><?php echo clean($_smarty_tpl->tpl_vars['item']->value['summary'],$_smarty_tpl);?>
</p></a>
			    	</li>
			    <?php }?>
		  	<?php } ?>
		  	</ul>
	  	<?php }?>
  	<?php } ?>
  <?php }else{ ?>
  	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
			<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no time nor expenses logged for this task<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
		</ul>
	<?php }?>
</div><?php }} ?>