<?php /* Smarty version Smarty-3.1.12, created on 2016-03-04 06:09:07
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/views/default/fw_authentication/reset_password.tpl" */ ?>
<?php /*%%SmartyHeaderCode:169899273856d926835aa5b9-23380070%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6dffe84068e105ac9dec8c2a4d180fc424c1586c' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/views/default/fw_authentication/reset_password.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '169899273856d926835aa5b9-23380070',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'owner_company' => 0,
    'auto_focus' => 0,
    'user' => 0,
    'reset_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d92683668f89_14289527',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d92683668f89_14289527')) {function content_56d92683668f89_14289527($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_function_brand')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.brand.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_password_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.password_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.link.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reset Password<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<div id="login_company_logo">
  <img src="<?php echo smarty_function_brand(array('what'=>'logo'),$_smarty_tpl);?>
" alt="<?php echo clean($_smarty_tpl->tpl_vars['owner_company']->value->getName(),$_smarty_tpl);?>
 logo" />
</div>

<div id="auth_dialog_container">
  <div id="auth_dialog_container_inner">
    <div id="auth_dialog">
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>'post','autofocus'=>$_smarty_tpl->tpl_vars['auto_focus']->value,'show_errors'=>false)); $_block_repeat=true; echo smarty_block_form(array('method'=>'post','autofocus'=>$_smarty_tpl->tpl_vars['auto_focus']->value,'show_errors'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['user']->value->getDisplayName())); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['user']->value->getDisplayName()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Use the form below to reset password for :name's account<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['user']->value->getDisplayName()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</p>
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'passwords','class'=>"auth_elements")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'passwords','class'=>"auth_elements"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'password')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'password'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'resetFormPassword')); $_block_repeat=true; echo smarty_block_label(array('for'=>'resetFormPassword'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
New Password<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'resetFormPassword'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_password_field(array('name'=>'reset[password]','value'=>$_smarty_tpl->tpl_vars['reset_data']->value['password'],'id'=>'resetFormPasswordA','tabindex'=>1),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'password'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'password_a')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'password_a'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'resetFormPasswordA')); $_block_repeat=true; echo smarty_block_label(array('for'=>'resetFormPasswordA'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Repeat<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'resetFormPasswordA'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_password_field(array('name'=>'reset[password_a]','value'=>$_smarty_tpl->tpl_vars['reset_data']->value['password_a'],'id'=>'resetFormPasswordA','tabindex'=>2),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'password_a'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'passwords','class'=>"auth_elements"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>Router::assemble('login'),'class'=>'forgot_password_link')); $_block_repeat=true; echo smarty_block_link(array('href'=>Router::assemble('login'),'class'=>'forgot_password_link'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Back to Login Form<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>Router::assemble('login'),'class'=>'forgot_password_link'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

        <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array('tabindex'=>4)); $_block_repeat=true; echo smarty_block_submit(array('tabindex'=>4), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reset<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array('tabindex'=>4), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <div class="clear"></div>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>'post','autofocus'=>$_smarty_tpl->tpl_vars['auto_focus']->value,'show_errors'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
  </div>
</div><?php }} ?>