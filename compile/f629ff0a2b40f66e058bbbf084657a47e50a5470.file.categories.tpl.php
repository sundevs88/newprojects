<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 08:39:42
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/categories/views/default/fw_categories/categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:212760597656da9b4eccb1b7-00228769%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f629ff0a2b40f66e058bbbf084657a47e50a5470' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/categories/views/default/fw_categories/categories.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212760597656da9b4eccb1b7-00228769',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'categories' => 0,
    'category' => 0,
    'logged_user' => 0,
    'add_category_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56da9b4ed27af8_93723935',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56da9b4ed27af8_93723935')) {function content_56da9b4ed27af8_93723935($_smarty_tpl) {?><?php if (!is_callable('smarty_block_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.link.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_function_link_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.link_button.php';
?><div class="manage_categories">
  <div class="manage_categories_categories">
    <table class="common" style="display: none">
      <tbody>
    <?php if (is_foreachable($_smarty_tpl->tpl_vars['categories']->value)){?>
      <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
        <tr class="category" category_id="<?php echo clean($_smarty_tpl->tpl_vars['category']->value->getId(),$_smarty_tpl);?>
">
          <td class="name"><?php echo clean($_smarty_tpl->tpl_vars['category']->value->getName(),$_smarty_tpl);?>
</td>
          <td class="options"><?php if ($_smarty_tpl->tpl_vars['category']->value->canEdit($_smarty_tpl->tpl_vars['logged_user']->value)){?><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>$_smarty_tpl->tpl_vars['category']->value->getEditUrl(),'title'=>"Rename Category",'class'=>'rename_category')); $_block_repeat=true; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getEditUrl(),'title'=>"Rename Category",'class'=>'rename_category'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/edit.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="edit" /><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getEditUrl(),'title'=>"Rename Category",'class'=>'rename_category'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }?><?php if ($_smarty_tpl->tpl_vars['category']->value->canDelete($_smarty_tpl->tpl_vars['logged_user']->value)){?><?php $_smarty_tpl->smarty->_tag_stack[] = array('link', array('href'=>$_smarty_tpl->tpl_vars['category']->value->getDeleteUrl(),'class'=>'delete_category','title'=>"Delete Category")); $_block_repeat=true; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getDeleteUrl(),'class'=>'delete_category','title'=>"Delete Category"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<img src="<?php echo smarty_function_image_url(array('name'=>"icons/12x12/delete.png",'module'=>@ENVIRONMENT_FRAMEWORK),$_smarty_tpl);?>
" alt="" /><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_link(array('href'=>$_smarty_tpl->tpl_vars['category']->value->getDeleteUrl(),'class'=>'delete_category','title'=>"Delete Category"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }?></td>
        </tr>
      <?php } ?>
    <?php }?>
      </tbody>
    </table>
    <p class="empty_page" style="display: none"><span class="inner"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no categories in this section!<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></p>
  </div>
  
  <div class="new_category_button">
    <?php echo smarty_function_link_button(array('href'=>$_smarty_tpl->tpl_vars['add_category_url']->value,'label'=>"New Category",'icon_class'=>'button_add','class'=>'new_category'),$_smarty_tpl);?>

  </div>
</div><?php }} ?>