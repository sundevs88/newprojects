<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 04:41:01
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/_select_project_tabs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29939977156d7c05d0e86e2-36682521%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d0d5353145c6a431e3db637e01f62ef30d2bab1' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/_select_project_tabs.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29939977156d7c05d0e86e2-36682521',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_select_project_tabs_id' => 0,
    '_select_project_tabs_value' => 0,
    'value' => 0,
    '_select_project_tabs_name' => 0,
    '_select_project_tabs' => 0,
    'project_tab_name' => 0,
    'project_tab_text' => 0,
    'logged_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7c05d1371b7_14148913',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7c05d1371b7_14148913')) {function content_56d7c05d1371b7_14148913($_smarty_tpl) {?><?php if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php echo smarty_function_use_widget(array('name'=>"select_project_tabs",'module'=>"system"),$_smarty_tpl);?>


<div id="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value,$_smarty_tpl);?>
" class="select_project_tabs">
  <table>
    <tr>
      <td>
        <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Enabled Tabs<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</p>
        <ul id="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value,$_smarty_tpl);?>
_selected">
        <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['_select_project_tabs_value']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
?>
          <?php if ($_smarty_tpl->tpl_vars['value']->value=='-'){?>
            <li class="separator sortable">
              <input type="checkbox" name="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_name']->value,$_smarty_tpl);?>
[]" value="-" class="inline" checked="checked" /> ------------------------
            </li>
          <?php }else{ ?>
            <li class="tab sortable">
              <input type="checkbox" name="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_name']->value,$_smarty_tpl);?>
[]" value="<?php echo clean($_smarty_tpl->tpl_vars['value']->value,$_smarty_tpl);?>
" class="inline" checked="checked" /> <?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs']->value[$_smarty_tpl->tpl_vars['value']->value],$_smarty_tpl);?>

            </li>
          <?php }?>
        <?php } ?>
        </ul>
        
        <p><a href="#" class="button_add" id="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value,$_smarty_tpl);?>
_add_separator"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add Separator<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</a></p>
      </td>
      <td>
        <div id="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value,$_smarty_tpl);?>
_available_wrapper">
          <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Disabled Tabs<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</p>
          <ul id="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value,$_smarty_tpl);?>
_available">
          <?php  $_smarty_tpl->tpl_vars['project_tab_text'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['project_tab_text']->_loop = false;
 $_smarty_tpl->tpl_vars['project_tab_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_select_project_tabs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['project_tab_text']->key => $_smarty_tpl->tpl_vars['project_tab_text']->value){
$_smarty_tpl->tpl_vars['project_tab_text']->_loop = true;
 $_smarty_tpl->tpl_vars['project_tab_name']->value = $_smarty_tpl->tpl_vars['project_tab_text']->key;
?>
            <?php if (!in_array($_smarty_tpl->tpl_vars['project_tab_name']->value,$_smarty_tpl->tpl_vars['_select_project_tabs_value']->value)){?>
              <li class="tab sortable">
                <input type="checkbox" name="<?php echo clean($_smarty_tpl->tpl_vars['_select_project_tabs_name']->value,$_smarty_tpl);?>
[]" value="<?php echo clean($_smarty_tpl->tpl_vars['project_tab_name']->value,$_smarty_tpl);?>
" class="inline" /> <?php echo clean($_smarty_tpl->tpl_vars['project_tab_text']->value,$_smarty_tpl);?>

              </li>
            <?php }?>
          <?php } ?>
          </ul>
        </div>
      </td>
    </tr>
  </table>
  
  <?php if (!AngieApplication::isOndemand()&&$_smarty_tpl->tpl_vars['logged_user']->value->isAdministrator()){?>
  <p class="aid"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('modules_url'=>Router::assemble('modules_admin'))); $_block_repeat=true; echo smarty_block_lang(array('modules_url'=>Router::assemble('modules_admin')), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Note: Missing a feature? Please check <a href=":modules_url">the list of installed modules</a><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('modules_url'=>Router::assemble('modules_admin')), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
.</p>
  <?php }?>
</div>

<script type="text/javascript">
  App.widgets.SelectProjectTabs.init(<?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['_select_project_tabs_id']->value);?>
, <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['_select_project_tabs_name']->value);?>
);
</script><?php }} ?>