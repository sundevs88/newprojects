<?php /* Smarty version Smarty-3.1.12, created on 2016-03-27 06:47:53
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/milestones/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:41084296056f78219bac788-76427774%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '251e089a5354b63f1b64296be96f1bdbc63dea3e' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/default/milestones/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '41084296056f78219bac788-76427774',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'flyout' => 0,
    'active_project' => 0,
    'day_width' => 0,
    'milestones' => 0,
    'diagram_images' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56f78219c090a8_38036817',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f78219c090a8_38036817')) {function content_56f78219c090a8_38036817($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Milestones<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<?php if ($_smarty_tpl->tpl_vars['flyout']->value){?><div class="timeline_flyout_wrapper" style="height: 450px;"><?php }?>
<div id="milestones_diagram"></div>
<?php if ($_smarty_tpl->tpl_vars['flyout']->value){?></div><?php }?>

<script type="text/javascript">
  $('#milestones_diagram').each(function() {
    var milestone_wrapper = $(this);

    milestone_wrapper.timelineDiagram({
      project_id : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['active_project']->value->getId());?>
,
      day_width : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['day_width']->value);?>
,
      data : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['milestones']->value);?>
,
      work_days : App.Config.get('work_days'),
      days_off : App.Config.get('days_off'),
      skip_days_off : true,
      images : <?php echo smarty_modifier_json($_smarty_tpl->tpl_vars['diagram_images']->value);?>
,
      reschedule : function (milestone, start_date, end_date) { },
      select : function (milestone, start_ate, end_date) { }
    });
  });

  // Milestones reordered
  App.Wireframe.Events.bind('milestones_reordered.content', function (event, milestones) {
    App.Wireframe.Content.reload();
  });
</script><?php }} ?>