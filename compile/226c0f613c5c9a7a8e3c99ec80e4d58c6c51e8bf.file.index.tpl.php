<?php /* Smarty version Smarty-3.1.12, created on 2016-05-03 15:01:10
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/project_exporter/views/default/project_exporter/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5746565775728bd36cbbfc7-42137126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '226c0f613c5c9a7a8e3c99ec80e4d58c6c51e8bf' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/project_exporter/views/default/project_exporter/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5746565775728bd36cbbfc7-42137126',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'export_exists' => 0,
    'exporters' => 0,
    'exporter_id' => 0,
    'exporter' => 0,
    'active_project' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5728bd36d74d62_23117781',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728bd36d74d62_23117781')) {function content_5728bd36d74d62_23117781($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_assemble')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assemble.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_modifier_json')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/modifier.json.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Project Exporter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<div id="project_exporter_container">


  <?php if ($_smarty_tpl->tpl_vars['export_exists']->value){?>
    <div id="download_link_block" class="download_link_block_top" style="display: block;">
      <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Previous project archive already exists. You can download it using following link:<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<br />
      <div id="download_link">
        <a href="<?php echo clean($_smarty_tpl->tpl_vars['export_exists']->value,$_smarty_tpl);?>
" target="_blank"><?php echo clean($_smarty_tpl->tpl_vars['export_exists']->value,$_smarty_tpl);?>
</a>
      </div>
    </div>
  <?php }?>


  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('action'=>'','method'=>"post",'class'=>"project_exporter_modules")); $_block_repeat=true; echo smarty_block_form(array('action'=>'','method'=>"post",'class'=>"project_exporter_modules"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="fields_wrapper">
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'exporters')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'exporters'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Choose which project sections to export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

		    <table class="common" cellspacing="0">
		      <tbody>
		      <?php  $_smarty_tpl->tpl_vars['exporter'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['exporter']->_loop = false;
 $_smarty_tpl->tpl_vars['exporter_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['exporters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['exporter']->key => $_smarty_tpl->tpl_vars['exporter']->value){
$_smarty_tpl->tpl_vars['exporter']->_loop = true;
 $_smarty_tpl->tpl_vars['exporter_id']->value = $_smarty_tpl->tpl_vars['exporter']->key;
?>
		        <tr>
		          <td class="checkbox"><input type="checkbox" name="selected_exporters[]" value="<?php echo clean($_smarty_tpl->tpl_vars['exporter_id']->value,$_smarty_tpl);?>
" id="export_<?php echo clean($_smarty_tpl->tpl_vars['exporter_id']->value,$_smarty_tpl);?>
" section_exporter_name='<?php echo clean($_smarty_tpl->tpl_vars['exporter']->value['name'],$_smarty_tpl);?>
' section_exporter_url="<?php echo smarty_function_assemble(array('route'=>'project_exporter_section_exporter','exporter_id'=>$_smarty_tpl->tpl_vars['exporter_id']->value,'project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
" checked="checked" <?php if ($_smarty_tpl->tpl_vars['exporter']->value['mandatory']){?> disabled="disabled"<?php }?> /></td>
		          <td><?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"export_".((string)$_smarty_tpl->tpl_vars['exporter_id']->value),'main_label'=>false)); $_block_repeat=true; echo smarty_block_label(array('for'=>"export_".((string)$_smarty_tpl->tpl_vars['exporter_id']->value),'main_label'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo $_smarty_tpl->tpl_vars['exporter']->value['name'];?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"export_".((string)$_smarty_tpl->tpl_vars['exporter_id']->value),'main_label'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
		        </tr>
		      <?php } ?>
		        <tr>
		          <td class="checkbox"><input type="checkbox" name="" value="finalize" id="export_finalize" section_exporter_name='<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Finalize<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' section_exporter_url="<?php echo smarty_function_assemble(array('route'=>'project_exporter_finalize_export','project_slug'=>$_smarty_tpl->tpl_vars['active_project']->value->getSlug()),$_smarty_tpl);?>
" disabled="disabled" checked="checked" /></td>
		          <td><?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>"export_finalize",'main_label'=>false)); $_block_repeat=true; echo smarty_block_label(array('for'=>"export_finalize",'main_label'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Finalize export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>"export_finalize",'main_label'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
		        </tr>
		      </tbody>
		    </table>
	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'exporters'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    
	    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>'visibility')); $_block_repeat=true; echo smarty_block_wrap(array('field'=>'visibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	      <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array('for'=>'visibility')); $_block_repeat=true; echo smarty_block_label(array('for'=>'visibility'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array('for'=>'visibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	      <div><label><input type="radio" name='visibility' value="1" <?php if ($_smarty_tpl->tpl_vars['active_project']->value->getDefaultVisibility()==@VISIBILITY_PRIVATE){?>checked="checked"<?php }?> /> <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Only the data clients can see<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</label></div>
	      <div><label><input type="radio" name='visibility' value="0" <?php if ($_smarty_tpl->tpl_vars['active_project']->value->getDefaultVisibility()==@VISIBILITY_NORMAL){?>checked="checked"<?php }?> /> <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All project data, including data marked as private<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</label></div>
	    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>'visibility'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
       
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

      <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('class'=>"default")); $_block_repeat=true; echo smarty_block_button(array('class'=>"default"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('class'=>"default"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php if (!AngieApplication::isOnDemand()){?>
        <?php echo smarty_function_checkbox_field(array('id'=>'compress','name'=>'compress','value'=>true,'label'=>"Compress exported project"),$_smarty_tpl);?>

     <?php }?>
     <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('action'=>'','method'=>"post",'class'=>"project_exporter_modules"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  var is_on_demand = <?php echo clean(smarty_modifier_json(AngieApplication::isOnDemand()),$_smarty_tpl);?>
;

  var wrapper = $('#project_exporter_container');
  var form = wrapper.find('form:first');
  var sections_table = form.find('table.common');
  var mandatory_checkboxes = sections_table.find('input[type=checkbox]:disabled');
  var export_table;
  var sections_query;
  var visibility;

  /**
   * Perform export
   * 
   * @param string row_id
   * @return null
   *|*|*|*/
  var perform_export_section = function (row) {
    var next_row = row.next('tr');

    var indicator = row.find('td.checkbox img').attr('src', App.Wireframe.Utils.indicatorUrl());

    $.ajax({
      'url' : App.extendUrl(row.attr('export_url'), {
        'sections' : sections_query,
        'visibility' : visibility,
        'compress' : compress
      }),
      'success' : function (response) {
        if (response.warnings) {
          var field = row.find('td.export_log');
          
          for (var key in response.warnings) {
            if (response.warnings.hasOwnProperty(key)) {
              field.append(response.warnings[key] + '<br />');
            } // if
          } //  if

          indicator.attr('src', App.Wireframe.Utils.indicatorUrl('warning'));
        } else {
          indicator.attr('src', App.Wireframe.Utils.indicatorUrl('ok'));
        } // if
        
        if (next_row.length) {
          perform_export_section(next_row);
        } else {
          var output_div = $('<div class="project_export_path_block" style="display: block;"></div>');
          if (response.compress == 0) {
            output_div.append('Exported project is located:<br /><div class="download_link"><strong>' + response.info_path + '</strong></div>');
          } else {
            output_div.append('<span class="download_link"><a href="' + response.info_path + '" target="_blank">' + App.lang('Download project archive') + '</a></span>');
          } //if
          $('<div class="fields_wrapper"></div>').append(output_div).insertAfter(export_table); 
        }// if
      },
      'error' : function (response, response_text) {
        indicator.attr('src', App.Wireframe.Utils.indicatorUrl('error'));
        var unknown_error = App.lang('Unknown error occurred');
        if (response.responseText[0] == '{') {
          var response_obj;
          eval('response_obj = ' + response.responseText);
          var error_message = response_obj.message ? response_obj.message : unknown_error;
          App.Wireframe.Flash.error(error_message);
          row.find('td.export_log').html(error_message);          
        } else {
          App.Wireframe.Flash.error(unknown_error);
          row.find('td.export_log').html(unknown_error);
        } // if
      }
    });
  }; // perform_export_section;

  // on form submit
  form.find('button').click(function() {
    form.hide();
    visibility = form.find('input[type=radio][name=visibility]:checked').attr('value');
    if(is_on_demand) {
      compress = 1;
    } else {
      compress = (form.find('input[type=checkbox][name=compress]:checked').attr('value') === undefined) ? 0 : 1;
    } //if
    export_table = $('<table class="common selected_exporters" cellspacing="0"></table>');
       
    sections_query = new Array();
    sections_table.find('input[type=checkbox]:checked').each(function () {
      var checkbox = $(this);
      var image = $('<img src="' + App.Wireframe.Utils.indicatorUrl('pending') + '" alt="pending" />');
      export_table.append('<tr export_url="' + checkbox.attr('section_exporter_url') + '" id="export_' + checkbox.attr('value') + '"><td class="checkbox"><img src="' + App.Wireframe.Utils.indicatorUrl('pending') + '" alt="pending" /></td><td class="name">' + checkbox.attr('section_exporter_name') + '</td><td class="export_log"></td></tr>');
      sections_query.push(checkbox.attr('value'));
    });
//    export_table.insertAfter(form);
    sections_query = sections_query.join(',');

    $('<div class="fields_wrapper"></div>').append(export_table).insertAfter(form); 
    perform_export_section(export_table.find('tr:first'));
    return false;
  });


</script><?php }} ?>