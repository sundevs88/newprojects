<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:40:09
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4747214015742ec19042f16-48880952%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'defcb663b6c5d3d1fe95301a1b0480ecf6195624' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/pdf_settings_admin/footer.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4747214015742ec19042f16-48880952',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_url' => 0,
    'template_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742ec190c5a53_93922540',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742ec190c5a53_93922540')) {function content_5742ec190c5a53_93922540($_smarty_tpl) {?><?php if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_function_checkbox')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_wrap')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap.php';
if (!is_callable('smarty_block_label')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.label.php';
if (!is_callable('smarty_function_radio_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.radio_field.php';
if (!is_callable('smarty_function_select_font')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/helpers/function.select_font.php';
if (!is_callable('smarty_function_color_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.color_field.php';
if (!is_callable('smarty_function_checkbox_field')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox_field.php';
if (!is_callable('smarty_block_wrap_buttons')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.wrap_buttons.php';
if (!is_callable('smarty_block_submit')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.submit.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"footer_body_form",'enctype'=>"multipart/form-data")); $_block_repeat=true; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"footer_body_form",'enctype'=>"multipart/form-data"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <div class="content_stack_wrapper">
  
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <div class="content_stack_optional"><?php echo smarty_function_checkbox(array('name'=>"template[print_footer]",'label'=>"Print",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_footer']),$_smarty_tpl);?>
</div>
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Footer layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
      
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"footer_layout")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"footer_layout"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Choose layout<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_radio_field(array('name'=>"template[footer_layout]",'label'=>"Invoice number on the left, page number on the right",'value'=>0,'checked'=>!$_smarty_tpl->tpl_vars['template_data']->value['footer_layout']),$_smarty_tpl);?>
<br/>
          <?php echo smarty_function_radio_field(array('name'=>"template[footer_layout]",'label'=>"Page number on the left, invoice number on the right",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['footer_layout']),$_smarty_tpl);?>

         <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"footer_layout"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      </div>
    </div>
    
    <div class="content_stack_element">
      <div class="content_stack_element_info">
        <h3><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Appearance<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</h3>
      </div>
      <div class="content_stack_element_body">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"client_details_font")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"client_details_font"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('label', array()); $_block_repeat=true; echo smarty_block_label(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text Style<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_label(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo smarty_function_select_font(array('name'=>"template[footer_font]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['footer_font']),$_smarty_tpl);?>

          <?php echo smarty_function_color_field(array('name'=>"template[footer_text_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['footer_text_color'],'class'=>"inline_color_picker"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"client_details_font"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
           
        
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap', array('field'=>"header_text_color")); $_block_repeat=true; echo smarty_block_wrap(array('field'=>"header_text_color"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <?php echo smarty_function_checkbox_field(array('name'=>"template[print_footer_border]",'label'=>"Show footer border",'value'=>1,'checked'=>$_smarty_tpl->tpl_vars['template_data']->value['print_footer_border'],'id'=>"border_toggler"),$_smarty_tpl);?>
&nbsp;
          <?php echo smarty_function_color_field(array('name'=>"template[footer_border_color]",'value'=>$_smarty_tpl->tpl_vars['template_data']->value['footer_border_color'],'class'=>"inline_color_picker border_property"),$_smarty_tpl);?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap(array('field'=>"header_text_color"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
    
      </div>
    </div>

  </div>

  <?php $_smarty_tpl->smarty->_tag_stack[] = array('wrap_buttons', array()); $_block_repeat=true; echo smarty_block_wrap_buttons(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <?php $_smarty_tpl->smarty->_tag_stack[] = array('submit', array()); $_block_repeat=true; echo smarty_block_submit(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save Changes<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_submit(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_wrap_buttons(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>"POST",'action'=>$_smarty_tpl->tpl_vars['form_url']->value,'id'=>"footer_body_form",'enctype'=>"multipart/form-data"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


  <script type="text/javascript">
    var wrapper = $('#footer_body_form');

    var border_toggler = wrapper.find('#border_toggler');
    var border_properties = wrapper.find('.border_property');
    var check_border_properties = function () {
      var is_checked = border_toggler.is(':checked');
      if (is_checked) {
        border_properties.show();
      } else {
        border_properties.hide();
      } // if      
    };    
    border_toggler.bind('click', check_border_properties);
    check_border_properties();

  </script><?php }} ?>