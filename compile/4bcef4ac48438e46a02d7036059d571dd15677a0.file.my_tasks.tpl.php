<?php /* Smarty version Smarty-3.1.12, created on 2016-05-29 01:48:43
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/backend/my_tasks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1197397606574a4a7b297824-00384741%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4bcef4ac48438e46a02d7036059d571dd15677a0' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/system/views/phone/backend/my_tasks.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1197397606574a4a7b297824-00384741',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'assignments' => 0,
    'assignments_name' => 0,
    'assignment' => 0,
    'urls' => 0,
    'project_slugs' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_574a4a7b307220_71966026',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_574a4a7b307220_71966026')) {function content_574a4a7b307220_71966026($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_image_url')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.image_url.php';
if (!is_callable('smarty_function_assignments_list_item')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.assignments_list_item.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Assignments<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
All Assignments<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div class="my_tasks">
	<ul data-role="listview" data-inset="true" data-dividertheme="j" data-theme="j">
		<?php if (is_foreachable($_smarty_tpl->tpl_vars['assignments']->value)){?>
	  	<?php  $_smarty_tpl->tpl_vars['assignments_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['assignments_name']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['assignments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['assignments_name']->key => $_smarty_tpl->tpl_vars['assignments_name']->value){
$_smarty_tpl->tpl_vars['assignments_name']->_loop = true;
?>
	  		<li data-role="list-divider"><img src="<?php echo smarty_function_image_url(array('name'=>"icons/listviews/projects-icon.png",'module'=>@SYSTEM_MODULE,'interface'=>AngieApplication::INTERFACE_PHONE),$_smarty_tpl);?>
" class="divider_icon" alt=""><?php echo clean($_smarty_tpl->tpl_vars['assignments_name']->value['label'],$_smarty_tpl);?>
</li>
				<?php if (is_foreachable($_smarty_tpl->tpl_vars['assignments_name']->value['assignments'])){?>
					<?php  $_smarty_tpl->tpl_vars['assignment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['assignment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['assignments_name']->value['assignments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['assignment']->key => $_smarty_tpl->tpl_vars['assignment']->value){
$_smarty_tpl->tpl_vars['assignment']->_loop = true;
?>
						<?php echo smarty_function_assignments_list_item(array('object'=>$_smarty_tpl->tpl_vars['assignment']->value,'urls'=>$_smarty_tpl->tpl_vars['urls']->value,'project_slugs'=>$_smarty_tpl->tpl_vars['project_slugs']->value),$_smarty_tpl);?>

					<?php } ?>
				<?php }?>
	  	<?php } ?>
	  <?php }else{ ?>
			<li><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There are no assignments<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</li>
		<?php }?>
	</ul>
</div><?php }} ?>