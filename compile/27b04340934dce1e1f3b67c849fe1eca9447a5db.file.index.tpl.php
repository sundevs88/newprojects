<?php /* Smarty version Smarty-3.1.12, created on 2016-03-03 08:52:36
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_email_to_comment/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:213576640956d7fb54b1a8f6-41921986%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '27b04340934dce1e1f3b67c849fe1eca9447a5db' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/email/views/default/fw_email_to_comment/index.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '213576640956d7fb54b1a8f6-41921986',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sections' => 0,
    'section' => 0,
    'step' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56d7fb54b74863_50330626',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d7fb54b74863_50330626')) {function content_56d7fb54b74863_50330626($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array()); $_block_repeat=true; echo smarty_block_title(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reply to Comment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Reply to Comment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="reply_to_comment">
  <table id="reply_to_comment_sections" cellspacing="0" cellpadding="0">
  <?php  $_smarty_tpl->tpl_vars['section'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['section']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['section']->key => $_smarty_tpl->tpl_vars['section']->value){
$_smarty_tpl->tpl_vars['section']->_loop = true;
?>
    <tr class="reply_to_comment_section <?php if ($_smarty_tpl->tpl_vars['section']->value['all_ok']){?>all_ok<?php }else{ ?>not_all_ok<?php }?>">
      <td class="section_content">
        <h2><?php echo clean($_smarty_tpl->tpl_vars['section']->value['title'],$_smarty_tpl);?>
</h2>
        <ul>
        <?php  $_smarty_tpl->tpl_vars['step'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['step']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['section']->value['steps']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['step']->key => $_smarty_tpl->tpl_vars['step']->value){
$_smarty_tpl->tpl_vars['step']->_loop = true;
?>
          <li class="<?php if ($_smarty_tpl->tpl_vars['step']->value['is_ok']){?>ok<?php }else{ ?>nok<?php }?>"><?php echo clean($_smarty_tpl->tpl_vars['step']->value['text'],$_smarty_tpl);?>
</li>
        <?php } ?>
        </ul>
      </td>
      <td class="section_next_step">
      <?php if ($_smarty_tpl->tpl_vars['section']->value['next_step']){?>
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('href'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['url'],'mode'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['mode'],'success_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['success_event'],'flyout_width'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['flyout_width'],'title'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['text'],'error_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['error_event'])); $_block_repeat=true; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['url'],'mode'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['mode'],'success_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['success_event'],'flyout_width'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['flyout_width'],'title'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['text'],'error_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['error_event']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean($_smarty_tpl->tpl_vars['section']->value['next_step']['text'],$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('href'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['url'],'mode'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['mode'],'success_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['success_event'],'flyout_width'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['flyout_width'],'title'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['text'],'error_event'=>$_smarty_tpl->tpl_vars['section']->value['next_step']['error_event']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

      <?php }?>
      </td>
    </tr>
  <?php } ?>
  </table>
  <p id="reply_to_comment_info"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Use this checklist to configure activeCollab's "Reply to Comment" feature. This feature will not work unless all of the listed requirements are met!<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
</div>

<script type="text/javascript">
  App.Wireframe.Events.bind('from_address_updated.content incoming_mailbox_created.content incoming_mailbox_updated.content', function() {
    App.Wireframe.Content.reload();
  });
  
  App.Wireframe.Events.bind('incoming_mailbox_updated_error.content', function(response, response_message) {
   	App.Wireframe.Flash.error(response_message);
  });
</script><?php }} ?>