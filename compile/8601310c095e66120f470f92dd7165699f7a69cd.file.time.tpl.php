<?php /* Smarty version Smarty-3.1.12, created on 2016-05-23 11:51:52
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/time.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3578994605742eed8ce8488-01085540%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8601310c095e66120f470f92dd7165699f7a69cd' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/invoicing/views/default/invoices/time.tpl',
      1 => 1426908836,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3578994605742eed8ce8488-01085540',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'active_invoice' => 0,
    'time_records' => 0,
    'expenses' => 0,
    'items_can_be_released' => 0,
    'time_record' => 0,
    'expense' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5742eed8e8e6f0_42721620',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5742eed8e8e6f0_42721620')) {function content_5742eed8e8e6f0_42721620($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_block_add_bread_crumb')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.add_bread_crumb.php';
if (!is_callable('smarty_function_use_widget')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.use_widget.php';
if (!is_callable('smarty_block_form')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.form.php';
if (!is_callable('smarty_function_checkbox')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.checkbox.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
if (!is_callable('smarty_function_user_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/authentication/helpers/function.user_link.php';
if (!is_callable('smarty_function_object_link')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.object_link.php';
if (!is_callable('smarty_block_button')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.button.php';
if (!is_callable('smarty_function_empty_slate')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/function.empty_slate.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array('lang'=>false)); $_block_repeat=true; echo smarty_block_title(array('lang'=>false), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getName())); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getName()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:name Time and Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getName()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array('lang'=>false), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('add_bread_crumb', array()); $_block_repeat=true; echo smarty_block_add_bread_crumb(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time & Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_add_bread_crumb(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

<?php echo smarty_function_use_widget(array('name'=>"form",'module'=>"environment"),$_smarty_tpl);?>


<div id="invoice_time">
  <?php $_smarty_tpl->smarty->_tag_stack[] = array('form', array('method'=>"post",'action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getReleaseUrl(),'class'=>"release_form")); $_block_repeat=true; echo smarty_block_form(array('method'=>"post",'action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getReleaseUrl(),'class'=>"release_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

  <?php if (is_foreachable($_smarty_tpl->tpl_vars['time_records']->value)||is_foreachable($_smarty_tpl->tpl_vars['expenses']->value)){?>
    <div id="timerecords">
      <table class="common" cellspacing="0">
        <thead>
          <tr>
            <th class="item"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Item<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="date left"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="user"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="hours"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="description"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <?php if ($_smarty_tpl->tpl_vars['items_can_be_released']->value){?>
            <th class="action"><?php echo smarty_function_checkbox(array('class'=>"check_all_items",'name'=>"all_chx"),$_smarty_tpl);?>
</th>
            <?php }?>
          </tr>
        </thead>
        <tbody>
      <?php  $_smarty_tpl->tpl_vars['time_record'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['time_record']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['time_records']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['time_record']->key => $_smarty_tpl->tpl_vars['time_record']->value){
$_smarty_tpl->tpl_vars['time_record']->_loop = true;
?>
        <tr>
          <td class="type"><?php echo clean($_smarty_tpl->tpl_vars['time_record']->value->getVerboseType(),$_smarty_tpl);?>
</td>
          <td class="date left"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['time_record']->value->getRecordDate(),0),$_smarty_tpl);?>
</td>
          <td class="user"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['time_record']->value->getUser()),$_smarty_tpl);?>
</td>
          <td class="hours"><?php echo clean($_smarty_tpl->tpl_vars['time_record']->value->getName(true),$_smarty_tpl);?>
</td>
          <td class="description">
          <?php if ($_smarty_tpl->tpl_vars['time_record']->value->getParent() instanceof ProjectObject){?>
            <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['time_record']->value->getParent()),$_smarty_tpl);?>

            <?php if ($_smarty_tpl->tpl_vars['time_record']->value->getSummary()){?>
              &mdash; <?php echo clean($_smarty_tpl->tpl_vars['time_record']->value->getSummary(),$_smarty_tpl);?>

            <?php }?>
          <?php }else{ ?>
            <?php echo clean($_smarty_tpl->tpl_vars['time_record']->value->getSummary(),$_smarty_tpl);?>

          <?php }?>
          </td>
          <?php if ($_smarty_tpl->tpl_vars['items_can_be_released']->value){?>
          <td class="action"><?php echo smarty_function_checkbox(array('name'=>"release_times[]",'value'=>$_smarty_tpl->tpl_vars['time_record']->value->getId(),'class'=>"action_chx"),$_smarty_tpl);?>
</td>
          <?php }?>
        </tr>
      <?php } ?>
      <?php  $_smarty_tpl->tpl_vars['expense'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['expense']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['expenses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['expense']->key => $_smarty_tpl->tpl_vars['expense']->value){
$_smarty_tpl->tpl_vars['expense']->_loop = true;
?>
        <tr>
          <td class="type"><?php echo clean($_smarty_tpl->tpl_vars['expense']->value->getVerboseType(),$_smarty_tpl);?>
</td>
          <td class="date left"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['expense']->value->getRecordDate(),0),$_smarty_tpl);?>
</td>
          <td class="user"><?php echo smarty_function_user_link(array('user'=>$_smarty_tpl->tpl_vars['expense']->value->getUser()),$_smarty_tpl);?>
</td>
          <td class="hours"><?php echo clean($_smarty_tpl->tpl_vars['expense']->value->getName(true,true),$_smarty_tpl);?>
</td>
          <td class="description">
          <?php if ($_smarty_tpl->tpl_vars['expense']->value->getParent() instanceof ProjectObject){?>
            <?php echo smarty_function_object_link(array('object'=>$_smarty_tpl->tpl_vars['expense']->value->getParent()),$_smarty_tpl);?>

            <?php if ($_smarty_tpl->tpl_vars['expense']->value->getSummary()){?>
              &mdash; <?php echo clean($_smarty_tpl->tpl_vars['expense']->value->getSummary(),$_smarty_tpl);?>

            <?php }?>
          <?php }else{ ?>
            <?php echo clean($_smarty_tpl->tpl_vars['expense']->value->getSummary(),$_smarty_tpl);?>

          <?php }?>
          </td>
          <?php if ($_smarty_tpl->tpl_vars['items_can_be_released']->value){?>
          <td class="action"><?php echo smarty_function_checkbox(array('name'=>"release_expenses[]",'value'=>$_smarty_tpl->tpl_vars['expense']->value->getId(),'class'=>"action_chx"),$_smarty_tpl);?>
</td>
          <?php }?>
        </tr>
      <?php } ?>
        </tbody>
      <?php if ($_smarty_tpl->tpl_vars['items_can_be_released']->value){?>
        <tfoot>
          <tr>
            <td class="right" id="release_invoice_time_records" colspan="<?php if ($_smarty_tpl->tpl_vars['items_can_be_released']->value){?>6<?php }else{ ?>5<?php }?>"><?php $_smarty_tpl->smarty->_tag_stack[] = array('button', array('confirm'=>"Are you sure that you want to remove relation between this invoice and time records listed above? Note that time records will NOT be deleted!",'type'=>"submit")); $_block_repeat=true; echo smarty_block_button(array('confirm'=>"Are you sure that you want to remove relation between this invoice and time records listed above? Note that time records will NOT be deleted!",'type'=>"submit"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Release<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_button(array('confirm'=>"Are you sure that you want to remove relation between this invoice and time records listed above? Note that time records will NOT be deleted!",'type'=>"submit"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</td>
          </tr>
        </tfoot>
      <?php }?>
      </table>
    </div>

    <?php echo smarty_function_empty_slate(array('name'=>'time','module'=>'invoicing'),$_smarty_tpl);?>

  <?php }else{ ?>
    <p class="empty_page"><span class="inner"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
There is no time attached to this invoice<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span></p>
  <?php }?>
  <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('method'=>"post",'action'=>$_smarty_tpl->tpl_vars['active_invoice']->value->getReleaseUrl(),'class'=>"release_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div>

<script type="text/javascript">
  $('#invoice_time').each(function() {
    var wrapper = $(this);

    var check_all_items = wrapper.find('.check_all_items').change(function(){
      var action_chx = wrapper.find('.action_chx');

      if($(this).attr('checked')) {
        action_chx.prop('checked', true);
      } else {
        action_chx.prop('checked', false);
      } // if
    });

    var form = wrapper.find('form.release_form');

    form.submit(function() {
      if(form.find('input[type=checkbox]:checked').length > 0) {
        form.ajaxSubmit({
          'url' : App.extendUrl(form.attr('action'), {
            'async' : 1
          }),
          'type' : 'post',
          'success' : function(response) {
            App.Wireframe.Flash.success(App.lang('Related invoice items have been released'));
            App.Wireframe.Content.setFromUrl(response['urls']['view']);
          },
          'error' : function(response) {
            App.Wireframe.Flash.error(App.lang('An error occurred while trying to release related invoice items'));
          }
        });
      } else {
        App.Wireframe.Flash.error('Please choose items that you want to release');
      } // if

      return false;
    });
  });
</script><?php }} ?>