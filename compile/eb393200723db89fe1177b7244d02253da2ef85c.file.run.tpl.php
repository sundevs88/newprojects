<?php /* Smarty version Smarty-3.1.12, created on 2016-03-05 14:14:04
         compiled from "/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/printer/tracking_reports/run.tpl" */ ?>
<?php /*%%SmartyHeaderCode:113258774256dae9ac2ae823-43243551%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eb393200723db89fe1177b7244d02253da2ef85c' => 
    array (
      0 => '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/modules/tracking/views/printer/tracking_reports/run.tpl',
      1 => 1426908837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113258774256dae9ac2ae823-43243551',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'filter_name' => 0,
    'result' => 0,
    'records_group' => 0,
    'filter' => 0,
    'currencies' => 0,
    'currency' => 0,
    'record' => 0,
    'currency_id' => 0,
    'expenses_for_currency' => 0,
    'logged_user' => 0,
    'formatted_currency' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_56dae9ac451733_97361897',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56dae9ac451733_97361897')) {function content_56dae9ac451733_97361897($_smarty_tpl) {?><?php if (!is_callable('smarty_block_title')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.title.php';
if (!is_callable('smarty_block_lang')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/block.lang.php';
if (!is_callable('smarty_modifier_hours')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.hours.php';
if (!is_callable('smarty_block_assign_var')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/environment/helpers/block.assign_var.php';
if (!is_callable('smarty_modifier_money')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.money.php';
if (!is_callable('smarty_modifier_date')) include '/home/admin/web/projects.ebluestore.com/public_html/activecollab/4.1.7/angie/frameworks/globalization/helpers/modifier.date.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('title', array('report_name'=>$_smarty_tpl->tpl_vars['filter_name']->value)); $_block_repeat=true; echo smarty_block_title(array('report_name'=>$_smarty_tpl->tpl_vars['filter_name']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time and Expenses Report (:report_name)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_title(array('report_name'=>$_smarty_tpl->tpl_vars['filter_name']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<div id="print_container">
<?php if ($_smarty_tpl->tpl_vars['result']->value){?>
  <?php  $_smarty_tpl->tpl_vars['records_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['records_group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['result']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['records_group']->key => $_smarty_tpl->tpl_vars['records_group']->value){
$_smarty_tpl->tpl_vars['records_group']->_loop = true;
?>
    <?php if (is_foreachable($_smarty_tpl->tpl_vars['records_group']->value['records'])){?>
      <h3><?php echo clean($_smarty_tpl->tpl_vars['records_group']->value['label'],$_smarty_tpl);?>
</h3>
      <table class="common" cellspacing="0">
        <thead>
          <tr>
        	<?php if ($_smarty_tpl->tpl_vars['filter']->value->getSumByUser()){?>
            <th class="user" style="width: 20%"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <?php if ($_smarty_tpl->tpl_vars['filter']->value->queryTimeRecords()){?><th class="time" style="width: 10%"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th><?php }?>
            <?php if ($_smarty_tpl->tpl_vars['filter']->value->queryExpenses()){?>
              <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
?>
                <th class="expenses"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('currency'=>$_smarty_tpl->tpl_vars['currency']->value->getCode())); $_block_repeat=true; echo smarty_block_lang(array('currency'=>$_smarty_tpl->tpl_vars['currency']->value->getCode()), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Expenses (:currency)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('currency'=>$_smarty_tpl->tpl_vars['currency']->value->getCode()), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
              <?php } ?>
            <?php }?>
        	<?php }else{ ?>
        	  <?php if ($_smarty_tpl->tpl_vars['filter']->value->getGroupBy()!=TrackingReport::GROUP_BY_DATE){?><th class="date"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Date<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th><?php }?>
            <th class="value"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Value<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="user"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
User<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="summary"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Summary<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <th class="status"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th>
            <?php if ($_smarty_tpl->tpl_vars['filter']->value->getGroupBy()!=TrackingReport::GROUP_BY_PROJECT){?><th class="project"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Project<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</th><?php }?>
        	<?php }?>
          </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['record'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['record']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['records_group']->value['records']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['record']->key => $_smarty_tpl->tpl_vars['record']->value){
$_smarty_tpl->tpl_vars['record']->_loop = true;
?>
          <tr>
          <?php if ($_smarty_tpl->tpl_vars['filter']->value->getSumByUser()){?>
            <td class="user"><?php echo clean($_smarty_tpl->tpl_vars['record']->value['user_name'],$_smarty_tpl);?>
</td>
          	<?php if ($_smarty_tpl->tpl_vars['filter']->value->queryTimeRecords()){?><td class="time" style="width: 10%"><?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['time']),$_smarty_tpl);?>
h</td><?php }?>
          	<?php if ($_smarty_tpl->tpl_vars['filter']->value->queryExpenses()){?>
              <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_smarty_tpl->tpl_vars['currency_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
 $_smarty_tpl->tpl_vars['currency_id']->value = $_smarty_tpl->tpl_vars['currency']->key;
?>
                <?php $_smarty_tpl->smarty->_tag_stack[] = array('assign_var', array('name'=>'expenses_for_currency')); $_block_repeat=true; echo smarty_block_assign_var(array('name'=>'expenses_for_currency'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
expenses_for_<?php echo clean($_smarty_tpl->tpl_vars['currency_id']->value,$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_assign_var(array('name'=>'expenses_for_currency'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                <td class="expenses"><?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['record']->value[$_smarty_tpl->tpl_vars['expenses_for_currency']->value],$_smarty_tpl->tpl_vars['currency']->value),$_smarty_tpl);?>
</td>
              <?php } ?>
            <?php }?>
         	<?php }else{ ?>
         		<?php if ($_smarty_tpl->tpl_vars['filter']->value->getGroupBy()!=TrackingReport::GROUP_BY_DATE){?>
         	  <td class="date"><?php echo clean(smarty_modifier_date($_smarty_tpl->tpl_vars['record']->value['record_date'],0),$_smarty_tpl);?>
</td>
         	  <?php }?>
            <td class="value">
            <?php if ($_smarty_tpl->tpl_vars['record']->value['type']=='TimeRecord'){?>
            	<?php if ($_smarty_tpl->tpl_vars['record']->value['group_name']){?>
            	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('hours'=>smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['value']),'job_type'=>$_smarty_tpl->tpl_vars['record']->value['group_name'])); $_block_repeat=true; echo smarty_block_lang(array('hours'=>smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['value']),'job_type'=>$_smarty_tpl->tpl_vars['record']->value['group_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:hours of :job_type<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('hours'=>smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['value']),'job_type'=>$_smarty_tpl->tpl_vars['record']->value['group_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            	<?php }else{ ?>
            	  <?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['record']->value['value']),$_smarty_tpl);?>
h
            	<?php }?>
            <?php }else{ ?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('assign_var', array('name'=>'formatted_currency')); $_block_repeat=true; echo smarty_block_assign_var(array('name'=>'formatted_currency'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo clean(smarty_modifier_money($_smarty_tpl->tpl_vars['record']->value['value'],$_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['record']->value['currency_id']],$_smarty_tpl->tpl_vars['logged_user']->value->getLanguage(),true,true),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_assign_var(array('name'=>'formatted_currency'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            	<?php if ($_smarty_tpl->tpl_vars['record']->value['group_name']){?>
            	  <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('amount'=>$_smarty_tpl->tpl_vars['formatted_currency']->value,'category'=>$_smarty_tpl->tpl_vars['record']->value['group_name'])); $_block_repeat=true; echo smarty_block_lang(array('amount'=>$_smarty_tpl->tpl_vars['formatted_currency']->value,'category'=>$_smarty_tpl->tpl_vars['record']->value['group_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
:amount in :category<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('amount'=>$_smarty_tpl->tpl_vars['formatted_currency']->value,'category'=>$_smarty_tpl->tpl_vars['record']->value['group_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            	<?php }else{ ?>
            	  <?php echo clean($_smarty_tpl->tpl_vars['formatted_currency']->value,$_smarty_tpl);?>

            	<?php }?>
            <?php }?>
            </td>
            <td class="user" style="width: 20%"><?php echo clean($_smarty_tpl->tpl_vars['record']->value['user_name'],$_smarty_tpl);?>
</td>
            <td class="summary">
            <?php if (($_smarty_tpl->tpl_vars['record']->value['parent_type']=='Task'&&$_smarty_tpl->tpl_vars['record']->value['parent_name'])&&$_smarty_tpl->tpl_vars['record']->value['summary']){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name'])); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task: :name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 (<?php echo clean($_smarty_tpl->tpl_vars['record']->value['summary'],$_smarty_tpl);?>
)
            <?php }elseif($_smarty_tpl->tpl_vars['record']->value['parent_type']=='Task'&&$_smarty_tpl->tpl_vars['record']->value['parent_name']){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name'])); $_block_repeat=true; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Task: :name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array('name'=>$_smarty_tpl->tpl_vars['record']->value['parent_name']), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['record']->value['summary']){?>
              <?php echo clean($_smarty_tpl->tpl_vars['record']->value['summary'],$_smarty_tpl);?>

            <?php }?>
            </td>
            <td class="status">
            <?php if ($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_NOT_BILLABLE){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Not Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_BILLABLE){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Billable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }elseif($_smarty_tpl->tpl_vars['record']->value['billable_status']==@BILLABLE_STATUS_PENDING_PAYMENT){?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Pending Payment<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }else{ ?>
              <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Paid<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

            <?php }?>
            </td>
            <?php if ($_smarty_tpl->tpl_vars['filter']->value->getGroupBy()!=TrackingReport::GROUP_BY_PROJECT){?><td class="project"><?php echo clean($_smarty_tpl->tpl_vars['record']->value['project_name'],$_smarty_tpl);?>
</td><?php }?>
          <?php }?>
          </tr>
        <?php } ?>
        </tbody>
        <tfoot>
          <tr>
        <?php if ($_smarty_tpl->tpl_vars['filter']->value->getSumByUser()){?>
          <td style="width: 20%"><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:</td>
          <?php if ($_smarty_tpl->tpl_vars['filter']->value->queryTimeRecords()){?><td style="width: 10%"><?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['records_group']->value['total_time']),$_smarty_tpl);?>
h</td><?php }?>
          <?php if ($_smarty_tpl->tpl_vars['filter']->value->queryExpenses()){?>
            <?php  $_smarty_tpl->tpl_vars['currency'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency']->_loop = false;
 $_smarty_tpl->tpl_vars['currency_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency']->key => $_smarty_tpl->tpl_vars['currency']->value){
$_smarty_tpl->tpl_vars['currency']->_loop = true;
 $_smarty_tpl->tpl_vars['currency_id']->value = $_smarty_tpl->tpl_vars['currency']->key;
?>
              <td><?php echo clean($_smarty_tpl->tpl_vars['records_group']->value['total_expenses'][$_smarty_tpl->tpl_vars['currency_id']->value]['verbose'],$_smarty_tpl);?>
</td>
            <?php } ?>
          <?php }?>
        <?php }else{ ?>
          <td colspan="<?php if ($_smarty_tpl->tpl_vars['filter']->value->getGroupBy()==TrackingReport::GROUP_BY_DATE||$_smarty_tpl->tpl_vars['filter']->value->getGroupBy()==TrackingReport::GROUP_BY_PROJECT){?>5<?php }else{ ?>6<?php }?>">
          <?php if ($_smarty_tpl->tpl_vars['records_group']->value['total_time']){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total Time<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: <?php echo clean(smarty_modifier_hours($_smarty_tpl->tpl_vars['records_group']->value['total_time']),$_smarty_tpl);?>
h.
          <?php }?>

          <?php if ($_smarty_tpl->tpl_vars['records_group']->value['has_expenses']){?>
            <?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Total Expenses<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:
            <?php  $_smarty_tpl->tpl_vars['currency_details'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['currency_details']->_loop = false;
 $_smarty_tpl->tpl_vars['currency_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['currencies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['currency_details']->key => $_smarty_tpl->tpl_vars['currency_details']->value){
$_smarty_tpl->tpl_vars['currency_details']->_loop = true;
 $_smarty_tpl->tpl_vars['currency_id']->value = $_smarty_tpl->tpl_vars['currency_details']->key;
?>
              <?php if ($_smarty_tpl->tpl_vars['records_group']->value['total_expenses'][$_smarty_tpl->tpl_vars['currency_id']->value]['value']){?>
                <?php echo clean($_smarty_tpl->tpl_vars['records_group']->value['total_expenses'][$_smarty_tpl->tpl_vars['currency_id']->value]['verbose'],$_smarty_tpl);?>

              <?php }?>
            <?php } ?>
          <?php }?>
          </td>
        <?php }?>
          </tr>
        </tfoot>
      </table>
    <?php }?>
  <?php } ?>
<?php }else{ ?>
  <p><?php $_smarty_tpl->smarty->_tag_stack[] = array('lang', array()); $_block_repeat=true; echo smarty_block_lang(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filter returned an empty result<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_lang(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</p>
<?php }?>
</div><?php }} ?>